package com.company;

import java.util.Scanner;

import static com.company.Bloco2.getCustoMaoObra;
import static com.company.Bloco2.getCustoTotalTinta;

public class Main {

    public static void main(String[] args) {
        //Exercicio1();
        //Exercicio1d();
        //Exercicio2();
        //Exercicio2d();
        //Exercicio3();
        //Exercicio4();
        //Exercicio5();
        //Exercicio6();
        //Exercicio7();
        //Exercicio9();
        //Exercicio10();
        //Exercicio11();
        //Exercicio12();
        //Exercicio13();
        //Exercicio14();
        //Exercicio15();
        //Exercicio16();
        //Exercicio19();
        Exercicio20();
    }

    public static void Exercicio1() {
        //dados
        int nota1, nota2, nota3, peso1, peso2, peso3;
        double mediaPesada;

        Scanner ler = new Scanner(System.in);

        //leitura de dados
        System.out.println("Introduza a 1º nota:");
        nota1 = ler.nextInt();
        System.out.println("Introduza a 2º nota:");
        nota2 = ler.nextInt();
        System.out.println("Introduza a 3º nota:");
        nota3 = ler.nextInt();
        System.out.println("Introduza o peso da 1º nota:");
        peso1 = ler.nextInt();
        System.out.println("Introduza o peso da 2º nota:");
        peso2 = ler.nextInt();
        System.out.println("Introduza o peso da 3º nota;");
        peso3 = ler.nextInt();


        //processamento
        mediaPesada = getMediaPesada(nota1, nota2, nota3, peso1, peso2, peso3);

        //saida de dados
        System.out.println("A média ponderada é de:" + mediaPesada);
    }

    public static double getMediaPesada(int nota1, int nota2, int nota3, int peso1, int peso2, int peso3) {
        return (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3);
    }

    public static void Exercicio1d() {
        //dados
        int nota1, nota2, nota3, peso1, peso2, peso3;
        double mediaPesada;

        Scanner ler = new Scanner(System.in);

        //leitura de dados
        System.out.println("Introduza a 1º nota:");
        nota1 = ler.nextInt();
        System.out.println("Introduza a 2º nota:");
        nota2 = ler.nextInt();
        System.out.println("Introduza a 3º nota:");
        nota3 = ler.nextInt();
        System.out.println("Introduza o peso da 1º nota:");
        peso1 = ler.nextInt();
        System.out.println("Introduza o peso da 2º nota:");
        peso2 = ler.nextInt();
        System.out.println("Introduza o peso da 3º nota;");
        peso3 = ler.nextInt();


        //processamento
        mediaPesada = getMediaPesada(nota1, nota2, nota3, peso1, peso2, peso3);

        //saida de dados
        if (mediaPesada < 8) {
            System.out.println("O aluno não cumpre a nota mínima exigida");
        } else {
            System.out.println("O aluno cumpre a nota mínima exigida");
        }
    }

    public static void Exercicio2() {
        //dados
        int numero, digito1, digito2, digito3;

        Scanner ler = new Scanner(System.in);

        //leitura de dados
        System.out.println("Introduza o número");
        numero = ler.nextInt();

        //processamento
        if (numero < 100 || numero > 999) {
            System.out.println("O número " + numero + " não tem três dígitos.");
        } else {
            digito3 = numero % 10;
            digito2 = (numero / 10) % 10;
            digito1 = (numero / 100) % 10;
            System.out.println("O número " + numero + " tem os seguintes digitos: " + digito1 + " " + digito2 + " " + digito3);
        }
    }

    public static void Exercicio2d() {
        //dados
        int numero, digito1, digito2, digito3;

        Scanner ler = new Scanner(System.in);

        //leitura de dados
        System.out.println("Introduza o número");
        numero = ler.nextInt();

        //processamento
        if (numero < 100 || numero > 999) {
            System.out.println("O número " + numero + " não tem três dígitos.");
        } else {
            digito3 = numero % 10;
            digito2 = (numero / 10) % 10;
            digito1 = (numero / 100) % 10;

            if (numero % 2 == 0) {
                System.out.println("O número " + numero + " tem os seguintes digitos: " + digito1 + " " + digito2 + " " + digito3 + " e é um número par");
            } else {
                System.out.println("O número " + numero + " tem os seguintes digitos: " + digito1 + " " + digito2 + " " + digito3 + " e é um número ímpar");
            }
        }
    }

    public static void Exercicio3() {
        double x1;
        double y1;
        double x2;
        double y2;
        double d;

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza a coordenada x do primeiro ponto:");
        x1 = ler.nextDouble();
        System.out.println("Introduza a coordenada y do primeiro ponto:");
        y1 = ler.nextDouble();
        System.out.println("Introduza a coordenada x do segundo ponto:");
        x2 = ler.nextDouble();
        System.out.println("Introduza a coordenada y do segundo ponto:");
        y2 = ler.nextDouble();

        d = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));

        System.out.println("A distância entre os dois pontos é de: " + d);

    }

    public static void Exercicio4() {
        double x;
        double y;

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o valor de x:");
        x = ler.nextDouble();

        if (x < 0) {
            y = x;
            System.out.println("O valor da função é igual a:" + y);
        } else {
            if (x > 0) {
                y = Math.pow(x, 2) - 2 * x;
                System.out.println("O valor da função é igual a:" + y);
            } else {
                y = 0;
                System.out.println("O valor da função é igual a:" + y);
            }
        }
    }

    public static void Exercicio5() {
        double area, aresta, volume;

        Scanner ler = new Scanner(System.in);

        System.out.println("Insina a área do cubo em cm^2:");
        area = ler.nextDouble();

        if (area > 0) {
            aresta = Math.sqrt(area / 6);
            volume = Math.pow(aresta, 3);

            System.out.println("O volume do cubo é:" + volume);
        } else {
            System.out.println("Valor de área incorreto.");
        }
    }

    public static void Exercicio6() {
        //dados
        int tempoinicial;
        double horas, minutos, segundos;

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o tempo em segundos:");
        tempoinicial = ler.nextInt();

        //processamento
        horas = (tempoinicial / 60) / 60;
        minutos = (tempoinicial - horas * 3600) / 60;
        segundos = tempoinicial - (horas * 3600) - (minutos * 60);

        //saida de dados
        System.out.println("São " + String.format("%.0f", horas) + ":" + String.format("%.0f", minutos) + ":" + String.format("%.0f", segundos) + " h");
    }

    public static void Exercicio7() {
        //dados
        double areaEdificio, custoLTinta, rendimentoL, salarioDiaPintor, numeroPintores, diasnecessarios, custoTotal, custoMaoObra, custoTotalTinta;

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza a área do edifício em metros:");
        areaEdificio = ler.nextDouble();
        System.out.println("Introduza o custo da tinta por litro:");
        custoLTinta = ler.nextDouble();
        System.out.println("Introduza o rendimento da tinta:");
        rendimentoL = ler.nextDouble();
        System.out.println("Introduza o salário por dia do pintor:");
        salarioDiaPintor = ler.nextDouble();

        if (areaEdificio < 0 && areaEdificio < 100) {
            numeroPintores = 1;
        }
        if (areaEdificio >= 100 && areaEdificio < 300) {
            numeroPintores = 2;
        }
        if (areaEdificio >= 300 && areaEdificio < 1000) {
            numeroPintores = 3;
        } else {
            numeroPintores = 4;
        }

        System.out.println("O que pretende calcular?");
        System.out.println("Insira 1-Custo total da obra, 2-Custo da Mão de Obra e 3-Custo Total da Tinta");
        int x;
        x = ler.nextInt();
        if (x >= 1 && x < 3) {

            diasnecessarios = (areaEdificio / 16) / numeroPintores;
            custoTotalTinta = getCustoTotalTinta(areaEdificio, custoLTinta, rendimentoL);
            custoMaoObra = getCustoMaoObra(salarioDiaPintor, numeroPintores, diasnecessarios);
            custoTotal = getCustoTotalTinta(areaEdificio, custoLTinta, rendimentoL) + getCustoMaoObra(salarioDiaPintor, numeroPintores, diasnecessarios);

            switch (x) {
                case 1: {
                    System.out.println("O custo total da obra é de: " + custoTotal + "€");
                    break;
                }
                case 2: {
                    System.out.println("O custo total da mão de obra é de:" + custoMaoObra + "€");
                    break;
                }
                case 3: {
                    System.out.println("O custo total da tinta é de:" + custoTotalTinta + "€");
                    break;
                }
            }
        } else {
            System.out.println("O número introduzido é uma opção inválida");
        }
    }

    public static void Exercicio9() {
        int centenas, dezenas, unidades;

        Scanner ler = new Scanner(System.in);

        System.out.println("Indique o número das centenas:");
        centenas = ler.nextInt();
        System.out.println("Indique o número das dezenas");
        dezenas = ler.nextInt();
        System.out.println("Indique o número das unidades");
        unidades = ler.nextInt();

        if (centenas < dezenas && dezenas < unidades) {
            System.out.println("A sequência de números é crescente");
        } else if (centenas > dezenas && dezenas > unidades) {
            System.out.println("A sequência de números é decrescente");
        } else {
            System.out.println("A sequência de números não é crescente nem decrescente");
        }
    }

    public static void Exercicio10() {
        double precoInicial, precoFinal, x;

        Scanner ler = new Scanner(System.in);

        System.out.println("Insira o preço inicial:");
        precoInicial = ler.nextDouble();

        if (precoInicial > 200) {
            x = 0.4;
        } else if (precoInicial > 100) {
            x = 0.6;
        } else if (precoInicial > 50) {
            x = 0.7;
        } else {
            x = 0.8;
        }
        precoFinal = precoInicial * x;
        System.out.println("O preço final com desconto é de:" + precoFinal);
    }

    public static void Exercicio11() {
        double aprovados;

        Scanner ler = new Scanner(System.in);

        System.out.println("Insira a percentagem de aprovados:");
        aprovados = ler.nextDouble();

        if (aprovados < 0 || aprovados > 1) {
            System.out.println("Valor Inválido");
        } else {
            if (aprovados < 0.2) {
                System.out.println("Turma Má");
            } else if (aprovados < 0.5) {
                System.out.println("Turma Fraca");
            } else if (aprovados < 0.7) {
                System.out.println("Turma Razoável");
            } else if (aprovados < 0.9) {
                System.out.println("Turma Boa");
            } else
                System.out.println("Turma Excelente");
        }
    }

    public static void Exercicio12() {
        double indicePoluicao;

        Scanner ler = new Scanner((System.in));

        System.out.println("Introduza o índice de poluição do meio ambiente:");
        indicePoluicao = ler.nextDouble();

        if (indicePoluicao < 0) {
            System.out.println("O índice de poluição introduzido não é válido");
        } else if (indicePoluicao >= 0 && indicePoluicao <= 0.3) {
            System.out.println("Índice de poluição aceitável.");
        } else if (indicePoluicao <= 0.4) {
            System.out.println("Intimidar as indústrias do 1º grupo para suspender as suas actividades.");
        } else if (indicePoluicao <= 0.5) {
            System.out.println("Intimidar as indústrias do 1º e 2º grupo para suspender as suas actividades.");
        } else {
            System.out.println("Intimidar as 3 empresas a suspender as suas actividades.");
        }
    }

    public static void Exercicio13() {
        double m2, nrArvores, nrArbustos, segundosGrama, segundosArvores, segundosArbustos, custoGrama, custoArvores, custoArbustos, horasTotal, minutosTotal, custoTotal;
        int x;

        Scanner ler = new Scanner((System.in));

        System.out.println("Introduza a área para colocar a grama:");
        m2 = ler.nextDouble();
        System.out.println("Introduza o número de árvores desejados:");
        nrArvores = ler.nextDouble();
        System.out.println("Introduza o número de arbustos desejados:");
        nrArbustos = ler.nextDouble();
        System.out.println("Introduza o número da opção que quer saber:");
        System.out.println("1 para o tempo total do serviço, 2 para o custo total do serviço e 3 para o tempo e custo total.");
        x = ler.nextInt();

        segundosGrama = 300 * m2;
        segundosArvores = 600 * nrArvores;
        segundosArbustos = 400 * nrArbustos;

        custoGrama = 10 * m2;
        custoArvores = 20 * nrArvores;
        custoArbustos = 15 * nrArbustos;

        horasTotal = (segundosGrama + segundosArvores + segundosArbustos) / 3600;
        minutosTotal = (horasTotal % 3600) * 60;

        custoTotal = (horasTotal * 10) + (m2 * 10) + (nrArvores * 20) + (nrArbustos * 15);

        if (x == 1) {
            System.out.println("O tempo total para o serviço solicitado é de: " + String.format("%.0f", horasTotal) + ":" + String.format("%.0f", minutosTotal));
        } else if (x == 2) {
            System.out.println("O custo total do serviço solicitado é de: " + String.format("%.2f", custoTotal));
        } else if (x == 3) {
            System.out.println("A quantidade de horas para o serviço solicitado é de: " + String.format("%.0f", horasTotal) + ":" + String.format("%.0f", minutosTotal) + " e o custo total é de:" + String.format("%.2f", custoTotal));
        } else {
            System.out.println("Caracter inválido.");
        }
    }

    public static void Exercicio14() {
        //dados. d de distância. dmd: distância média diária
        double d1, d2, d3, d4, d5, dmd;

        Scanner ler = new Scanner(System.in);

        System.out.println("Insira as milhas percorridas no 1º dia:");
        d1 = ler.nextDouble();
        System.out.println("Insira as milhas percorridas no 2º dia:");
        d2 = ler.nextDouble();
        System.out.println("Insira as milhas percorridas no 3º dia:");
        d3 = ler.nextDouble();
        System.out.println("Insira as milhas percorridas no 4º dia:");
        d4 = ler.nextDouble();
        System.out.println("Insira as milhas percorridas no 5º dia:");
        d5 = ler.nextDouble();

        //a dmd tem que vir em km (1 milha= 1,609km)
        dmd = getMediaMilhasDiaria(d1, d2, d3, d4, d5) * 1.609;

        System.out.println("A distância média diária percorrida pelo estafeta é de:" + dmd);
    }

    public static double getMediaMilhasDiaria(double d1, double d2, double d3, double d4, double d5) {
        return (d1 + d2 + d3 + d4 + d5) / 5;
    }

    public static void Exercicio15() {
        //dados h=hipotenusa, c1=cateto1, c2=cateto2
        double h, c1, c2;

        Scanner ler = new Scanner(System.in);

        System.out.println("Insira o valor da hipotenusa:");
        h = ler.nextDouble();
        System.out.println("Insira o valor do primeiro cateto:");
        c1 = ler.nextDouble();
        System.out.println("Insira o valor do segundo cateto");
        c2 = ler.nextDouble();

        if (h < 0 || c1 < 0 || c2 < 0 || h > c1 + c2 || c1 > h + c2 || c2 > h + c1) {
            System.out.println("Um triângulo com estas medidas é impossível.");
        } else if (h == c1 && c1 == c2) {
            System.out.println("O triângulo é equilátero.");
        } else if (h != c1 && h != c2 && c1 != c2) {
            System.out.println("É um triângulo escaleno.");
        } else {
            System.out.println("É um triângulo isósceles.");
        }
    }

    public static void Exercicio16() {
        double a1, a2, a3;

        Scanner ler = new Scanner(System.in);

        System.out.println("Insira o valor do primeiro ângulo:");
        a1 = ler.nextDouble();
        System.out.println("Insira o valor do segundo ângulo:");
        a2 = ler.nextDouble();
        System.out.println("Insira o valor do terceiro ângulo");
        a3 = ler.nextDouble();

        if (a1 < 0 || a2 < 0 || a3 < 0 || a1 + a2 + a3 != 180) {
            System.out.println("Este triângulo não é possível.");
            //ver relação dos angulos com as medidas
        } else if (a1 > 90 || a2 > 90 || a3 > 90) {
            System.out.println("O triângulo é obtusângulo");
        } else if (a1 == 90 || a2 == 90 || a3 == 90) {
            System.out.println("O triângulo é rectângulo");
        } else if (a1 < 90 && a2 < 90 && a3 < 90) {
            System.out.println("O triângulo é acutângulo");
        }
    }

    public static void Exercicio19() {
        //hT= horas de trabalho sS= salário semanal
        double hT, sS;

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o número de horas trabalhadas");
        hT = ler.nextDouble();

        //assumindo que o número máximo de horas que um empregado pode trabalhar por semana é de 120h
        if (hT <= 0) {
            System.out.println("O empregado não trabalhou para ter salário.");
        } else if (hT > 120) {
            System.out.println("Não minta! Ninguém trabalha tanto!");
        } else if (hT <= 36) {
            sS = hT * 7.5;
            System.out.println("O salário semanal é de: " + sS);
        } else if (hT > 36 && hT <= 41) {
            sS = 36 * 7.5 + (hT - 36) * 10;
            System.out.println("O salário semanal é de: " + sS);
        } else if (hT > 41 && hT <= 120) {
            sS = 36 * 7.5 + 5 * 10 + (hT - 41) * 15;
            System.out.println("O salário semanal é de: " + sS);
        }
    }

    public static void Exercicio20() {
        //dados dS=dia da semana tK=tipo de kit
        //1,2,3,4 e 5 correspondem aos dias de semana. 6,7 e 8 a sábado, domingo e feriado.
        int dS, tK;

        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o dia da semana:");
        dS = ler.nextInt();
        System.out.println("Introduza o tipo de kit pretendido");
        tK = ler.nextInt();

        if (dS == 1 || dS == 2 || dS == 3 || dS == 4 || dS == 5 && tK == 1) {
            System.out.println("O preço do aluguer é de 30€");
        } else if (dS == 1 || dS == 2 || dS == 3 || dS == 4 || dS == 5 && tK == 2) {
            System.out.println("O preço do aluguer é de 50€");
        } else if (dS == 1 || dS == 2 || dS == 3 || dS == 4 || dS == 5 && tK == 3) {
            System.out.println("O preço do aluguer é de 100€");
        } else if (dS == 6 || dS == 7 || dS == 8 && tK == 1) {
            System.out.println("O preço do aluguer é de 40€");
        } else if (dS == 6 || dS == 7 || dS == 8 && tK == 2) {
            System.out.println("O preço do aluguer é de 70€");
        } else if (dS == 6 || dS == 7 || dS == 8 && tK == 2) {
            System.out.println("O preço do aluguer é de 140€");
        } else
            System.out.println("O dia da semana e/ou o tipo de kit introduzidos são inválidos");

    }
}

