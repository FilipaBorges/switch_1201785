package com.company;

import static com.company.Main.getMediaMilhasDiaria;

public class Bloco2 {

    public static double Exercicio1(int nota1, int nota2, int nota3, int peso1, int peso2, int peso3) {
        //dados
        double mediaPesada;

        mediaPesada = Main.getMediaPesada(nota1, nota2, nota3, peso1, peso2, peso3);

        if (mediaPesada < 8) {
            System.out.println("O aluno não cumpre a nota mínima exigida");
        } else {
            System.out.println("O aluno cumpre a nota mínima exigida");
        }
        return mediaPesada;
    }

    public static String Exercicio2e(int numero) {
        int digito1;
        int digito2;
        int digito3;

        if (numero < 100 || numero > 999) {
            return ("O número " + numero + " não tem três dígitos.");
        } else {
            digito3 = numero % 10;
            digito2 = (numero / 10) % 10;
            digito1 = (numero / 100) % 10;

            if (numero % 2 == 0) {
                return ("O número " + numero + " tem os seguintes digitos: " + digito1 + " " + digito2 + " " + digito3 + " e é um número par");
            } else {
                return ("O número " + numero + " tem os seguintes digitos: " + digito1 + " " + digito2 + " " + digito3 + " e é um número ímpar");
            }
        }
    }

    public static double Exercicio3_v2(double x1, double y1, double x2, double y2) {
        double d;

        d = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));

        return d;
    }

    public static double Exercicio4_v2(double x) {
        double y;

        if (x < 0) {
            y = x;
            return y;
        } else {
            if (x > 0) {
                y = Math.pow(x, 2) - 2 * x;
                return y;
            } else {
                y = 0;
                return y;
            }
        }
    }

    public static String Exercicio5_v2(double area) {
        double aresta, volume;

        if (area > 0) {
            aresta = Math.sqrt(area / 6);
            volume = Math.pow(aresta, 3);

            if (volume <= 1000) {
                return ("O volume é de:" + String.format("%.2f", volume) + " e é um cubo pequeno.");
            }
            if (volume > 1000 && volume <= 2000) {
                return ("O volume é de:" + String.format("%.2f", volume) + " e é um cubo médio.");
            } else {
                return ("O volume é de:" + String.format("%.2f", volume) + " e é um cubo grande.");
            }
        } else {
            return ("Valor de área incorreto.");
        }
    }

    public static String Exercicio6_v2(int tempoinicial) {
        int horas, minutos, segundos;

        if (tempoinicial < 0) {
            return ("Não existem horas negativas");
        } else {
            horas = (tempoinicial / 60) / 60;
            minutos = (tempoinicial - horas * 3600) / 60;
            segundos = tempoinicial - (horas * 3600) - (minutos * 60);

            return ("São " + horas + ":" + minutos + ":" + segundos + " h");
            //inicialmente tinha as variaveis como double e portanto tinha formatação. Como depois os testes não corriam por erros de arredondamento, coloquei tudo em int.
            //return ("São " + String.format("%.0f", horas) + ":" + String.format("%.0f", minutos) + ":" + String.format("%.0f", segundos) + " h");
        }
    }

    public static String Exercicio6_v3(int tempoinicial) {
        //Entre as 6:0:0 (21600sg) e as 12:0:0(43200sg) tem que retornar Bom dia.
        //Entre 12:0:1(43200sg) e as 20:0:0(72000) tem que retornar Boa tarde.

        if (tempoinicial > 21600 && tempoinicial < 43201) {
            return ("Bom dia!");
        }
        if (tempoinicial >= 43201 && tempoinicial < 72001) {
            return ("Boa tarde!");
        } else {
            return ("Boa noite!");
        }
    }

    public static String Exercicio7(double areaEdificio, double custoLTinta, double rendimentoL, double salarioDiaPintor, double numeroPintores, double diasnecessarios) {
        double custoTotal, custoMaoObra, custoTotalTinta;
        //Cada pintor pinta 16m2/dia

        custoTotalTinta = getCustoTotalTinta(areaEdificio, custoLTinta, rendimentoL);
        custoMaoObra = getCustoMaoObra(salarioDiaPintor, numeroPintores, diasnecessarios);
        custoTotal = getCustoTotalTinta(areaEdificio, custoLTinta, rendimentoL) + getCustoMaoObra(salarioDiaPintor, numeroPintores, diasnecessarios);
        diasnecessarios = (areaEdificio / 16) / numeroPintores;

        if (areaEdificio == 0) {
            return ("O custo total é Zero");
        }
        if (areaEdificio > 0 && areaEdificio < 100) {
            numeroPintores = 1;
            return ("O Custo total da Obra é de: " + String.format("%.2f", custoTotal) + "€");
        }
        if (areaEdificio >= 100 && areaEdificio < 300) {
            numeroPintores = 2;
            return ("O Custo total da Obra é de: " + String.format("%.2f", custoTotal) + "€");
        }
        if (areaEdificio >= 300 && areaEdificio < 1000) {
            numeroPintores = 3;
            return ("O Custo total da Obra é de: " + String.format("%.2f", custoTotal) + "€");
        } else {
            numeroPintores = 4;
            return ("O Custo total da Obra é de: " + String.format("%.2f", custoTotal) + "€");
        }
    }

    public static double getCustoMaoObra(double salarioDiaPintor, double numeroPintores,
                                         double diasnecessarios) {
        return numeroPintores * salarioDiaPintor * diasnecessarios;
    }

    public static double getCustoTotalTinta(double areaEdificio, double custoLTinta, double rendimentoL) {
        return (areaEdificio / rendimentoL) * custoLTinta;
    }


    public static String Exercicio8(int x, int y) {

        if (x % y == 0) {
            return ("Y é múltiplo de X");
        }
        if (x % y != 0) {
            return ("Y não é múltiplo de X");
        } else {
            return ("Y não é múltiplo nem divisível por X");
        }
    }

    public static String Exercicio9(int centenas, int dezenas, int unidades) {

        if (centenas < dezenas && dezenas < unidades) {
            return ("A sequência de números é crescente");
        } else if (centenas > dezenas && dezenas > unidades) {
            return ("A sequência de números é decrescente");
        } else {
            return ("A sequência de números não é crescente nem decrescente");
        }
    }

    public static String Exercicio10(double precoInicial, double precoFinal) {
        double x;

        if (precoInicial > 200) {
            x = 0.4;
        } else if (precoInicial > 100) {
            x = 0.6;
        } else if (precoInicial > 50) {
            x = 0.7;
        } else {
            x = 0.8;
        }
        precoFinal = precoInicial * x;
        return ("O preço final da peça é de:" + precoFinal);
    }

    public static String Exercicio11(double aprovados) {

        if (aprovados < 0 || aprovados > 1) {
            return "Valor Inválido";
        } else {
            if (aprovados < 0.2) {
                return "Turma Má";
            } else if (aprovados < 0.5) {
                return "Turma Fraca";
            } else if (aprovados < 0.7) {
                return "Turma Razoável";
            } else if (aprovados < 0.9) {
                return "Turma Boa";
            } else
                return "Turma Excelente";
        }
    }

    public static String Exercicio12(double indicePoluicao) {

        if (indicePoluicao < 0) {
            return "O índice de poluição introduzido não é válido";
        } else if (indicePoluicao >= 0 && indicePoluicao <= 0.3) {
            return "Índice de poluição aceitável.";
        } else if (indicePoluicao <= 0.4) {
            return "Intimidar as indústrias do 1º grupo para suspender as suas actividades.";
        } else if (indicePoluicao <= 0.5) {
            return "Intimidar as indústrias do 1º e 2º grupo para suspender as suas actividades.";
        } else {
            return "Intimidar as 3 empresas a suspender as suas actividades.";
        }
    }

    public static String Exercicio13(double m2, double nrArvores, double nrArbustos, int x) {
        double segundosGrama, segundosArvores, segundosArbustos, custoGrama, custoArvores, custoArbustos, horasTotal, minutosTotal, custoTotal;

        segundosGrama = 300 * m2;
        segundosArvores = 600 * nrArvores;
        segundosArbustos = 400 * nrArbustos;

        custoGrama = 10 * m2;
        custoArvores = 20 * nrArvores;
        custoArbustos = 15 * nrArbustos;

        horasTotal = (segundosGrama + segundosArvores + segundosArbustos) / 3600;
        //minutosTotal= (horasTotal%3600)*60;
        custoTotal = (horasTotal * 10) + (m2 * 10) + (nrArvores * 20) + (nrArbustos * 15);

        if (x == 1) {
            return "O tempo total para o serviço solicitado é de: " + String.format("%.2f", horasTotal);
        } else if (x == 2) {
            return "O custo total do serviço solicitado é de: " + String.format("%.2f", custoTotal);
        } else if (x == 3) {
            return "A quantidade de horas para o serviço solicitado é de: " + String.format("%.2f", horasTotal) + " e o custo total é de:" + String.format("%.2f", custoTotal);
        } else {
            return ("Caracter inválido.");
        }
    }

    public static String Exercicio14(double d1, double d2, double d3, double d4, double d5) {
        //dados. d de distância. dmd: distância média diária
        double dmd;

        if (d1 < 0 || d2 < 0 || d3 < 0 || d4 < 0 || d5 < 0) {
            return "Distância Inválida.";
        }
        //a dmd tem que vir em km (1 milha= 1,609km)
        dmd = getMediaMilhasDiaria(d1, d2, d3, d4, d5) * 1.609;

        return "A distância média percorrida pelo estafeta foi de: " + dmd;

    }

    public static String Exercicio15(double h, double c1, double c2) {
        //dados h=hipotenusa, c1=cateto1, c2=cateto2

        if (h < 0 || c1 < 0 || c2 < 0 || h > c1 + c2 || c1 > h + c2 || c2 > h + c1) {
            return ("Um triângulo com estas medidas é impossível.");
        } else if (h == c1 && c1 == c2) {
            return ("O triângulo é equilátero.");
        } else if (h != c1 && h != c2 && c1 != c2) {
            return ("É um triângulo escaleno.");
        } else {
            return ("É um triângulo isósceles.");
        }
    }

    public static String Exercicio16(double a1, double a2, double a3) {


        if (a1 < 0 || a2 < 0 || a3 < 0 || a1 + a2 + a3 != 180) {
            return "Este triângulo não é possível.";
        } else if (a1 > 90 || a2 > 90 || a3 > 90) {
            return "O triângulo é obtusângulo";
        } else if (a1 == 90 || a2 == 90 || a3 == 90) {
            return "O triângulo é rectângulo";
        } else if (a1 < 90 && a2 < 90 && a3 < 90) {
            return "O triângulo é acutângulo";
        } else
            return "Este triângulo não é possível.";
    }

    public static String Exercicio17(int hp, int mp, int hd, int md) {
        int chegada = hp * 60 + mp + hd * 60 + md;
        int hc = chegada / 60;
        int mc = chegada % 60;

        if (hc >= 24) {
            //hc= hc-24
            hc -= 24;
            return ("O comboio chega às " + hc + ":" + mc + "h do dia de amanhã");
            //ou
            //dia=amanhã e no else colocamos dia=hoje
        } else {
            return ("O comboio chega às " + hc + ":" + mc + "h do dia de hoje");
            //Uma alternativa seria colocar String dia=hoje logo após os dados e ele ia assumir por defeito que o dia era hoje. Se entrasse no if então teriamos o dia amanhã. Nesse caso, só seria necessário um return e não era necessário o else.
        }
    }

    public static String Exercicio18(int hi, int mi, int si, int sprocessamento) {
        int tempoinicial = hi * 3600 + mi * 60 + si;
        int tempofinal = tempoinicial + sprocessamento;
        int hf, mf, sf;

        hf = tempofinal / 3600;
        mf = (tempofinal % 3600) / 60;
        sf = (tempofinal % 3600) % 60;

        return hf + ":" + mf + ":" + sf + "h";
    }

    public static String Exercicio19(double hT) {
        double sS;

        //assumindo que o número máximo de horas que um empregado pode trabalhar por semana é de 120h
        if (hT <= 0) {
            return "O empregado não trabalhou para ter salário.";
        } else if (hT <= 36) {
            sS = hT * 7.5;
            return "O salário semanal é de: " + String.format("%.2f", sS);
        } else if (hT > 36 && hT <= 41) {
            sS = 36 * 7.5 + (hT - 36) * 10;
            return "O salário semanal é de: " + String.format("%.2f", sS);
        } else if (hT > 41 && hT <= 120) {
            sS = 36 * 7.5 + 5 * 10 + (hT - 41) * 15;
            return "O salário semanal é de: " + String.format("%.2f", sS);
        } else
            return "Não minta! Ninguém trabalha tanto!";
    }

    public static String Exercicio20(int dS, int tK) {
        //dados dS=dia da semana tK=tipo de kit

        if (dS == 1 || dS == 2 || dS == 3 || dS == 4 || dS == 5) {
            if (tK == 1) {
                return "O preço do aluguer é de 30€";
            } else if (tK == 2) {
                return "O preço do aluguer é de 50€";
            } else if (tK == 3) {
                return "O preço do aluguer é de 100€";
            }
        } else if (dS == 6 || dS == 7 || dS == 8) {
            if (tK == 1) {
                return "O preço do aluguer é de 40€";
            } else if (tK == 2) {
                return "O preço do aluguer é de 70€";
            } else if (tK == 3) {
                return "O preço do aluguer é de 140€";
            }
        }
        //if (dS<0 || dS >8 || tK<1 || tK>3)
        return "O dia da semana e/ou o tipo de kit introduzidos são inválidos";
    }
}