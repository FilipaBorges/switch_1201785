package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Bloco2_Test {

    @Test
    void Exercicio1_test() {
        //arrange
        int nota1 = 14;
        int nota2 = 13;
        int nota3 = 7;
        int peso1 = 1;
        int peso2 = 4;
        int peso3 = 4;
        double esperado = 10;
        //act
        double resultado = Bloco2.Exercicio1(nota1, nota2, nota3, peso1, peso2, peso3);
        //assert
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void Exercicio1_test2() {
        //arrange
        int nota1 = 7;
        int nota2 = 16;
        int nota3 = 4;
        int peso1 = 3;
        int peso2 = 1;
        int peso3 = 4;
        double esperado = 6;
        //act
        double resultado = Bloco2.Exercicio1(nota1, nota2, nota3, peso1, peso2, peso3);
        //assert
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void Exercicio2_test1() {
        //arrange
        int numero = 224;
        int digito1 = 2;
        int digito2 = 2;
        int digito3 = 4;
        String esperada = "O número " + numero + " tem os seguintes digitos: " + digito1 + " " + digito2 + " " + digito3 + " e é um número par";
        //act
        String resultado = Bloco2.Exercicio2e(numero);
        //assert
        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio2_test2() {
        //arrange
        int numero = 30;

        String esperada = "O número " + numero + " não tem três dígitos.";
        //act
        String resultado = Bloco2.Exercicio2e(numero);
        //assert
        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio2_test3() {
        //arrange
        int numero = 325;
        int digito1 = 3;
        int digito2 = 2;
        int digito3 = 5;
        String esperada = "O número " + numero + " tem os seguintes digitos: " + digito1 + " " + digito2 + " " + digito3 + " e é um número ímpar";
        //act
        String resultado = Bloco2.Exercicio2e(numero);
        //assert
        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio2_test4() {
        //arrange
        int numero = 1002;

        String esperada = "O número " + numero + " não tem três dígitos.";
        //act
        String resultado = Bloco2.Exercicio2e(numero);
        //assert
        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio3_test1() {
        double x1 = 3;
        double y1 = 5;
        double x2 = 4;
        double y2 = 8;
        double esperado = 3.16;

        double resultado = Bloco2.Exercicio3_v2(x1, y1, x2, y2);

        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void Exercicio4_v2_test1() {
        double x = 0;
        double esperado = 0;

        double resultado = Bloco2.Exercicio4_v2(x);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio4_v2_test2() {
        double x = 4;
        double esperado = 8;

        double resultado = Bloco2.Exercicio4_v2(x);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio4_v2_test3() {
        double x = -2;
        double esperado = -2;

        double resultado = Bloco2.Exercicio4_v2(x);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5_v2_test1() {
        double area = 3;
        String esperado = "O volume é de:" + 0.35 + " e é um cubo pequeno.";

        String resultado = Bloco2.Exercicio5_v2(area);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5_v2_test2() {
        double area = 4000;
        String esperado = "O volume é de:" + 17213.26 + " e é um cubo grande.";

        String resultado = Bloco2.Exercicio5_v2(area);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5_v2_test3() {
        double area = 700;
        String esperado = "O volume é de:" + 1260.14 + " e é um cubo médio.";

        String resultado = Bloco2.Exercicio5_v2(area);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6_v2_testartemponegativo() {
        int tempoinicial = -120;
        String esperado = "Não existem horas negativas";

        String resultado = Bloco2.Exercicio6_v2(tempoinicial);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6_v2_testar3600segundos_1h() {
        int tempoinicial = 3600;
        double horas = 1;
        double minutos = 0;
        double segundos = 0;
        String esperado = "São " + 1 + ":" + 0 + ":" + 0 + " h";

        String resultado = Bloco2.Exercicio6_v2(tempoinicial);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6_v2_testar3724segundos() {
        int tempoinicial = 3724;
        int horas = 1;
        int minutos = 2;
        int segundos = 4;
        String esperado = "São " + 1 + ":" + 2 + ":" + 4 + " h";

        String resultado = Bloco2.Exercicio6_v2(tempoinicial);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6_v3_testarBomDia() {
        int tempoinicial = 30000;
        String esperado = "Bom dia!";

        String resultado = Bloco2.Exercicio6_v3(tempoinicial);

        assertEquals(esperado, resultado)
        ;
    }

    @Test
    void Exercicio6_v3_testarBoaTarde() {
        int tempoinicial = 60000;
        String esperado = "Boa tarde!";

        String resultado = Bloco2.Exercicio6_v3(tempoinicial);

        assertEquals(esperado, resultado)
        ;
    }

    @Test
    void Exercicio6_v3_testarBoaNoite() {
        int tempoinicial = 90000;
        String esperado = "Boa noite!";

        String resultado = Bloco2.Exercicio6_v3(tempoinicial);

        assertEquals(esperado, resultado)
        ;
    }

    @Test
    void Exercicio7_TestarValorNulo() {
        double areaEdificio = 0;
        double custoLTinta = 2.4;
        double rendimentoL = 1.2;
        double salarioDiaPintor = 40;
        double numeroPintores = 1;
        double diasnecessarios = 1;
        String esperado = "O custo total é Zero";

        String resultado = Bloco2.Exercicio7(areaEdificio, custoLTinta, rendimentoL, salarioDiaPintor, numeroPintores, diasnecessarios);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio7_TestarPrimeiraArea() {
        double areaEdificio = 90;
        double custoLTinta = 2.4;
        double rendimentoL = 1.2;
        double salarioDiaPintor = 40;
        double numeroPintores = 1;
        double diasnecessarios = 5.62;
        String esperado = "O Custo total da Obra é de: " + String.format("%.2f", 404.80) + "€";

        String resultado = Bloco2.Exercicio7(areaEdificio, custoLTinta, rendimentoL, salarioDiaPintor, numeroPintores, diasnecessarios);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio7_TestarSegundaArea() {
        double areaEdificio = 220;
        double custoLTinta = 2.4;
        double rendimentoL = 1.2;
        double salarioDiaPintor = 40;
        double numeroPintores = 2;
        double diasnecessarios = 6.87;
        String esperado = "O Custo total da Obra é de: " + String.format("%.2f", 989.60) + "€";

        String resultado = Bloco2.Exercicio7(areaEdificio, custoLTinta, rendimentoL, salarioDiaPintor, numeroPintores, diasnecessarios);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio8_testarPrimeiraCondicao() {
        int x = 20;
        int y = 4;
        String esperada = "Y é múltiplo de X";

        String resultado = Bloco2.Exercicio8(x, y);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio8_testarSegundaCondicao() {
        int x = 4;
        int y = 20;
        String esperada = "Y não é múltiplo de X";

        String resultado = Bloco2.Exercicio8(x, y);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio9_testarSequenciaCrescente() {
        int centenas = 2;
        int dezenas = 4;
        int unidades = 8;
        String esperada = "A sequência de números é crescente";

        String resultado = Bloco2.Exercicio9(centenas, dezenas, unidades);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio9_testarSequenciaDecrescente() {
        int centenas = 9;
        int dezenas = 5;
        int unidades = 3;
        String esperada = "A sequência de números é decrescente";

        String resultado = Bloco2.Exercicio9(centenas, dezenas, unidades);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio9_testarSemSequencia() {
        int centenas = 5;
        int dezenas = 9;
        int unidades = 3;
        String esperada = "A sequência de números não é crescente nem decrescente";

        String resultado = Bloco2.Exercicio9(centenas, dezenas, unidades);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio10_testarValorSuperior200() {
        double precoInicial = 250;
        double x = 0.4;
        String esperado = ("O preço final da peça é de:" + 100.0);

        String resultado = Bloco2.Exercicio10(precoInicial, x);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_testarValorInferior200MasMaior100() {
        double precoInicial = 125;
        double x = 0.6;
        String esperado = ("O preço final da peça é de:" + 75.0);

        String resultado = Bloco2.Exercicio10(precoInicial, x);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_testarValorInferior100MasMaior50() {
        double precoInicial = 72;
        double x = 0.7;
        String esperado = ("O preço final da peça é de:" + 50.4);

        String resultado = Bloco2.Exercicio10(precoInicial, x);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_testarValorInferior50() {
        double precoInicial = 22;
        double x = 0.8;
        String esperado = ("O preço final da peça é de:" + 17.6);

        String resultado = Bloco2.Exercicio10(precoInicial, x);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio11_valorinvalido() {
        double aprovados = -2;
        String esperada = "Valor Inválido";

        String resultado = Bloco2.Exercicio11(aprovados);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio11_valorinvalido1() {
        double aprovados = 1.2;
        String esperada = "Valor Inválido";

        String resultado = Bloco2.Exercicio11(aprovados);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio11_turmaMa() {
        double aprovados = 0.1;
        String esperada = "Turma Má";

        String resultado = Bloco2.Exercicio11(aprovados);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio11_turmaFraca() {
        double aprovados = 0.4;
        String esperada = "Turma Fraca";

        String resultado = Bloco2.Exercicio11(aprovados);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio11_turmaRazoavel() {
        double aprovados = 0.6;
        String esperada = "Turma Razoável";

        String resultado = Bloco2.Exercicio11(aprovados);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio11_turmaBoa() {
        double aprovados = 0.8;
        String esperada = "Turma Boa";

        String resultado = Bloco2.Exercicio11(aprovados);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio11_turmaExcelente() {
        double aprovados = 0.9;
        String esperada = "Turma Excelente";

        String resultado = Bloco2.Exercicio11(aprovados);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio12_testindiceinvalido() {
        double indicePoluicao = -2;
        String esperado = "O índice de poluição introduzido não é válido";

        String resultado = Bloco2.Exercicio12(indicePoluicao);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio12_testindiceaceitavel() {
        double indicePoluicao = 0.2;
        String esperado = "Índice de poluição aceitável.";

        String resultado = Bloco2.Exercicio12(indicePoluicao);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio12_testindiceaceitavelLimite() {
        double indicePoluicao = 0.3;
        String esperado = "Índice de poluição aceitável.";

        String resultado = Bloco2.Exercicio12(indicePoluicao);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio12_testPararPrimeiroGrupo() {
        double indicePoluicao = 0.35;
        String esperado = "Intimidar as indústrias do 1º grupo para suspender as suas actividades.";

        String resultado = Bloco2.Exercicio12(indicePoluicao);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio12_testPararPrimeiroESegundoGrupo() {
        double indicePoluicao = 0.5;
        String esperado = "Intimidar as indústrias do 1º e 2º grupo para suspender as suas actividades.";

        String resultado = Bloco2.Exercicio12(indicePoluicao);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio12_testPararTodosGrupo() {
        double indicePoluicao = 0.7;
        String esperado = "Intimidar as 3 empresas a suspender as suas actividades.";

        String resultado = Bloco2.Exercicio12(indicePoluicao);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio13_test1() {
        double m2 = 200;
        double nrArvores = 4;
        double nrArbustos = 10;
        int x = 1;
        String esperado = ("O tempo total para o serviço solicitado é de: " + 18.44);

        String resultado = Bloco2.Exercicio13(m2, nrArvores, nrArbustos, x);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio13_test2() {
        double m2 = 200;
        double nrArvores = 4;
        double nrArbustos = 10;
        int x = 2;
        String esperado = ("O custo total do serviço solicitado é de: " + 2414.44);

        String resultado = Bloco2.Exercicio13(m2, nrArvores, nrArbustos, x);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio13_test3() {
        double m2 = 200;
        double nrArvores = 4;
        double nrArbustos = 10;
        int x = 3;
        String esperado = ("A quantidade de horas para o serviço solicitado é de: " + 18.44 + " e o custo total é de:" + 2414.44);

        String resultado = Bloco2.Exercicio13(m2, nrArvores, nrArbustos, x);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio13_test4() {
        double m2 = 200;
        double nrArvores = 4;
        double nrArbustos = 10;
        int x = 7;
        String esperado = ("Caracter inválido.");

        String resultado = Bloco2.Exercicio13(m2, nrArvores, nrArbustos, x);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio14_test1() {
        int d1 = 6;
        int d2 = 8;
        int d3 = 7;
        int d4 = 6;
        int d5 = 2;
        String esperado = "A distância média percorrida pelo estafeta foi de: " + 9.3322;

        String resultado = Bloco2.Exercicio14(d1, d2, d3, d4, d5);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio14_test2() {
        double d1 = 6.9;
        double d2 = 8.7;
        double d3 = 5.2;
        double d4 = 6.7;
        double d5 = 2.1;
        String esperado = "A distância média percorrida pelo estafeta foi de: " + 9.52528;

        String resultado = Bloco2.Exercicio14(d1, d2, d3, d4, d5);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio14_test3() {
        double d1 = -9;
        double d2 = 8.7;
        double d3 = 5.2;
        double d4 = 6.7;
        double d5 = 2.1;
        String esperado = "Distância Inválida.";

        String resultado = Bloco2.Exercicio14(d1, d2, d3, d4, d5);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_testeInvalido() {
        double h = -2;
        double c1 = 1;
        double c2 = 1;
        String esperada = "Um triângulo com estas medidas é impossível.";

        String resultado = Bloco2.Exercicio15(h, c1, c2);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio15_testeInvalido2() {
        double h = 2;
        double c1 = -1;
        double c2 = 1;
        String esperada = "Um triângulo com estas medidas é impossível.";

        String resultado = Bloco2.Exercicio15(h, c1, c2);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio15_testeInvalido3() {
        double h = 2;
        double c1 = 1;
        double c2 = -1;
        String esperada = "Um triângulo com estas medidas é impossível.";

        String resultado = Bloco2.Exercicio15(h, c1, c2);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio15_testeInvalido4() {
        double h = 3;
        double c1 = 1;
        double c2 = 1;
        String esperada = "Um triângulo com estas medidas é impossível.";

        String resultado = Bloco2.Exercicio15(h, c1, c2);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio15_testeInvalido5() {
        double h = 2;
        double c1 = 7;
        double c2 = 1;
        String esperada = "Um triângulo com estas medidas é impossível.";

        String resultado = Bloco2.Exercicio15(h, c1, c2);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio15_testeInvalido6() {
        double h = 2;
        double c1 = 4;
        double c2 = 7;
        String esperada = "Um triângulo com estas medidas é impossível.";

        String resultado = Bloco2.Exercicio15(h, c1, c2);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio15_testeEquilatero() {
        double h = 6;
        double c1 = 6;
        double c2 = 6;
        String esperada = "O triângulo é equilátero.";

        String resultado = Bloco2.Exercicio15(h, c1, c2);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio15_testeEscaleno() {
        double h = 6;
        double c1 = 9;
        double c2 = 10;
        String esperada = "É um triângulo escaleno.";

        String resultado = Bloco2.Exercicio15(h, c1, c2);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio15_testeIsosceles() {
        double h = 6;
        double c1 = 6;
        double c2 = 10;
        String esperada = "É um triângulo isósceles.";

        String resultado = Bloco2.Exercicio15(h, c1, c2);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio16_testeimpossivel() {
        double a1 = -2;
        double a2 = 97;
        double a3 = 100;
        String esperada = "Este triângulo não é possível.";

        String resultado = Bloco2.Exercicio16(a1, a2, a3);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio16_testimpossivel1() {
        double a1 = 60;
        double a2 = 60;
        double a3 = 40;

        String esperada = "Este triângulo não é possível.";

        String resultado = Bloco2.Exercicio16(a1, a2, a3);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio16_testeimpossivel2() {
        double a1 = 40;
        double a2 = -97;
        double a3 = 100;
        String esperada = "Este triângulo não é possível.";

        String resultado = Bloco2.Exercicio16(a1, a2, a3);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio16_testeimpossivel3() {
        double a1 = 40;
        double a2 = 97;
        double a3 = -10;
        String esperada = "Este triângulo não é possível.";

        String resultado = Bloco2.Exercicio16(a1, a2, a3);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio16_TesteObtusangulo() {
        double a1 = 40;
        double a2 = 97;
        double a3 = 43;
        String esperada = "O triângulo é obtusângulo";

        String resultado = Bloco2.Exercicio16(a1, a2, a3);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio16_TesteRetangulo() {
        double a1 = 90;
        double a2 = 30;
        double a3 = 60;

        String esperada = "O triângulo é rectângulo";

        String resultado = Bloco2.Exercicio16(a1, a2, a3);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio16_TesteAcutangulo() {
        double a1 = 60;
        double a2 = 40;
        double a3 = 80;

        String esperada = "O triângulo é acutângulo";

        String resultado = Bloco2.Exercicio16(a1, a2, a3);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio17_TesteChegadaHoje() {
        int hp = 10;
        int mp = 32;
        int hd = 2;
        int md = 4;
        String esperada = "O comboio chega às " + 12 + ":" + 36 + "h do dia de hoje";

        String resultado = Bloco2.Exercicio17(hp, mp, hd, md);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio17_chegadaAmanha() {
        int hp = 10;
        int mp = 32;
        int hd = 14;
        int md = 4;
        String esperada = "O comboio chega às " + 00 + ":" + 36 + "h do dia de amanhã";

        String resultado = Bloco2.Exercicio17(hp, mp, hd, md);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio18_teste1() {
        int hi = 12;
        int mi = 4;
        int si = 30;
        int sprocessamento = 3600;
        String esperada = "13" + ":" + "4" + ":" + "30" + "h";

        String resultado = Bloco2.Exercicio18(hi, mi, si, sprocessamento);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio18_teste2() {
        int hi = 10;
        int mi = 4;
        int si = 32;
        int sprocessamento = 1406;
        String esperada = "10" + ":" + "27" + ":" + "58" + "h";

        String resultado = Bloco2.Exercicio18(hi, mi, si, sprocessamento);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio19_testeSemSalario() {
        double hT = 0;
        String esperada = "O empregado não trabalhou para ter salário.";

        String resultado = Bloco2.Exercicio19(hT);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio19_testeSalarioNas36h() {
        double hT = 28;
        String esperada = "O salário semanal é de: " + String.format("%.2f", 210.00);

        String resultado = Bloco2.Exercicio19(hT);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio19_testeSalarioNas5hExtra() {
        double hT = 39;
        String esperada = "O salário semanal é de: " + String.format("%.2f", 300.00);

        String resultado = Bloco2.Exercicio19(hT);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio19_testeSalarioApos5hExtra() {
        double hT = 43;
        String esperada = "O salário semanal é de: " + String.format("%.2f", 350.00);

        String resultado = Bloco2.Exercicio19(hT);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio20_testeDiaSemanaKit1() {
        int dS = 2;
        int tK = 1;
        String esperada = "O preço do aluguer é de 30€";

        String resultado = Bloco2.Exercicio20(dS, tK);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio20_testeDiaSemanaKit2() {
        int dS = 4;
        int tK = 2;
        String esperada = "O preço do aluguer é de 50€";

        String resultado = Bloco2.Exercicio20(dS, tK);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio20_testeDiaSemanaKit3() {
        int dS = 3;
        int tK = 3;
        String esperada = "O preço do aluguer é de 100€";

        String resultado = Bloco2.Exercicio20(dS, tK);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio20_testeFDSKit1() {
        int dS = 7;
        int tK = 1;
        String esperada = "O preço do aluguer é de 40€";

        String resultado = Bloco2.Exercicio20(dS, tK);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio20_testFDSKit2() {
        int dS = 6;
        int tK = 2;
        String esperada = "O preço do aluguer é de 70€";

        String resultado = Bloco2.Exercicio20(dS, tK);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio20_testFDSKit3() {
        int dS = 8;
        int tK = 3;
        String esperada = "O preço do aluguer é de 140€";

        String resultado = Bloco2.Exercicio20(dS, tK);

        assertEquals(esperada, resultado);
    }


    @Test
    void Exercicio20_testInvalido1() {
        int dS = 7;
        int tK = 4;
        String esperada = "O dia da semana e/ou o tipo de kit introduzidos são inválidos";

        String resultado = Bloco2.Exercicio20(dS, tK);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio20_testInvalido2() {
        int dS = 10;
        int tK = 3;
        String esperada = "O dia da semana e/ou o tipo de kit introduzidos são inválidos";

        String resultado = Bloco2.Exercicio20(dS, tK);

        assertEquals(esperada, resultado);
    }
}
