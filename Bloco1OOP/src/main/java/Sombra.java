public class Sombra {
    //Atributo
    private double sombra = -1;

    //Construtor
    public Sombra (double sombra) {

        if (!isSombraValid(sombra)) {
            throw new IllegalArgumentException("Sombra tem de ser superior a zero.");
        } else {
            this.sombra = sombra;
        }

    }

    //Método de validação
    private boolean isSombraValid (double sombra) {

        return sombra > 0;

    }

    public double obterSombra () {
        return this.sombra;
    }
}
