public class Edificio {
    //Atributos;
    //private double sombraEdificio = -1; //não é um atributo da classe porque varia e não é

    //Construtor
    public Edificio () {
/*
        if (sombraEdificio <= 0) {
            throw new IllegalArgumentException("Não pode ter valores iguais ou inferiores a zero.");
        } else {
            this.sombraEdificio = sombraEdificio;
        }*/
    }

    //Métodos
    public double obterAlturaEdificio (Sombra sombraEdificio, Pessoa pessoa, Sombra sombraPessoa) {

        double alturaEdificio = (sombraEdificio.obterSombra()*pessoa.obterAlturaPessoa())/sombraPessoa.obterSombra();

        return alturaEdificio;
    }
}
