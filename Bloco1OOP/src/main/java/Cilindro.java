public class Cilindro {
    //Declarar variáveis com inicialização de valores de default que são assumidos quando se inicia um objeto através de um construtor vazio
    private double raio = -1;
    private double altura = -1;

    //Construtor
    public Cilindro(double raio, double altura) {
        if(raio<0) {
            throw new IllegalArgumentException("O raio não pode ter valores negativos");
        }
        if(altura<0) {
            throw new IllegalArgumentException("A altura não pode ter valores negativos");
        }
        this.raio = raio;
        this.altura = altura;

    }

    public double obterVolume() {

        double areaBase = obterAreaBase(); //ou this.obterAreaBase();
        double volume = areaBase*altura;
        return volume;

    }

    private double obterAreaBase() { //porque não interessa chamarmos a partir do main
        return Math.PI*(raio*raio);
    }
}

