public class Main {

    public static void main(String[] args) {

        TeoremaTales teoremaTalesAlturaEdificio = new TeoremaTales(40, 2, 4);

        System.out.println(teoremaTalesAlturaEdificio.obterAlturaEdificio());

        Triangulo meuTriangulo = new Triangulo(4,4);

        System.out.println("O valor da hipotenusa é: " + Math.round(meuTriangulo.obterHipotenusa()));

        Retangulo novoRetangulo = new Retangulo(4,2);

        System.out.println(novoRetangulo.obterPerimetro());

        Pessoa pessoa_1 = new Pessoa(1.68);
        Edificio edificio_1 = new Edificio();
        Sombra sombraPessoa = new Sombra(2);
        Sombra sombraEdificio = new Sombra(20);

        System.out.println("Altura do edifício é: " + edificio_1.obterAlturaEdificio(sombraEdificio,pessoa_1,sombraPessoa));

        Cilindro myCilindro = new Cilindro(3,5);
        System.out.println(myCilindro.obterVolume());

        myCilindro = new Cilindro(2,5);
        System.out.println(myCilindro.obterVolume());
    }
}
