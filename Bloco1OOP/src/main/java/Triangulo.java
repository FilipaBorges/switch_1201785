public class Triangulo {
    private double primeiroCateto=-1;
    private double segundoCateto=-1;

    public Triangulo(double primeiroCateto, double segundoCateto) {
        if(primeiroCateto>0 && segundoCateto>0) {
            this.primeiroCateto = primeiroCateto;
            this.segundoCateto = segundoCateto;
        } else
            throw new IllegalArgumentException("O comprimento dos catetos tem que ser positivo");
    }

    public double obterHipotenusa() {
        double hipotenusa= Math.sqrt(Math.pow(primeiroCateto,2)+Math.pow(segundoCateto,2));
        return hipotenusa;
    }

}
