public class TeoremaTales {
    private double sombraEdificio;
    private double alturaPessoa;
    private double sombraPessoa;

    public TeoremaTales(double sombraEdificio, double alturaPessoa, double sombraPessoa) {
        this.sombraEdificio=sombraEdificio;
        this.alturaPessoa=alturaPessoa;
        this.sombraPessoa=sombraPessoa;
    }

    public double obterAlturaEdificio() {
        double alturaEdificio= (sombraEdificio/sombraPessoa)*alturaPessoa;
        return alturaEdificio;
    }

}
