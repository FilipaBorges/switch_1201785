public class Retangulo {
    private double comprimento;
    private double altura;

    public Retangulo(double comprimento, double altura) {
        this.comprimento=comprimento;
        this.altura=altura;
    }

    public double obterPerimetro() {
        double perimetro= 2*comprimento+2*altura;
        return perimetro;
    }
}
