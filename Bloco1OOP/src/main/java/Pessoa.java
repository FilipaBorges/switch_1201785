public class Pessoa {
    //Atributos
    private double alturaPessoa = -1;
    //private double sombraPessoa = -1;

    //Construtor
    public Pessoa (double alturaPessoa) {

        if (alturaPessoa <= 0) {
            throw new IllegalArgumentException("Não pode ter valores iguais ou inferiores a zero.");
        } else {
            this.alturaPessoa = alturaPessoa;
        }
    }

    //Métodos

    public double obterAlturaPessoa () {
        return this.alturaPessoa;
    }
/*
    public double obterSombraPessoa () {
        return this.sombraPessoa;
    }*/
}
