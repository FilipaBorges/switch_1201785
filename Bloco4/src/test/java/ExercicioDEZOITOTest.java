import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDEZOITOTest {

    @Test
    void Exercicio18_numeroColunas() {
        char [][] matriz={{'a','b','c'},{'d','e','f'}};
        int esperado=3;

        int resultado=ExercicioDEZOITO.numeroColunas(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void verificaMatrizQuadrada() {
        char [][] matriz= {{'a','c','c'},{'a','e','i'},{'a','a','b'}};

        boolean resultado= ExercicioDEZOITO.verificaMatrizQuadrada(matriz);

        assertTrue(resultado);
    }

    @Test
    void verificaMatrizNaoQuadrada() {
        char [][] matriz= {{'a','c','c'},{'a','a','b'}};

        boolean resultado= ExercicioDEZOITO.verificaMatrizQuadrada(matriz);

        assertFalse(resultado);
    }

    @Test
    void Exercicio18_numeroColunasOito() {
        char [][] matriz={{'a','b','c','d','e','f','g','h'},{'d','e','f','g','h','i','j','k'}};
        int esperado=8;

        int resultado=ExercicioDEZOITO.numeroColunas(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio18_matrizMascaraLetraA() {
        char [][] matriz= {{'e','i','f'},{'a','b','c'},{'e','a','f'}};
        char letra = 'a';
        int [][] esperado= {{0,0,0},{1,0,0},{0,1,0}};

        int[][]resultado=ExercicioDEZOITO.matrizMascaraParaLetra(matriz, letra);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio18_matrizNaoQuadrada() {
        char [][] matriz={{'a','e','g'},{'f','e','e'},{'i'}};
        char letra= 'a';
        int [][] esperado=null;

        int[][] resultado= ExercicioDEZOITO.matrizMascaraParaLetra(matriz, letra);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio18_matrizMascaraLetraB() {
        char [][] matriz= {{'b','i','f','e'},{'a','b','c','p'},{'e','a','f','l'},{'k','b','a','t'}};
        char letra = 'b';
        int [][] esperado= {{1,0,0,0},{0,1,0,0},{0,0,0,0},{0,1,0,0}};

        int[][]resultado=ExercicioDEZOITO.matrizMascaraParaLetra(matriz, letra);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void convertePalavraFILIPAParaArray() {
        String palavra= "FILIPA";
        char [] esperado= {'F','I','L','I','P','A'};

        char [] resultado= ExercicioDEZOITO.convertePalavraDeStringParaArray(palavra);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void convertePalavraParaArray() {
        String palavra= "";
        char [] esperado= {};

        char [] resultado= ExercicioDEZOITO.convertePalavraDeStringParaArray(palavra);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificaNumero1NaMatrizMascara() {
        int [][] matrizMascara= {{0,0,0,0},{0,0,0,0},{0,0,0,1},{0,0,0,0}};

        boolean resultado= ExercicioDEZOITO.existeNumeroUm(matrizMascara);

        assertTrue(resultado);
    }

    @Test
    void verificaNumeroUmNaMatrizMascara() {
        int [][] matrizMascara= {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};

        boolean resultado= ExercicioDEZOITO.existeNumeroUm(matrizMascara);

        assertFalse(resultado);
    }

    @Test
    void ExistePalavraEsquerdaParaDireitaPalavraPrimeiraLinha() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','p','a'},
                {'a','f','b','f','p','l','i','a','a'},
                {'a','f','b','i','i','l','i','p','a'},
                {'a','f','b','d','i','l','i','p','a'},
                {'a','c','b','f','i','l','g','p','a'},
                {'a','f','b','f','i','l','i','p','a'},
                {'a','e','b','d','i','l','i','p','a'},
                {'a','f','b','p','i','l','i','p','a'},
                {'a','a','b','f','i','l','i','p','a'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteDaEsquerdaParaDireita(matrizLetras,palavra);

        assertTrue(resultado);
    }

    @Test
    void ExistePalavraEsquerdaParaDireitaPalavraUltimaLinha() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','f','b','f','p','l','i','a','a'},
                {'a','f','b','i','i','l','i','p','a'},
                {'a','f','b','d','i','l','i','p','a'},
                {'a','c','b','f','i','l','g','p','a'},
                {'a','f','b','f','i','l','i','p','a'},
                {'a','e','b','d','i','l','i','p','a'},
                {'a','f','b','p','i','l','i','p','a'},
                {'a','a','f','i','l','i','p','a','a'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteDaEsquerdaParaDireita(matrizLetras,palavra);

        assertTrue(resultado);
    }

    @Test
    void naoExistePalavraEsquerdaParaDireita() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','f','b','f','p','l','i','a','a'},
                {'a','f','b','i','i','l','i','p','a'},
                {'a','f','b','d','i','l','i','p','a'},
                {'a','c','b','f','i','l','g','p','a'},
                {'a','f','b','f','i','i','i','p','a'},
                {'a','e','b','d','i','l','i','p','a'},
                {'a','f','b','p','i','l','i','p','a'},
                {'a','a','f','i','l','p','p','a','a'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteDaEsquerdaParaDireita(matrizLetras,palavra);

        assertFalse(resultado);
    }

    @Test
    void existePalavraDireitaParaEsquerda() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','f','b','f','p','l','i','a','a'},
                {'a','p','i','l','i','f','i','p','a'},
                {'a','f','b','d','i','l','i','p','a'},
                {'a','c','b','f','i','l','g','p','a'},
                {'a','f','b','f','i','i','i','p','a'},
                {'a','e','b','d','i','l','i','p','a'},
                {'a','f','b','p','i','l','i','p','a'},
                {'a','a','f','i','l','p','p','a','a'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteDaDireitaParaEsquerda(matrizLetras,palavra);

        assertTrue(resultado);
    }

    @Test
    void existePalavraDireitaParaEsquerdaUltimaLinha() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','f','b','f','p','l','i','a','a'},
                {'a','p','a','l','i','f','i','p','a'},
                {'a','f','b','d','i','l','i','p','a'},
                {'a','c','b','p','i','l','i','f','a'},
                {'a','f','b','f','i','i','i','p','a'},
                {'a','e','b','d','i','l','i','p','a'},
                {'a','f','b','p','i','l','i','p','a'},
                {'a','a','p','i','l','i','f','a','a'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteDaDireitaParaEsquerda(matrizLetras,palavra);

        assertTrue(resultado);
    }

    @Test
    void existePalavraCimaParaBaixo() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','f','b','f','i','l','i','p','a'},
                {'a','i','b','i','i','l','i','p','a'},
                {'a','l','b','f','i','l','i','p','a'},
                {'a','i','b','i','i','l','g','p','a'},
                {'a','l','b','l','i','i','i','p','a'},
                {'a','i','b','i','i','l','i','p','a'},
                {'a','f','b','p','i','l','i','p','a'},
                {'a','a','f','a','l','p','p','a','a'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteDeCimaParaBaixo(matrizLetras,palavra);

        assertTrue(resultado);
    }

    @Test
    void existePalavraCimaParaBaixoUltimaLinha() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','f','b','f','i','l','i','p','a'},
                {'a','i','b','i','i','l','i','p','a'},
                {'a','l','b','f','i','l','i','p','f'},
                {'a','i','b','i','i','l','g','p','i'},
                {'a','l','b','l','i','i','i','p','l'},
                {'a','i','b','p','i','l','i','p','i'},
                {'a','f','b','p','i','l','i','p','p'},
                {'a','a','f','a','l','p','p','a','a'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteDeCimaParaBaixo(matrizLetras,palavra);

        assertTrue(resultado);
    }

    @Test
    void naoExistePalavraCimaParaBaixo() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','f','b','f','i','l','i','p','a'},
                {'a','i','b','i','i','l','i','p','a'},
                {'a','l','b','f','i','l','i','p','f'},
                {'a','i','b','i','i','l','g','p','i'},
                {'a','l','b','l','i','i','i','p','l'},
                {'a','i','b','p','i','l','i','p','o'},
                {'a','f','b','p','i','l','i','p','p'},
                {'a','a','f','a','l','p','p','a','a'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteDeCimaParaBaixo(matrizLetras,palavra);

        assertFalse(resultado);
    }

    @Test
    void existePalavraBaixoParaCima() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','f','b','f','p','l','i','a','a'},
                {'a','p','a','l','i','f','i','p','a'},
                {'p','f','b','d','i','l','i','p','a'},
                {'i','c','b','p','i','l','i','f','a'},
                {'l','f','b','f','i','i','i','p','a'},
                {'i','e','b','d','i','l','i','p','a'},
                {'f','f','b','p','i','l','i','p','a'},
                {'a','a','p','i','l','i','f','a','a'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteDaDireitaParaEsquerda(matrizLetras,palavra);

        assertTrue(resultado);
    }

    @Test
    void naoExistePalavraBaixoParaCima() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','f','b','f','i','l','i','p','a'},
                {'a','f','b','i','i','l','i','p','a'},
                {'a','p','b','f','i','l','i','p','a'},
                {'a','i','b','i','i','l','g','p','a'},
                {'a','l','b','l','i','i','i','p','a'},
                {'a','i','b','i','i','l','i','p','a'},
                {'a','f','b','p','i','l','i','p','a'},
                {'a','a','f','a','l','p','p','a','a'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteBaixoParaCima(matrizLetras,palavra);

        assertFalse(resultado);
    }

    @Test
    void existePalavraBaixoParaCimaUltimaColuna() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','f','b','f','p','l','i','a','p'},
                {'a','p','a','l','i','f','i','p','i'},
                {'p','f','b','d','i','l','i','p','l'},
                {'p','c','b','p','i','l','i','f','i'},
                {'l','f','b','f','i','i','i','p','f'},
                {'i','e','b','d','i','l','i','p','a'},
                {'f','f','b','p','i','l','i','p','a'},
                {'a','a','p','i','l','i','f','a','a'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteDaDireitaParaEsquerda(matrizLetras,palavra);

        assertTrue(resultado);
    }

    @Test
    void existePalavraDiagonalDescendenteDireita() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','f','b','f','p','l','i','a','p'},
                {'a','p','a','l','i','f','i','p','i'},
                {'p','f','b','f','i','l','i','p','l'},
                {'p','c','b','p','i','l','i','f','i'},
                {'l','f','b','f','i','l','i','t','f'},
                {'i','e','b','d','i','l','i','p','a'},
                {'f','f','b','p','i','l','i','p','a'},
                {'a','a','p','i','l','i','f','a','a'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteDiagonalDescendenteDireita(matrizLetras,palavra);

        assertTrue(resultado);
    }

    @Test
    void naoExistePalavraDiagonalDescendenteDireita() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','f','b','f','p','l','i','a','p'},
                {'a','p','a','l','i','f','i','p','i'},
                {'p','f','b','f','i','l','i','p','l'},
                {'p','c','b','p','i','l','i','f','i'},
                {'l','f','b','f','i','l','i','t','f'},
                {'i','e','b','d','i','l','i','p','a'},
                {'f','f','b','p','i','l','i','p','a'},
                {'a','a','p','i','l','i','f','a','p'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteDiagonalDescendenteDireita(matrizLetras,palavra);

        assertFalse(resultado);
    }

    @Test
    void existePalavraDiagonalDescendenteEsquerda() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','f','b','f','p','l','i','a','p'},
                {'a','p','a','l','i','f','i','p','i'},
                {'p','f','b','f','i','f','i','p','l'},
                {'p','c','b','p','i','l','i','f','i'},
                {'l','f','b','l','i','l','i','t','f'},
                {'i','e','i','d','i','l','i','p','a'},
                {'f','p','b','p','i','l','i','p','a'},
                {'a','a','p','i','l','i','f','a','p'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteDiagonalDescendenteEsquerda(matrizLetras,palavra);

        assertTrue(resultado);
    }

    @Test
    void existePalavraDiagonalDescendenteEsquerda1() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','f','b','f','p','l','i','a','p'},
                {'a','p','a','l','i','f','i','p','i'},
                {'p','f','b','f','i','a','i','p','f'},
                {'p','c','b','p','i','l','i','i','i'},
                {'l','f','b','l','i','l','l','t','f'},
                {'i','e','i','d','i','i','i','p','a'},
                {'f','p','b','p','p','l','i','p','a'},
                {'a','a','p','a','l','i','f','a','p'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteDiagonalDescendenteEsquerda(matrizLetras,palavra);

        assertTrue(resultado);
    }

    @Test
    void naoExistePalavraDiagonasDescendenteEsquerda() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','f','b','f','p','l','i','a','p'},
                {'a','p','a','l','i','f','i','p','i'},
                {'p','f','b','f','i','a','i','p','f'},
                {'p','c','b','p','i','l','i','a','i'},
                {'l','f','b','l','i','l','l','t','f'},
                {'i','e','i','d','i','i','i','p','a'},
                {'f','p','b','p','p','l','i','p','a'},
                {'a','a','p','a','l','i','f','a','p'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteDiagonalDescendenteEsquerda(matrizLetras,palavra);

        assertFalse(resultado);
    }

    @Test
    void existePalavraDiagonalAscendenteDireita() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','f','b','f','p','l','i','a','p'},
                {'a','p','a','l','i','a','i','p','i'},
                {'p','f','b','f','p','a','i','p','f'},
                {'p','c','b','i','i','l','i','i','i'},
                {'l','f','l','l','i','l','l','t','f'},
                {'i','i','i','d','i','i','i','p','a'},
                {'f','p','b','p','a','l','i','p','a'},
                {'f','a','p','a','l','i','f','a','p'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteDiagonalAscendenteDireita(matrizLetras,palavra);

        assertTrue(resultado);
    }

    @Test
    void existePalavraDiagonalAscendenteDireitaUltimaLinha() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','f','b','f','p','l','i','p','p'},
                {'a','p','a','l','i','a','i','p','i'},
                {'p','f','b','f','p','l','i','p','f'},
                {'p','c','b','i','i','l','i','i','i'},
                {'l','f','o','f','i','l','l','t','f'},
                {'i','i','i','d','i','i','i','p','a'},
                {'f','p','b','p','p','l','i','p','a'},
                {'f','a','p','a','l','i','f','a','p'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteDiagonalAscendenteDireita(matrizLetras,palavra);

        assertTrue(resultado);
    }

    @Test
    void naoExistePalavraDiagonalAscendenteDireita() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','f','b','f','p','l','i','p','p'},
                {'a','p','a','l','i','a','m','p','i'},
                {'p','f','b','f','p','l','i','p','f'},
                {'p','c','b','i','i','l','i','i','i'},
                {'l','f','o','f','i','l','l','t','f'},
                {'i','i','i','d','i','i','i','p','a'},
                {'f','p','b','p','p','l','i','p','a'},
                {'f','a','p','a','l','i','f','a','p'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteDiagonalAscendenteDireita(matrizLetras,palavra);

        assertFalse(resultado);
    }

    @Test
    void existePalavraDiagonalAscendenteEsquerdaLimite() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','p','b','f','p','l','i','p','p'},
                {'a','p','i','l','i','a','i','p','i'},
                {'p','f','b','l','p','l','i','p','f'},
                {'p','c','b','i','i','l','i','i','i'},
                {'l','f','o','f','i','f','l','t','f'},
                {'i','i','i','d','i','i','i','p','a'},
                {'f','p','b','p','p','l','i','p','a'},
                {'f','a','p','a','l','i','f','a','p'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteDiagonalAscendenteEsquerda(matrizLetras,palavra);

        assertTrue(resultado);
    }

    @Test
    void existePalavraDiagonalAscendenteEsquerda() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','a','a','f','p','l','i','p','p'},
                {'a','p','i','p','i','a','i','p','i'},
                {'p','f','b','l','i','l','i','p','f'},
                {'p','c','b','i','i','l','i','i','i'},
                {'l','f','o','f','i','f','i','t','f'},
                {'i','i','i','d','i','i','i','f','a'},
                {'f','p','b','p','p','l','i','p','a'},
                {'f','a','p','a','l','i','f','a','p'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteDiagonalAscendenteEsquerda(matrizLetras,palavra);

        assertTrue(resultado);
    }

    @Test
    void naoExistePalavraDiagonalAscendenteEsquerda() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','a','a','f','p','l','i','p','p'},
                {'a','p','i','p','i','a','i','p','i'},
                {'p','f','b','l','i','l','i','p','f'},
                {'p','c','b','i','i','z','i','i','i'},
                {'l','f','o','f','i','f','i','t','f'},
                {'i','i','i','d','i','i','i','f','a'},
                {'f','p','b','p','p','l','i','p','a'},
                {'f','a','p','a','l','i','f','a','p'}};
        char [] palavra= {'f','i','l','i','p','a'};

        boolean resultado = ExercicioDEZOITO.palavraExisteDiagonalAscendenteEsquerda(matrizLetras,palavra);

        assertFalse(resultado);
    }

    @Test
    void verificarSePalavraExisteEmQualquerDirecaoMatrizMenorQuePalavra() {
        char [][] matrizLetras= {{'a','f','b','f'},
                {'i','i','i','d'},
                {'f','p','b','p'},
                {'f','a','p','a'}};
        String palavra="filipa";

        boolean resultado= ExercicioDEZOITO.verificaPalavra(matrizLetras, palavra);

        assertFalse(resultado);
    }

    @Test
    void verificarSePalavraExisteEmQualquerDirecaoMatrizNaoQuadrada() {
        char [][] matrizLetras= {{'a','f','b','f'},
                {'i','i','i','d'},
                {'f','p','b','p'},
                {'f','a','p','a'},
                {'f','i','l','i'}};
        String palavra="filipa";

        boolean resultado= ExercicioDEZOITO.verificaPalavra(matrizLetras, palavra);

        assertFalse(resultado);
    }

    @Test
    void verificaPalavraEmQualquerDirecaoSemPalavra() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'a','a','a','f','p','l','i','p','p'},
                {'a','p','i','p','i','a','z','p','i'},
                {'p','f','b','l','i','l','i','p','f'},
                {'p','c','b','i','i','z','i','i','i'},
                {'l','f','o','f','i','f','i','t','f'},
                {'i','i','i','d','i','i','i','f','a'},
                {'f','p','b','p','p','l','i','p','a'},
                {'f','a','p','a','l','i','f','a','p'}};
        String palavra="filipa";

        boolean resultado = ExercicioDEZOITO.verificaPalavra(matrizLetras, palavra);

        assertFalse(resultado);
    }

    @Test
    void verificaPalavraEmQualquerDirecaoPalavraPrograma() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'p','r','o','g','r','a','m','a','p'},
                {'a','p','i','p','i','a','m','p','i'},
                {'p','f','b','l','i','a','i','p','f'},
                {'p','c','b','i','r','z','i','i','i'},
                {'l','f','o','g','i','f','i','t','f'},
                {'i','i','o','d','i','i','i','f','a'},
                {'f','r','b','p','p','l','i','p','a'},
                {'p','a','p','a','l','i','f','a','p'}};
        String palavra="programa";

        boolean resultado = ExercicioDEZOITO.verificaPalavra(matrizLetras, palavra);

        assertTrue(resultado);
    }

    @Test
    void verificaPalavraEmQualquerDirecaoPalavraEu() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'p','r','o','e','r','a','m','a','p'},
                {'a','p','i','u','i','a','m','p','i'},
                {'p','f','b','l','i','a','i','p','f'},
                {'p','c','b','i','r','z','i','i','i'},
                {'l','f','o','g','i','f','i','t','f'},
                {'i','i','o','d','i','i','i','f','a'},
                {'f','r','b','p','p','l','i','p','a'},
                {'p','a','p','a','l','i','f','a','p'}};
        String palavra="eu";

        boolean resultado = ExercicioDEZOITO.verificaPalavra(matrizLetras, palavra);

        assertTrue(resultado);
    }

    @Test
    void verificaPalavraEmQualquerDirecaoPalavraEuUltimaLinhaEColuna() {
        char [][] matrizLetras= {{'a','f','b','f','i','l','i','a','a'},
                {'p','r','o','e','r','a','m','a','p'},
                {'a','p','i','o','i','a','m','p','i'},
                {'p','f','b','l','i','a','i','p','f'},
                {'p','c','b','i','r','z','i','i','i'},
                {'l','f','o','g','i','f','i','t','f'},
                {'i','i','o','d','i','i','i','f','a'},
                {'f','r','b','p','p','l','i','u','a'},
                {'p','a','p','a','l','i','f','a','e'}};
        String palavra="eu";

        boolean resultado = ExercicioDEZOITO.verificaPalavra(matrizLetras, palavra);

        assertTrue(resultado);
    }

    @Test
    void verificaPalavraEsquerdaDireitaImpossivel() {
        char [][] matrizLetras={{'a','f','b','f','i','l','i','a','a'},
                {'p','r','o','e','r','a','m','a','p'},
                {'a','p','i','o','i','a','m','p','i'},
                {'p','f','b','l','i','a','i','p','f'},
                {'p','c','b','i','r','z','i','i','i'},
                {'l','f','o','g','i','f','i','t','f'},
                {'i','i','o','d','i','i','i','f','a'},
                {'f','r','b','p','p','l','i','u','a'},
                {'p','a','p','a','l','i','f','a','e'}};
        char [] palavra= {'f','i','l','i','p','a'};
        int linha1= 1;
        int coluna1=2;
        int linha2= 1;
        int coluna2=4;

        boolean resultado= ExercicioDEZOITO.palavraEPossivelNasCoordenadasDadas(matrizLetras, palavra, linha1, coluna1, linha2, coluna2);

        assertFalse(resultado);
    }

    @Test
    void verificaPalavraEsquerdaDireitaPossivel() {
        char [][] matrizLetras={{'a','f','b','f','i','l','i','a','a'},
                {'p','r','o','e','r','a','m','a','p'},
                {'a','p','i','o','i','a','m','p','i'},
                {'p','f','b','l','i','a','i','p','f'},
                {'p','c','b','i','r','z','i','i','i'},
                {'l','f','o','g','i','f','i','t','f'},
                {'i','i','o','d','i','i','i','f','a'},
                {'f','r','b','p','p','l','i','u','a'},
                {'p','a','p','a','l','i','f','a','e'}};
        char [] palavra= {'f','i','l','i','p','a'};
        int linha1= 1;
        int coluna1=0;
        int linha2= 1;
        int coluna2= 5;

        boolean resultado= ExercicioDEZOITO.palavraEPossivelNasCoordenadasDadas(matrizLetras, palavra, linha1, coluna1, linha2, coluna2);

        assertTrue(resultado);
    }

    @Test
    void verificaPalavraDireitaEsquerdaImpossivel() {
        char [][] matrizLetras={{'a','f','b','f','i','l','i','a','a'},
                {'p','r','o','e','r','a','m','a','p'},
                {'a','p','i','o','i','a','m','p','i'},
                {'p','f','b','l','i','a','i','p','f'},
                {'p','c','b','i','r','z','i','i','i'},
                {'l','f','o','g','i','f','i','t','f'},
                {'i','i','o','d','i','i','i','f','a'},
                {'f','r','b','p','p','l','i','u','a'},
                {'p','a','p','a','l','i','f','a','e'}};
        char [] palavra= {'f','i','l','i','p','a'};
        int linha1= 1;
        int coluna1=5;
        int linha2= 1;
        int coluna2= 3;

        boolean resultado= ExercicioDEZOITO.palavraEPossivelNasCoordenadasDadas(matrizLetras, palavra, linha1, coluna1, linha2, coluna2);

        assertFalse(resultado);
    }

    @Test
    void verificaPalavraDireitaEsquerdaPossivel() {
        char [][] matrizLetras={{'a','f','b','f','i','l','i','a','a'},
                {'p','r','o','e','r','a','m','a','p'},
                {'a','p','i','o','i','a','m','p','i'},
                {'p','f','b','l','i','a','i','p','f'},
                {'p','c','b','i','r','z','i','i','i'},
                {'l','f','o','g','i','f','i','t','f'},
                {'i','i','o','d','i','i','i','f','a'},
                {'f','r','b','p','p','l','i','u','a'},
                {'p','a','p','a','l','i','f','a','e'}};
        char [] palavra= {'f','i','l','i','p','a'};
        int linha1= 1;
        int coluna1=7;
        int linha2= 1;
        int coluna2= 2;

        boolean resultado= ExercicioDEZOITO.palavraEPossivelNasCoordenadasDadas(matrizLetras, palavra, linha1, coluna1, linha2, coluna2);

        assertTrue(resultado);
    }

    @Test
    void verificaPalavraCimaBaixoImpossivel() {
        char [][] matrizLetras={{'a','f','b','f','i','l','i','a','a'},
                {'p','r','o','e','r','a','m','a','p'},
                {'a','p','i','o','i','a','m','p','i'},
                {'p','f','b','l','i','a','i','p','f'},
                {'p','c','b','i','r','z','i','i','i'},
                {'l','f','o','g','i','f','i','t','f'},
                {'i','i','o','d','i','i','i','f','a'},
                {'f','r','b','p','p','l','i','u','a'},
                {'p','a','p','a','l','i','f','a','e'}};
        char [] palavra= {'f','i','l','i','p','a'};
        int linha1= 1;
        int coluna1=2;
        int linha2= 3;
        int coluna2= 2;

        boolean resultado= ExercicioDEZOITO.palavraEPossivelNasCoordenadasDadas(matrizLetras, palavra, linha1, coluna1, linha2, coluna2);

        assertFalse(resultado);
    }

    @Test
    void verificaPalavraCimaBaixoPossivel() {
        char [][] matrizLetras={{'a','f','b','f','i','l','i','a','a'},
                {'p','r','o','e','r','a','m','a','p'},
                {'a','p','i','o','i','a','m','p','i'},
                {'p','f','b','l','i','a','i','p','f'},
                {'p','c','b','i','r','z','i','i','i'},
                {'l','f','o','g','i','f','i','t','f'},
                {'i','i','o','d','i','i','i','f','a'},
                {'f','r','b','p','p','l','i','u','a'},
                {'p','a','p','a','l','i','f','a','e'}};
        char [] palavra= {'f','i','l','i','p','a'};
        int linha1= 3;
        int coluna1=2;
        int linha2= 8;
        int coluna2= 2;

        boolean resultado= ExercicioDEZOITO.palavraEPossivelNasCoordenadasDadas(matrizLetras, palavra, linha1, coluna1, linha2, coluna2);

        assertTrue(resultado);
    }

    @Test
    void verificaPalavraBaixoCimaImpossivel() {
        char [][] matrizLetras={{'a','f','b','f','i','l','i','a','a'},
                {'p','r','o','e','r','a','m','a','p'},
                {'a','p','i','o','i','a','m','p','i'},
                {'p','f','b','l','i','a','i','p','f'},
                {'p','c','b','i','r','z','i','i','i'},
                {'l','f','o','g','i','f','i','t','f'},
                {'i','i','o','d','i','i','i','f','a'},
                {'f','r','b','p','p','l','i','u','a'},
                {'p','a','p','a','l','i','f','a','e'}};
        char [] palavra= {'f','i','l','i','p','a'};
        int linha1= 8;
        int coluna1=2;
        int linha2= 6;
        int coluna2= 2;

        boolean resultado= ExercicioDEZOITO.palavraEPossivelNasCoordenadasDadas(matrizLetras, palavra, linha1, coluna1, linha2, coluna2);

        assertFalse(resultado);
    }

    @Test
    void verificaPalavraBaixoCimaPossivel() {
        char [][] matrizLetras={{'a','f','b','f','i','l','i','a','a'},
                {'p','r','o','e','r','a','m','a','p'},
                {'a','p','i','o','i','a','m','p','i'},
                {'p','f','b','l','i','a','i','p','f'},
                {'p','c','b','i','r','z','i','i','i'},
                {'l','f','o','g','i','f','i','t','f'},
                {'i','i','o','d','i','i','i','f','a'},
                {'f','r','b','p','p','l','i','u','a'},
                {'p','a','p','a','l','i','f','a','e'}};
        char [] palavra= {'f','i','l','i','p','a'};
        int linha1= 7;
        int coluna1=2;
        int linha2= 2;
        int coluna2= 2;

        boolean resultado= ExercicioDEZOITO.palavraEPossivelNasCoordenadasDadas(matrizLetras, palavra, linha1, coluna1, linha2, coluna2);

        assertTrue(resultado);
    }

    @Test
    void verificaPalavraDiagonalDescendenteDireitaImpossivel() {
        char [][] matrizLetras={{'a','f','b','f','i','l','i','a','a'},
                {'p','r','o','e','r','a','m','a','p'},
                {'a','p','i','o','i','a','m','p','i'},
                {'p','f','b','l','i','a','i','p','f'},
                {'p','c','b','i','r','z','i','i','i'},
                {'l','f','o','g','i','f','i','t','f'},
                {'i','i','o','d','i','i','i','f','a'},
                {'f','r','b','p','p','l','i','u','a'},
                {'p','a','p','a','l','i','f','a','e'}};
        char [] palavra= {'f','i','l','i','p','a'};
        int linha1= 1;
        int coluna1=1;
        int linha2= 3;
        int coluna2= 4;

        boolean resultado= ExercicioDEZOITO.palavraEPossivelNasCoordenadasDadas(matrizLetras, palavra, linha1, coluna1, linha2, coluna2);

        assertFalse(resultado);
    }

    @Test
    void verificaPalavraDiagonalDescendenteDireitaPossivel() {
        char [][] matrizLetras={{'a','f','b','f','i','l','i','a','a'},
                {'p','r','o','e','r','a','m','a','p'},
                {'a','p','i','o','i','a','m','p','i'},
                {'p','f','b','l','i','a','i','p','f'},
                {'p','c','b','i','r','z','i','i','i'},
                {'l','f','o','g','i','f','i','t','f'},
                {'i','i','o','d','i','i','i','f','a'},
                {'f','r','b','p','p','l','i','u','a'},
                {'p','a','p','a','l','i','f','a','e'}};
        char [] palavra= {'f','i','l','i','p','a'};
        int linha1= 1;
        int coluna1=2;
        int linha2= 6;
        int coluna2= 7;

        boolean resultado= ExercicioDEZOITO.palavraEPossivelNasCoordenadasDadas(matrizLetras, palavra, linha1, coluna1, linha2, coluna2);

        assertTrue(resultado);
    }

    @Test
    void verificaPalavraDiagonalDescendenteEsquerdaImpossivel() {
        char [][] matrizLetras={{'a','f','b','f','i','l','i','a','a'},
                {'p','r','o','e','r','a','m','a','p'},
                {'a','p','i','o','i','a','m','p','i'},
                {'p','f','b','l','i','a','i','p','f'},
                {'p','c','b','i','r','z','i','i','i'},
                {'l','f','o','g','i','f','i','t','f'},
                {'i','i','o','d','i','i','i','f','a'},
                {'f','r','b','p','p','l','i','u','a'},
                {'p','a','p','a','l','i','f','a','e'}};
        char [] palavra= {'f','i','l','i','p','a'};
        int linha1= 1;
        int coluna1=5;
        int linha2= 3;
        int coluna2= 4;

        boolean resultado= ExercicioDEZOITO.palavraEPossivelNasCoordenadasDadas(matrizLetras, palavra, linha1, coluna1, linha2, coluna2);

        assertFalse(resultado);
    }

    @Test
    void verificaPalavraDiagonalDescendenteEsquerdaPossivel() {
        char [][] matrizLetras={{'a','f','b','f','i','l','i','a','a'},
                {'p','r','o','e','r','a','m','a','p'},
                {'a','p','i','o','i','a','m','p','i'},
                {'p','f','b','l','i','a','i','p','f'},
                {'p','c','b','i','r','z','i','i','i'},
                {'l','f','o','g','i','f','i','t','f'},
                {'i','i','o','d','i','i','i','f','a'},
                {'f','r','b','p','p','l','i','u','a'},
                {'p','a','p','a','l','i','f','a','e'}};
        char [] palavra= {'f','i','l','i','p','a'};
        int linha1= 0;
        int coluna1=1;
        int linha2= 5;
        int coluna2= 6;

        boolean resultado= ExercicioDEZOITO.palavraEPossivelNasCoordenadasDadas(matrizLetras, palavra, linha1, coluna1, linha2, coluna2);

        assertTrue(resultado);
    }

    @Test
    void verificaPalavraDiagonalAscendenteDireitaImpossivel() {
        char [][] matrizLetras={{'a','f','b','f','i','l','i','a','a'},
                {'p','r','o','e','r','a','m','a','p'},
                {'a','p','i','o','i','a','m','p','i'},
                {'p','f','b','l','i','a','i','p','f'},
                {'p','c','b','i','r','z','i','i','i'},
                {'l','f','o','g','i','f','i','t','f'},
                {'i','i','o','d','i','i','i','f','a'},
                {'f','r','b','p','p','l','i','u','a'},
                {'p','a','p','a','l','i','f','a','e'}};
        char [] palavra= {'f','i','l','i','p','a'};
        int linha1= 8;
        int coluna1=1;
        int linha2= 3;
        int coluna2= 4;

        boolean resultado= ExercicioDEZOITO.palavraEPossivelNasCoordenadasDadas(matrizLetras, palavra, linha1, coluna1, linha2, coluna2);

        assertFalse(resultado);
    }

    @Test
    void verificaPalavraDiagonalAscendenteDireitaPossivel() {
        char [][] matrizLetras={{'a','f','b','f','i','l','i','a','a'},
                {'p','r','o','e','r','a','m','a','p'},
                {'a','p','i','o','i','a','m','p','i'},
                {'p','f','b','l','i','a','i','p','f'},
                {'p','c','b','i','r','z','i','i','i'},
                {'l','f','o','g','i','f','i','t','f'},
                {'i','i','o','d','i','i','i','f','a'},
                {'f','r','b','p','p','l','i','u','a'},
                {'p','a','p','a','l','i','f','a','e'}};
        char [] palavra= {'f','i','l','i','p','a'};
        int linha1=8;
        int coluna1=2;
        int linha2=3;
        int coluna2=7;

        boolean resultado= ExercicioDEZOITO.palavraEPossivelNasCoordenadasDadas(matrizLetras, palavra, linha1, coluna1, linha2, coluna2);

        assertTrue(resultado);
    }

    @Test
    void verificaPalavraDiagonalAscendenteEsquerdaImpossivel() {
        char [][] matrizLetras={{'a','f','b','f','i','l','i','a','a'},
                {'p','r','o','e','r','a','m','a','p'},
                {'a','p','i','o','i','a','m','p','i'},
                {'p','f','b','l','i','a','i','p','f'},
                {'p','c','b','i','r','z','i','i','i'},
                {'l','f','o','g','i','f','i','t','f'},
                {'i','i','o','d','i','i','i','f','a'},
                {'f','r','b','p','p','l','i','u','a'},
                {'p','a','p','a','l','i','f','a','e'}};
        char [] palavra= {'f','i','l','i','p','a'};
        int linha1= 8;
        int coluna1=8;
        int linha2= 3;
        int coluna2= 6;

        boolean resultado= ExercicioDEZOITO.palavraEPossivelNasCoordenadasDadas(matrizLetras, palavra, linha1, coluna1, linha2, coluna2);

        assertFalse(resultado);
    }

    @Test
    void verificaPalavraDiagonalAscendenteEsquerdaPossivel() {
        char [][] matrizLetras={{'a','f','b','f','i','l','i','a','a'},
                {'p','r','o','e','r','a','m','a','p'},
                {'a','p','i','o','i','a','m','p','i'},
                {'p','f','b','l','i','a','i','p','f'},
                {'p','c','b','i','r','z','i','i','i'},
                {'l','f','o','g','i','f','i','t','f'},
                {'i','i','o','d','i','i','i','f','a'},
                {'f','r','b','p','p','l','i','u','a'},
                {'p','a','p','a','l','i','f','a','e'}};
        char [] palavra= {'f','i','l','i','p','a'};
        int linha1= 8;
        int coluna1=6;
        int linha2= 3;
        int coluna2= 1;

        boolean resultado= ExercicioDEZOITO.palavraEPossivelNasCoordenadasDadas(matrizLetras, palavra, linha1, coluna1, linha2, coluna2);

        assertTrue(resultado);
    }

    @Test
    void verificaPalavraCordenadaImpossivel() {
        char [][] matrizLetras={{'a','f','b','f','i','l','i','a','a'},
                {'p','r','o','e','r','a','m','a','p'},
                {'a','p','i','o','i','a','m','p','i'},
                {'p','f','b','l','i','a','i','p','f'},
                {'p','c','b','i','r','z','i','i','i'},
                {'l','f','o','g','i','f','i','t','f'},
                {'i','i','o','d','i','i','i','f','a'},
                {'f','r','b','p','p','l','i','u','a'},
                {'p','a','p','a','l','i','f','a','e'}};
        char [] palavra= {'f','i','l','i','p','a'};
        int linha1= 9;
        int coluna1=8;
        int linha2= 4;
        int coluna2= 6;

        boolean resultado= ExercicioDEZOITO.palavraEPossivelNasCoordenadasDadas(matrizLetras, palavra, linha1, coluna1, linha2, coluna2);

        assertFalse(resultado);
    }
}