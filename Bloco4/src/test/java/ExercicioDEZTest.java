import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDEZTest {

    @Test
    void Exercicio10_menorValor() {
        int[] vetor={5,7,1,3,4,5,9};
        int esperado=1;

        int resultado=ExercicioDEZ.menorValorVetor(vetor);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_vazio() {
        int[] vetor={};
        Integer esperado=null;

        Integer resultado=ExercicioDEZ.menorValorVetor(vetor);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_menorValorNumerosRepetidos() {
        int[] vetor={20,12,14,6,7,9,6,20};
        int esperado=6;

        int resultado=ExercicioDEZ.menorValorVetor(vetor);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_menorValorNumNegativos() {
        int[] vetor={5,7,1,3,-7,-2,9};
        int esperado=-7;

        int resultado=ExercicioDEZ.menorValorVetor(vetor);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_vazioMaiorValor() {
        int[] vetor={};
        Integer esperado=null;

        Integer resultado=ExercicioDEZ.maiorValorVetor(vetor);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_maiorValor() {
        int[] vetor={20,14,6,7,9,31};
        int esperado=31;

        int resultado=ExercicioDEZ.maiorValorVetor(vetor);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_maiorValorNumeroRepetido() {
        int[] vetor={20,31,31,6,7,9,31};
        int esperado=31;

        int resultado=ExercicioDEZ.maiorValorVetor(vetor);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exericico10_mediaVetor() {
        int[] vetor={2,2,5,6,9};
        double esperado=4.8;

        double resultado=ExercicioDEZ.valorMedioVetor(vetor);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exericico10_mediaVetorvalorNulo() {
        int[] vetor={};
        Double esperado=null;

        Double resultado=ExercicioDEZ.valorMedioVetor(vetor);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exericico10_mediaVetorvalorzero() {
        int[] vetor={0};
        double esperado=0.0;

        double resultado=ExercicioDEZ.valorMedioVetor(vetor);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exericico10_mediaVetorcomValoresNegativos() {
        int[] vetor={2,-4,5,7,-8};
        double esperado=0.4;

        double resultado=ExercicioDEZ.valorMedioVetor(vetor);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_produtoElementosVetor() {
        int[] vetor= {1,2,3,4,5};
        double esperado=120.0;

        double resultado=ExercicioDEZ.produtoElementosVetor(vetor);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_produtoElementosVetorcomZero() {
        int[] vetor= {1,2,3,4,5,0};
        double esperado=0.0;

        double resultado=ExercicioDEZ.produtoElementosVetor(vetor);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_produtoElementosVetorNulo() {
        int[] vetor= {};
        Double esperado=null;

        Double resultado=ExercicioDEZ.produtoElementosVetor(vetor);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_produtoElementosRepetidos() {
        int[] vetor= {2,2,2,4,5,6};
        double esperado=960.0;

        double resultado=ExercicioDEZ.produtoElementosVetor(vetor);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_numerosRepetidos() {
        int[] vetor= {1,2,3,3,4,4,5,6,7,7};
        int[] esperado= {1,2,3,4,5,6,7};

        int[] resultado= ExercicioDEZ.vetorValoresNaoRepetidos(vetor);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_numerosRepetidos1() {
        int[] vetor= {5,5,5,6,1,33,51,2,2};
        int[] esperado= {5,6,1,33,51,2};

        int[] resultado= ExercicioDEZ.vetorValoresNaoRepetidos(vetor);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_numerosRepetidos2() {
        int[] vetor= {44,57,82,35,1};
        int[] esperado= {44,57,82,35,1};

        int[] resultado= ExercicioDEZ.vetorValoresNaoRepetidos(vetor);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_numerosRepetidos3() {
        int[] vetor= null;
        int[] esperado= null;

        int[] resultado= ExercicioDEZ.vetorValoresNaoRepetidos(vetor);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_numerosRepetidos4() {
        int[] vetor= {-423};
        int[] esperado= {-423};

        int[] resultado= ExercicioDEZ.vetorValoresNaoRepetidos(vetor);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exericio10_inverterVetor() {
        int[] vetor={1,2,3,4,5};
        int[] esperado={5,4,3,2,1};

        int[] resultado= ExercicioDEZ.vetorInverso(vetor);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exericio10_inverterVetorcomNumerosRepetidos() {
        int[] vetor={2,2,44,6,0,2,2};
        int[] esperado={2,2,0,6,44,2,2};

        int[] resultado= ExercicioDEZ.vetorInverso(vetor);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exericio10_inverterVetornull() {
        int[] vetor={};
        int[] esperado=null;

        int[] resultado= ExercicioDEZ.vetorInverso(vetor);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_numeroPrimoTresPrimos() {
        int[] vetor= {2,4,5,6,7,8};
        int[] esperado= {2,5,7};

        int[] resultado= ExercicioDEZ.elementosPrimos(vetor);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_numeroPrimoNull() {
        int[] vetor= {};
        int[] esperado= null;

        int[] resultado= ExercicioDEZ.elementosPrimos(vetor);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_numeroPrimoComNegativos() {
        int[] vetor= {-5,0,4,6,8,2};
        int[] esperado= {-5,2};

        int[] resultado= ExercicioDEZ.elementosPrimos(vetor);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_numeroPrimoSemPrimos() {
        int[] vetor= {4,6,8,20};
        int[] esperado= {};

        int[] resultado= ExercicioDEZ.elementosPrimos(vetor);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio10_booleanEPrimo() {
        int num=-5;
        boolean esperado=true;

        boolean resultado=ExercicioDEZ.ePrimo(num);

        assertTrue(resultado);
    }

    @Test
    void Exercicio10_booleanEPrimoNumero2() {
        int num=2;
        boolean esperado=true;

        boolean resultado=ExercicioDEZ.ePrimo(num);

        assertTrue(resultado);
    }

    @Test
    void Exercicio10_booleanNaoPrimo() {
        int num=1;
        boolean esperado=false;

        boolean resultado=ExercicioDEZ.ePrimo(num);

        assertFalse(resultado);
    }

    @Test
    void Exercicio10_booleanEPrimoNumero4() {
        int num=4;
        boolean esperado=false;

        boolean resultado=ExercicioDEZ.ePrimo(num);

        assertFalse(resultado);
    }
}