import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioCATORZETest {

    @Test
    void Exericio14_matrizRetangular() {
        int[][]matriz={{12,3,4},{2,4,5}};
        boolean esperado=true;

        boolean resultado= ExercicioCATORZE.verificaMatrizRetangular(matriz);

        assertTrue(resultado);
    }

    @Test
    void Exercicio14_matrizQuadrada() {
        int[][]matriz={{1,2,3},{4,5,6},{7,8,9}};
        boolean esperado= false;

        boolean resultado= ExercicioCATORZE.verificaMatrizRetangular(matriz);

        assertFalse(resultado);
    }

    @Test
    void Exercicio14_matrizRetangular1() {
        int[][]matriz={{1,2,3},{4,5,6},{7,8,9},{4,7,8}};
        boolean esperado= true;

        boolean resultado= ExercicioCATORZE.verificaMatrizRetangular(matriz);

        assertTrue(resultado);
    }

    @Test
    void Exercicio14_matrizNull() {
        int[][]matriz=null;
        boolean esperado= false;

        boolean resultado= ExercicioCATORZE.verificaMatrizRetangular(matriz);

        assertFalse(resultado);
    }
}