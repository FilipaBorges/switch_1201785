import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioSETETest {

    @Test
    void ExercicioSete_doisMultiplosTres() {
        int limiteMin=4;
        int limiteMax=10;
        int[] esperado={6,9};

        int[] resultado= ExercicioSETE.multiplosTres(limiteMin,limiteMax);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void ExercicioSete_cincoMultiplosTres() {
        int limiteMin=12;
        int limiteMax=0;
        int[] esperado={0,3,6,9,12};

        int[] resultado= ExercicioSETE.multiplosTres(limiteMin,limiteMax);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void ExercicioSete_multiplosNumIntervaloNegativo() {
        int limiteMin=-10;
        int limiteMax=0;
        int[] esperado= {-9,-6,-3,0};

        int[] resultado= ExercicioSETE.multiplosTres(limiteMin,limiteMax);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void ExercicioSete_semMultiplos() {
        int limiteMin=10;
        int limiteMax=11;
        int [] esperado= {};

        int [] resultado= ExercicioSETE.multiplosTres(limiteMin,limiteMax);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void ExercicioSete_multiplosX() {
        int limiteMin=-2;
        int limiteMax=10;
        int x=2;
        int [] esperado={-2,0,2,4,6,8,10};

        int[] resultado= ExercicioSETE.multiplosX(limiteMin,limiteMax,x);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void ExercicioSete_multiplosX1() {
        int limiteMin=20;
        int limiteMax=4;
        int x=4;
        int [] esperado={4,8,12,16,20};

        int [] resultado= ExercicioSETE.multiplosX(limiteMin,limiteMax,x);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void ExercicioSete_multiplosXcomNegativo() {
        int limiteMin=2;
        int limiteMax=10;
        int x=-2;
        int[] esperado={2,4,6,8,10};

        int[] resultado = ExercicioSETE.multiplosX(limiteMin,limiteMax,x);

        assertArrayEquals(esperado,resultado);
    }

}