import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioCINCOTest {

    @Test
    void Exercicio5_somaPar_doisQuatroSeis() {
        int numero=1234567;
        int esperado=12;

        int resultado= ExercicioCINCO.somaPares(numero);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5_somaPar_oitoSeis() {
        int numero=135860;
        int esperado=14;

        int resultado= ExercicioCINCO.somaPares(numero);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5_somaImpar_umTresCinco() {
        int numero=123456;
        int esperado=9;

        int resultado= ExercicioCINCO.somaImpares(numero);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5_somaImpar_cincoSeteNove() {
        int numero=2457960;
        int esperado=21;

        int resultado= ExercicioCINCO.somaImpares(numero);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio5_somaImpar_nulo() {
        int numero=2468;
        int esperado=0;

        int resultado= ExercicioCINCO.somaImpares(numero);

        assertEquals(esperado, resultado);
    }
}