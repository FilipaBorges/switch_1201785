import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDEZASSETETest {

    @Test
    void Exercicio17_matrizNull() {
        int [][] matriz=null;
        int constante= 2;
        int [][] esperado= null;

        int [][] resultado=ExercicioDEZASSETE.produtoMatrizPorConstante(matriz, constante);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio17_matrizQuadradaConstantePositiva() {
        int [][] matriz={{1,2,3},{5,6,7},{1,2,8}};
        int constante= 2;
        int [][] esperado= {{2,4,6},{10,12,14},{2,4,16}};

        int [][] resultado=ExercicioDEZASSETE.produtoMatrizPorConstante(matriz, constante);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio17_matrizQuadradaConstanteZero() {
        int [][] matriz={{1,2,3},{5,6,7},{1,2,8}};
        int constante= 0;
        int [][] esperado= {{0,0,0},{0,0,0},{0,0,0}};

        int [][] resultado=ExercicioDEZASSETE.produtoMatrizPorConstante(matriz, constante);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio17_matrizQuadradaConstanteNegativa() {
        int [][] matriz={{1,2,3},{5,6,7},{1,2,8}};
        int constante= -10;
        int [][] esperado= {{-10,-20,-30},{-50,-60,-70},{-10,-20,-80}};

        int [][] resultado=ExercicioDEZASSETE.produtoMatrizPorConstante(matriz, constante);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio17_matrizRetangularComNegativosConstantePositiva() {
        int [][] matriz={{1,2,3},{5,6,7},{1,2,8},{-2,-5,-10}};
        int constante=1;
        int [][] esperado= {{1,2,3},{5,6,7},{1,2,8},{-2,-5,-10}};

        int [][] resultado=ExercicioDEZASSETE.produtoMatrizPorConstante(matriz, constante);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio17_matrizRetangularComNegativosConstanteZero() {
        int [][] matriz={{1,2,3},{5,6,7},{1,2,8},{-2,-5,-10}};
        int constante=0;
        int [][] esperado={{0,0,0},{0,0,0},{0,0,0},{0,0,0}};

        int [][] resultado=ExercicioDEZASSETE.produtoMatrizPorConstante(matriz, constante);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio17_matrizRetangularComZeroENegativosConstanteNegativa() {
        int [][] matriz={{1,2,0},{5,6,7},{1,2,8},{-2,-5,-10}};
        int constante=-2;
        int [][] esperado={{-2,-4,0},{-10,-12,-14},{-2,-4,-16},{4,10,20}};

        int [][] resultado=ExercicioDEZASSETE.produtoMatrizPorConstante(matriz, constante);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void somaMatrizesComMatrizNull() {
        int [][] matriz1={{1,2,3},{1,2,3},{1,2,3}};
        int [][] matriz2=null;
        int [][] esperado= null;

        int [][] resultado= ExercicioDEZASSETE.somaDuasMatrizes(matriz1,matriz2);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void somaMatrizesComMatrizesDeLinhasDiferentes() {
        int [][] matriz1={{1,2,3},{1,2,3},{1,2,3}};
        int [][] matriz2={{1,2,3},{1,2,3}};
        int [][] esperado= null;

        int [][] resultado= ExercicioDEZASSETE.somaDuasMatrizes(matriz1,matriz2);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void somaMatrizesComMatrizesDeColunasDiferentes() {
        int [][] matriz1={{1,2,3},{1,2,3},{1,2,3}};
        int [][] matriz2={{1,2},{1,2},{1,2}};
        int [][] esperado= null;

        int [][] resultado= ExercicioDEZASSETE.somaDuasMatrizes(matriz1,matriz2);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void somaMatrizesComMatrizesPositivasComZeros() {
        int [][] matriz1={{1,2,3},{1,2,3},{1,2,3}};
        int [][] matriz2={{1,2,0},{5,7,0},{8,9,10}};
        int [][] esperado= {{2,4,3},{6,9,3},{9,11,13}};

        int [][] resultado= ExercicioDEZASSETE.somaDuasMatrizes(matriz1,matriz2);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void somaMatrizesComMatrizesComNegativos() {
        int [][] matriz1={{-4,2,3},{0,2,-3},{1,2,3}};
        int [][] matriz2={{1,2,0},{5,7,0},{8,9,10}};
        int [][] esperado= {{-3,4,3},{5,9,-3},{9,11,13}};

        int [][] resultado= ExercicioDEZASSETE.somaDuasMatrizes(matriz1,matriz2);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void somaMatrizesComMatrizesComSomaZero() {
        int [][] matriz1={{-4,-2,-3},{0,-2,-3},{8,-9,10}};
        int [][] matriz2={{4,2,3},{0,2,3},{-8,9,-10}};
        int [][] esperado= {{0,0,0},{0,0,0},{0,0,0}};

        int [][] resultado= ExercicioDEZASSETE.somaDuasMatrizes(matriz1,matriz2);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void produtoMatrizesComMatrizNull() {
        int [][] matriz1={{1,2,3},{1,2,3},{1,2,3}};
        int [][] matriz2=null;
        int [][] esperado= null;

        int [][] resultado= ExercicioDEZASSETE.produtoDuasMatrizes(matriz1,matriz2);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void produtoMatrizesComMatrizesDeLinhasDiferentes() {
        int [][] matriz1={{1,2,3},{1,2,3},{1,2,3}};
        int [][] matriz2={{1,2,3},{1,2,3}};
        int [][] esperado= null;

        int [][] resultado= ExercicioDEZASSETE.produtoDuasMatrizes(matriz1,matriz2);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void produtoMatrizesComMatrizesDeColunasDiferentes() {
        int [][] matriz1={{1,2,3},{1,2,3},{1,2,3}};
        int [][] matriz2={{1,2},{1,2},{1,2}};
        int [][] esperado= null;

        int [][] resultado= ExercicioDEZASSETE.produtoDuasMatrizes(matriz1,matriz2);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void produtoMatrizesComMatrizesPositivasComZeros() {
        int [][] matriz1={{1,2,3},{1,2,3},{1,2,3}};
        int [][] matriz2={{1,2,0},{5,7,0},{8,9,10}};
        int [][] esperado= {{35,43,30},{35,43,30},{35,43,30}};

        int [][] resultado= ExercicioDEZASSETE.produtoDuasMatrizes(matriz1,matriz2);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void produtoMatrizesComMatrizesComNumerosNegativos() {
        int [][] matriz1={{-3,-5,6},{4,7,-8},{0,0,0}};
        int [][] matriz2={{1,2,0},{5,-2,0},{8,9,10}};
        int [][] esperado= {{20,58,60},{-25,-78,-80},{0,0,0}};

        int [][] resultado= ExercicioDEZASSETE.produtoDuasMatrizes(matriz1,matriz2);

        assertArrayEquals(esperado, resultado);
    }
}