import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioNOVETest {

    @Test
    void ExercicioNove_verifica101capicua() {
        int numero=101;
        String esperada="O número é uma capicua";

        String resultado=ExercicioNOVE.verificarCapicua(numero);

        assertEquals(esperada, resultado);
    }

    @Test
    void ExercicioNove_numeroNegativo() {
        int numero=-2;
        String esperada="Insira um número positivo";

        String resultado=ExercicioNOVE.verificarCapicua(numero);

        assertEquals(esperada, resultado);
    }
    @Test
    void ExercicioNove_naoECapicua() {
        int numero=3896;
        String esperada="O número não é uma capicua";

        String resultado=ExercicioNOVE.verificarCapicua(numero);

        assertEquals(esperada, resultado);
    }

}