import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioTRESTest {

    @Test
    void ExercicioTRES_soma25(){
        int []numero={3,6,7,8,1};
        int esperado=25;

        int resultado= ExercicioTRES.somaVetor(numero);

        assertEquals(esperado, resultado);
    }

    @Test
    void ExercicioTRES_soma40(){
        int []numero={15,12,3,10};
        int esperado=40;

        int resultado= ExercicioTRES.somaVetor(numero);

        assertEquals(esperado, resultado);
    }

}