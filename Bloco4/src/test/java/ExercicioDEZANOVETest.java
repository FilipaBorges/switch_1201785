import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDEZANOVETest {

    @Test
    void matrizMascaraSudokuInicial() {
        int[][] matriz = {{1, 4, 0, 0, 0, 0, 2, 0, 6},
                {0, 0, 0, 1, 0, 2, 0, 0, 0},
                {0, 0, 2, 0, 0, 0, 1, 0, 5},
                {5, 0, 0, 2, 1, 0, 0, 9, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {4, 0, 0, 8, 0, 9, 5, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 1, 0, 7, 0, 0, 4, 0, 0},
                {3, 0, 0, 9, 0, 0, 7, 0, 0}};
        int[][] esperado = {{1, 1, 0, 0, 0, 0, 1, 0, 1},
                {0, 0, 0, 1, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 0, 0, 1, 0, 1},
                {1, 0, 0, 1, 1, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 1, 0, 1, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 1, 0, 1, 0, 0, 1, 0, 0},
                {1, 0, 0, 1, 0, 0, 1, 0, 0}};

        int[][] resultado = ExercicioDEZANOVE.matrizMascaraDeMatriz(matriz);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificaCelulaVazia() {
        int[][] matriz = {{1, 1, 0, 0, 0, 0, 1, 0, 1},
                {0, 0, 0, 1, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 0, 0, 1, 0, 1},
                {1, 0, 0, 1, 1, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 1, 0, 1, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 1, 0, 1, 0, 0, 1, 0, 0},
                {1, 0, 0, 1, 0, 0, 1, 0, 0}};

        boolean resultado = ExercicioDEZANOVE.verificaCelulasPorPreencher(matriz);

        assertTrue(resultado);
    }

    @Test
    void verificaCelulaVaziaParaMatrizPreenchida() {
        int[][] matriz = {{1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1}};

        boolean resultado = ExercicioDEZANOVE.verificaCelulasPorPreencher(matriz);

        assertFalse(resultado);
    }

    @Test
    void matrizMascaraDeUmNumero() {
        int[][] matriz = {{1, 4, 0, 0, 0, 0, 2, 0, 6},
                {0, 0, 0, 1, 0, 2, 0, 0, 0},
                {0, 0, 2, 0, 0, 0, 1, 0, 5},
                {5, 0, 0, 2, 1, 0, 0, 9, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {4, 0, 0, 8, 0, 9, 5, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 1, 0, 7, 0, 0, 4, 0, 0},
                {3, 0, 0, 9, 0, 0, 7, 0, 0}};
        int numero = 2;
        int[][] esperado = {{0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 1, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};

        int[][] resultado = ExercicioDEZANOVE.matrizMascaraDeUmNumero(matriz, numero);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void matrizAposJogadaCoordenadaErrada() {
        int[][] matriz = {{1, 4, 0, 0, 0, 0, 2, 0, 6},
                {0, 0, 0, 1, 0, 2, 0, 0, 0},
                {0, 0, 2, 0, 0, 0, 1, 0, 5},
                {5, 0, 0, 2, 1, 0, 0, 9, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {4, 0, 0, 8, 0, 9, 5, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 1, 0, 7, 0, 0, 4, 0, 0},
                {3, 0, 0, 9, 0, 0, 7, 0, 0}};
        int numero = 5;
        int linha = 10;
        int coluna = 1;
        int[][] esperado = matriz;

        int[][] resultado = ExercicioDEZANOVE.matrizAposAdicaoDeNumero(matriz, numero, linha, coluna);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void matrizAposJogadaNumeroErrado() {
        int[][] matriz = {{1, 4, 0, 0, 0, 0, 2, 0, 6},
                {0, 0, 0, 1, 0, 2, 0, 0, 0},
                {0, 0, 2, 0, 0, 0, 1, 0, 5},
                {5, 0, 0, 2, 1, 0, 0, 9, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {4, 0, 0, 8, 0, 9, 5, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 1, 0, 7, 0, 0, 4, 0, 0},
                {3, 0, 0, 9, 0, 0, 7, 0, 0}};
        int numero = 10;
        int linha = 2;
        int coluna = 1;
        int[][] esperado = matriz;

        int[][] resultado = ExercicioDEZANOVE.matrizAposAdicaoDeNumero(matriz, numero, linha, coluna);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void matrizAposJogadaNumeroLimiteSuperior() {
        int[][] matriz = {{1, 4, 0, 0, 0, 0, 2, 0, 6},
                {0, 0, 0, 1, 0, 2, 0, 0, 0},
                {0, 0, 2, 0, 0, 0, 1, 0, 5},
                {5, 0, 0, 2, 1, 0, 0, 9, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {4, 0, 0, 8, 0, 9, 5, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 1, 0, 7, 0, 0, 4, 0, 0},
                {3, 0, 0, 9, 0, 0, 7, 0, 0}};
        int numero = 9;
        int linha = 2;
        int coluna = 2;
        int[][] esperado = matriz;

        int[][] resultado = ExercicioDEZANOVE.matrizAposAdicaoDeNumero(matriz, numero, linha, coluna);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void matrizAposJogadaNumeroLimiteInferior() {
        int[][] matriz = {{1, 4, 0, 0, 0, 0, 2, 0, 6},
                {0, 0, 0, 1, 0, 2, 0, 0, 0},
                {0, 0, 2, 0, 0, 0, 1, 0, 5},
                {5, 0, 0, 2, 1, 0, 0, 9, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {4, 0, 0, 8, 0, 9, 5, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 1, 0, 7, 0, 0, 4, 0, 0},
                {3, 0, 0, 9, 0, 0, 7, 0, 0}};
        int numero = 1;
        int linha = 8;
        int coluna = 8;
        int[][] esperado = {{1, 4, 0, 0, 0, 0, 2, 0, 6},
                {0, 0, 0, 1, 0, 2, 0, 0, 0},
                {0, 0, 2, 0, 0, 0, 1, 0, 5},
                {5, 0, 0, 2, 1, 0, 0, 9, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {4, 0, 0, 8, 0, 9, 5, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 1, 0, 7, 0, 0, 4, 0, 0},
                {3, 0, 0, 9, 0, 0, 7, 0, 1}};

        int[][] resultado = ExercicioDEZANOVE.matrizAposAdicaoDeNumero(matriz, numero, linha, coluna);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificaSeONumeroPodeSerAdicionadoPrimeiroQuadrante() {
        int[][] matriz = {{1, 4, 0, 0, 0, 0, 2, 0, 6},
                {0, 0, 0, 1, 0, 2, 0, 0, 0},
                {0, 0, 2, 0, 0, 0, 1, 0, 5},
                {5, 0, 0, 2, 1, 0, 0, 9, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {4, 0, 0, 8, 0, 9, 5, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 1, 0, 7, 0, 0, 4, 0, 0},
                {3, 0, 0, 9, 0, 0, 7, 0, 0}};
        int numero = 1;
        int linha = 1;
        int coluna = 0;

        boolean resultado = ExercicioDEZANOVE.verificaSeNumeroPodeSerAdicionado(matriz, numero, linha, coluna);

        assertFalse(resultado);
    }

    @Test
    void verificaSeONumeroPodeSerAdicionadoSegundoQuadrante() {
        int[][] matriz = {{1, 4, 0, 0, 0, 0, 2, 0, 6},
                {0, 0, 0, 1, 0, 2, 0, 0, 0},
                {0, 0, 2, 0, 0, 0, 1, 0, 5},
                {5, 0, 0, 2, 1, 0, 0, 9, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {4, 0, 0, 8, 0, 9, 5, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 1, 0, 7, 0, 0, 4, 0, 0},
                {3, 0, 0, 9, 0, 0, 7, 0, 0}};
        int numero = 1;
        int linha = 0;
        int coluna = 4;

        boolean resultado = ExercicioDEZANOVE.verificaSeNumeroPodeSerAdicionado(matriz, numero, linha, coluna);

        assertFalse(resultado);
    }

    @Test
    void verificaSeONumeroPodeSerAdicionadoSegundoQuadranteF() {
        int[][] matriz = {{1, 4, 0, 0, 0, 0, 2, 0, 6},
                {0, 0, 0, 1, 0, 2, 0, 0, 0},
                {0, 0, 2, 0, 0, 0, 1, 0, 5},
                {5, 0, 0, 2, 1, 0, 0, 9, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {4, 0, 0, 8, 0, 9, 5, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 1, 0, 7, 0, 0, 4, 0, 0},
                {3, 0, 0, 9, 0, 0, 7, 0, 0}};
        int numero = 8;
        int linha = 0;
        int coluna = 4;

        boolean resultado = ExercicioDEZANOVE.verificaSeNumeroPodeSerAdicionado(matriz, numero, linha, coluna);

        assertFalse(resultado);
    }

    @Test
    void verificaSeONumeroPodeSerAdicionadoUltimoQuadranteV() {
        int[][] matriz = {{1, 4, 0, 0, 0, 0, 2, 0, 6},
                {0, 0, 0, 1, 0, 2, 0, 0, 0},
                {0, 0, 2, 0, 0, 0, 1, 0, 5},
                {5, 0, 0, 2, 1, 0, 0, 9, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {4, 0, 0, 8, 0, 9, 5, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 1, 0, 7, 0, 0, 4, 0, 0},
                {3, 0, 0, 9, 0, 0, 7, 0, 0}};
        int numero = 2;
        int linha = 8;
        int coluna = 7;

        boolean resultado = ExercicioDEZANOVE.verificaSeNumeroPodeSerAdicionado(matriz, numero, linha, coluna);

        assertTrue(resultado);
    }

    @Test
    void verificaSeAMatrizEstaPreenchida() {
        int[][] matriz = {{5, 3, 4, 6, 7, 8, 9, 1, 2},
                {6, 7, 2, 1, 9, 5, 3, 4, 8},
                {1, 9, 8, 3, 4, 2, 5, 6, 7},
                {8, 5, 9, 7, 6, 1, 4, 2, 3},
                {4, 2, 6, 8, 5, 3, 7, 9, 1},
                {7, 1, 3, 9, 2, 4, 8, 5, 6},
                {9, 6, 1, 5, 3, 7, 2, 8, 4},
                {2, 8, 7, 4, 1, 9, 6, 3, 5},
                {3, 4, 5, 2, 8, 6, 1, 7, 9}};

        boolean resultado= ExercicioDEZANOVE.verificaSeTodaAMatrizEstaPreenchida(matriz);

        assertTrue(resultado);
    }

    @Test
    void verificaSeAMatrizEstaPreenchidaNaoPreenchida() {
        int[][] matriz = {{5, 3, 4, 6, 7, 8, 9, 1, 2},
                {6, 7, 2, 1, 9, 5, 3, 4, 8},
                {1, 9, 8, 3, 4, 2, 5, 6, 7},
                {8, 5, 9, 7, 6, 1, 4, 2, 3},
                {4, 2, 6, 8, 5, 0, 7, 9, 1},
                {7, 1, 3, 9, 2, 4, 8, 5, 6},
                {9, 6, 1, 5, 3, 7, 2, 8, 4},
                {2, 8, 7, 4, 1, 9, 6, 3, 5},
                {3, 4, 5, 2, 8, 6, 1, 7, 9}};

        boolean resultado= ExercicioDEZANOVE.verificaSeTodaAMatrizEstaPreenchida(matriz);

        assertFalse(resultado);
    }

}