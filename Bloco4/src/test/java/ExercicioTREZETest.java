import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioTREZETest {

    @Test
    void Exercicio13_matrizQaudrada() {
        int[][] matriz={{1,2,3,4},{5,4,3,7},{5,6,8,9},{1,4,5,9}};
        boolean esperado= true;

        boolean resultado= ExercicioTREZE.verificaMatrizQuadrada(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio13_matrizNaoQaudrada() {
        int[][] matriz={{1,2,3,4},{5,6,8,9},{1,4,5,9}};
        boolean esperado= false;

        boolean resultado= ExercicioTREZE.verificaMatrizQuadrada(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio13_matrizSemColunas() {
        int[][] matriz={{1,2,3,4}};
        boolean esperado= false;

        boolean resultado= ExercicioTREZE.verificaMatrizQuadrada(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio13_matrizNull() {
        int[][] matriz=null;
        boolean esperado= false;

        boolean resultado= ExercicioTREZE.verificaMatrizQuadrada(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void matrizNaoQuadradaComDiferentesTamanhosColunas() {
        int [][] matriz= {{1,2,3}, {2,4}, {1,4,5,6}};

        boolean resultado= ExercicioTREZE.verificaMatrizQuadrada(matriz);

        assertFalse(resultado);
    }
}