import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioUMTest {

    @Test
    void ExercicioUm_quatroDigitos() {
        int numero= 2789;
        int esperado=4;

        int resultado= ExercicioUM.numeroDigitos(numero);

        assertEquals(esperado, resultado);
    }

    @Test
    void ExercicioUm_umDigitos() {
        int numero= 1;
        int esperado=1;

        int resultado= ExercicioUM.numeroDigitos(numero);

        assertEquals(esperado, resultado);
    }

    @Test
    void ExercicioUm_seteDigitos() {
        int numero= 2578912;
        int esperado=7;

        int resultado= ExercicioUM.numeroDigitos(numero);

        assertEquals(esperado, resultado);
    }

    @Test
    void ExercicioUM_numeroNegativo() {
        int numero= -2;
        int esperado=0;

        int resultado= ExercicioUM.numeroDigitos(numero);

        assertEquals(esperado, resultado);
    }
}