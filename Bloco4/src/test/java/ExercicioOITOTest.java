import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioOITOTest {

    @Test
    void ExercicioOito_MultiplosDeDoisETres() {
        int limiteMin=1;
        int limiteMax=10;
        int[] numeros={2,3};
        int[] esperado={6};

        int[] resultado= ExercicioOITO.multiplosComuns(limiteMin,limiteMax, numeros);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void ExercicioOito_MultiplosDeDoisETresLimitesTrocados() {
        int limiteMin=21;
        int limiteMax=10;
        int[] numeros={2,3};
        int[] esperado={12,18};

        int[] resultado= ExercicioOITO.multiplosComuns(limiteMin,limiteMax, numeros);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void ExercicioOito_null() {
        int limiteMin=21;
        int limiteMax=10;
        int[] numeros=null;
        int[] esperado=null;

        int[] resultado= ExercicioOITO.multiplosComuns(limiteMin,limiteMax, numeros);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void ExercicioOito_null1() {
        int limiteMin=21;
        int limiteMax=10;
        int[] numeros={2};
        int[] esperado=null;

        int[] resultado= ExercicioOITO.multiplosComuns(limiteMin,limiteMax, numeros);

        assertArrayEquals(esperado, resultado);
    }
}