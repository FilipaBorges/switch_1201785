import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDOZETest {

    @Test
    void Exercicio12_numeroDiferenteColunas() {
        int[][] matriz= {{1,2,3},{7,8}};
        int esperado= -1;

        int resultado= ExercicioDOZE.matrizComNumeroIgualDeColunasEmTodasAsLinhas(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio12_numeroIgualColunas() {
        int[][] matriz= {{1,2,3},{7,8,9}};
        int esperado= 3;

        int resultado= ExercicioDOZE.matrizComNumeroIgualDeColunasEmTodasAsLinhas(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio12_numeroIgualColunas1() {
        int[][] matriz= {{1,2,3,4,5},{7,8,9,10,11},{3,4,5,6,7},{10,20,30,40,50},{13,14,15,16,17},{1,3,4,5,6}};
        int esperado= 5;

        int resultado= ExercicioDOZE.matrizComNumeroIgualDeColunasEmTodasAsLinhas(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio12_matrizNull() {
        int[][] matriz= null;
        int esperado=-1;

        int resultado= ExercicioDOZE.matrizComNumeroIgualDeColunasEmTodasAsLinhas(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio12_matrizVazia() {
        int[][] matriz= {};
        int esperado=-1;

        int resultado= ExercicioDOZE.matrizComNumeroIgualDeColunasEmTodasAsLinhas(matriz);

        assertEquals(esperado, resultado);
    }
}