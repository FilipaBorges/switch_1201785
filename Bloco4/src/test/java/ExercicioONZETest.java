import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioONZETest {

    @Test
    void Exercicio11_arraysComComprimentoDiferentes() {
        int[] array1={4,7,2,33,4};
        int[] array2={5,8,9,22,48,52,3,4};
        Integer esperado=null;

        Integer resultado= ExercicioONZE.produtoEscalarDoisVetores(array1,array2);

        assertEquals(esperado, resultado);
        assertNull(resultado);
    }

    @Test
    void Exercicio11_arrayNull() {
        int[] array1={4,7,2,33,4};
        int[] array2=null;
        Integer esperado=null;

        Integer resultado= ExercicioONZE.produtoEscalarDoisVetores(array1,array2);

        assertEquals(esperado, resultado);
        assertNull(resultado);
    }

    @Test
    void Exercicio11_arrayNull1() {
        int[] array1=null;
        int[] array2={8,20,3};
        Integer esperado=null;

        Integer resultado= ExercicioONZE.produtoEscalarDoisVetores(array1,array2);

        assertEquals(esperado, resultado);
        assertNull(resultado);
    }

    @Test
    void Exercicio11_arrayVazio() {
        int[] array1={4,7,2,33,4};
        int[] array2={};
        Integer esperado=null;

        Integer resultado= ExercicioONZE.produtoEscalarDoisVetores(array1,array2);

        assertEquals(esperado, resultado);
        assertNull(resultado);
    }

    @Test
    void Exercicio11_arraysCom3Digitos() {
        int[] array1={2,7,9};
        int[] array2={14,1,5};
        int esperado= 80;

        int resultado= ExercicioONZE.produtoEscalarDoisVetores(array1, array2);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio11_arraysComNegativos() {
        int[] array1={-2,7,9};
        int[] array2={14,-3,5};
        int esperado= -4;

        int resultado= ExercicioONZE.produtoEscalarDoisVetores(array1, array2);

        assertEquals(esperado, resultado);
    }
}