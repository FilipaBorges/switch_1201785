import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDOISTest {

    @Test
    void Exercicio2_retornarArray() {
        int numero= 12345;
        int esperado[]= {1,2,3,4,5};

        int resultado []= ExercicioDOIS.converterArray(numero);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio2_retornarArray1() {
        int numero=106783940;
        int esperado []= {1,0,6,7,8,3,9,4,0};

        int resultado []= ExercicioDOIS.converterArray(numero);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio2_retornarArray3() {
        int numero=-2;
        int esperado []= {};

        int resultado []= ExercicioDOIS.converterArray(numero);

        assertArrayEquals(esperado, resultado);
    }

}