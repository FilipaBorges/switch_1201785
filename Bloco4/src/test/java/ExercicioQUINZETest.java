import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioQUINZETest {

    @Test
    void Exercicio15_menorValor() {
        int[][] matriz = {{4, 5, 6, 2, 7}, {3, 5, 9, 8}, {1, 2, 9}, {2, 5, 7, 8, 9}};
        int esperado = 1;

        int resultado = ExercicioQUINZE.elementoMenorValorMatriz(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_menorValorComNegativos() {
        int[][] matriz = {{4, 5, 6, -10, 7}, {-22, 5, 9, 8}, {1, 2, -34}, {2, 5, 7, 8, 9}};
        int esperado = -34;

        int resultado = ExercicioQUINZE.elementoMenorValorMatriz(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_menorValorNull() {
        int[][] matriz = null;
        Integer esperado = null;

        Integer resultado = ExercicioQUINZE.elementoMenorValorMatriz(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_menorValorVazio() {
        int[][] matriz = {};
        Integer esperado = null;

        Integer resultado = ExercicioQUINZE.elementoMenorValorMatriz(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_maiorValor() {
        int[][] matriz = {{2, 6, 9, 3}, {9, 5, 7}, {3, 8, 7, 6, 5, 2}, {6, 5, 3, 1}};
        int esperado = 9;

        int resultado = ExercicioQUINZE.elementoMaiorValorMatriz(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exericicio15_maiorValorComNegativos() {
        int[][] matriz = {{-4, 5, 8, 9, 27}, {-20, 4, 5, -33, 33}, {5, 7, 9, 10}};
        int esperado = 33;

        int resultado = ExercicioQUINZE.elementoMaiorValorMatriz(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_null() {
        int[][] matriz = null;
        Integer esperado = null;

        Integer resultado = ExercicioQUINZE.elementoMaiorValorMatriz(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_matrizVazia() {
        int[][] matriz = {};
        Integer esperado = null;

        Integer resultado = ExercicioQUINZE.elementoMaiorValorMatriz(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_mediaElementosMatrizVazia() {
        int[][] matriz = {};
        Double esperado = null;

        Double resultado = ExercicioQUINZE.mediaElementosMatriz(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_mediaElementosMatrizNull() {
        int[][] matriz = null;
        Double esperado = null;

        Double resultado = ExercicioQUINZE.mediaElementosMatriz(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_mediaElementos() {
        int[][] matriz = {{4, 5, 8, 9}, {2, 3, 4}, {5, 9, 1, 2, 3, 4}};
        double esperado = 4.54;

        double resultado = ExercicioQUINZE.mediaElementosMatriz(matriz);

        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void Exercicio15_mediaElementosComNegativos() {
        int[][] matriz = {{4, 5, 8, 9}, {2, -3, 4}, {-2, 5}};
        double esperado = 3.5556;

        double resultado = ExercicioQUINZE.mediaElementosMatriz(matriz);

        assertEquals(esperado, resultado, 0.0001);
    }

    @Test
    void Exercicio15_mediaElementos1() {
        int[][] matriz = {{-2, 5 - 6}, {2, -3, 4, 7}, {-2, 5}};
        double esperado = 1.25;

        double resultado = ExercicioQUINZE.mediaElementosMatriz(matriz);

        assertEquals(esperado, resultado, 0.0001);
    }

    @Test
    void Exercicio15_produtoElementosComNegativo() {
        int[][] matriz = {{2, 4, 7}, {-2, 1, 20}};
        double esperado = -2240;

        double resultado = ExercicioQUINZE.produtosElementosMatriz(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_produtoElementosComZero() {
        int[][] matriz = {{45, 67, 81, 20}, {3, 56, 7, 9}, {1, 2, 0}};
        double esperado = 0;

        double resultado = ExercicioQUINZE.produtosElementosMatriz(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exericio15_produtoElementosMatrizVazia() {
        int[][] matriz = {};
        Double esperado = null;

        Double resultado = ExercicioQUINZE.produtosElementosMatriz(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_elementoNaoRepetido() {
        int[][] matriz = {{1, 2, 3}, {2, 4, 5}, {5, 6, 7}};
        int[] esperado = {1, 2, 3, 4, 5, 6, 7};

        int[] resultado = ExercicioQUINZE.valoresUnicosMatriz(matriz);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_elementoNaoRepetidoComZeroseNegativos() {
        int[][] matriz = {{1, 2, 3}, {2, 0, 5}, {5, 6, -7}};
        int[] esperado = {1, 2, 3, 0, 5, 6, -7};

        int[] resultado = ExercicioQUINZE.valoresUnicosMatriz(matriz);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_elementosNaoRepetidoNull() {
        int[][] matriz = {};
        int[] esperado = null;

        int[] resultado = ExercicioQUINZE.valoresUnicosMatriz(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_elementosNaoRepetidoMatrizNaoRetangular() {
        int[][] matriz = {{1, 2, 3, 4}, {7, 8}, {9, 19, 40, 33, 1}};
        int[] esperado = null;

        int[] resultado = ExercicioQUINZE.valoresUnicosMatriz(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_elementosPrimos() {
        int[][] matriz = {{1, 2, 3, 4}, {5, 6, 7, 8}};
        int[] esperado = {2, 3, 5, 7};

        int[] resultado = ExercicioQUINZE.elementosPrimosMatriz(matriz);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_elementosPrimosComNegativos() {
        int[][] matriz = {{1, 2, 3, 4}, {5, 6, 7, 8}, {-5, 6, -7, 8}};
        int[] esperado = {2, 3, 5, 7, -5, -7};

        int[] resultado = ExercicioQUINZE.elementosPrimosMatriz(matriz);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_elementosPrimosVazio() {
        int[][] matriz = {{1, 0, 8, 10}, {6, 12, 16, 40}};
        int[] esperado = {};

        int[] resultado = ExercicioQUINZE.elementosPrimosMatriz(matriz);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_elementosPrimosNull() {
        int[][] matriz = null;
        int[] esperado = null;

        int[] resultado = ExercicioQUINZE.elementosPrimosMatriz(matriz);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_diagonalPrincipalMatrizQuadrada() {
        int[][] matriz = {{1, 2, 3}, {1, 2, 3}, {1, 2, 3}};
        int[] esperado = {1, 2, 3};

        int[] resultado = ExercicioQUINZE.diagonalPrincipal(matriz);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_diagonalPrincipalMatrizRetangular() {
        int[][] matriz = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}};
        int[] esperado = {1, 6, 11};

        int[] resultado = ExercicioQUINZE.diagonalPrincipal(matriz);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_diagonalPrincipalMatrizNaoQuadradaNemRetangular() {
        int[][] matriz = {{1, 2, 3}, {1, 4, 5, 6}, {7, 8}};
        int[] esperado = null;

        int[] resultado = ExercicioQUINZE.diagonalPrincipal(matriz);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_diagonalPrincipalComNull() {
        int[][] matriz = null;
        int[] esperado = null;

        int[] resultado = ExercicioQUINZE.diagonalPrincipal(matriz);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_diagonalSecundariaComNull() {
        int[][] matriz = null;
        int[] esperado = null;

        int[] resultado = ExercicioQUINZE.diagonalSecundaria(matriz);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_diagonalSecundariaMatrizNaoQuadradaNemRetangular() {
        int[][] matriz = {{1, 2, 3}, {1, 4, 5, 6}, {7, 8}};
        int[] esperado = null;

        int[] resultado = ExercicioQUINZE.diagonalSecundaria(matriz);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_diagonalSecundaria() {
        int[][] matriz = {{1, 2, 3, 4}, {1, 4, 5, 6}, {7, 8, 9, 10}};
        int[] esperado = {4, 5, 8};

        int[] resultado = ExercicioQUINZE.diagonalSecundaria(matriz);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_diagonalSecundariaComZeros() {
        int[][] matriz = {{1, 2, 0, 4}, {1, 4, 0, 6}, {7, 0, 9, 10}};
        int[] esperado = {4, 0, 0};

        int[] resultado = ExercicioQUINZE.diagonalSecundaria(matriz);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_diagonalSecundariaComNegativos() {
        int[][] matriz = {{1, 2, 0, 4}, {1, 4, -4, 6}, {7, 20, 9, 10}};
        int[] esperado = {4, -4, 20};

        int[] resultado = ExercicioQUINZE.diagonalSecundaria(matriz);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio15_matrizIdentidade() {
        int[][] matriz = {{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};

        boolean resultado = ExercicioQUINZE.matrizIdentidade(matriz);

        assertTrue(resultado);
    }

    @Test
    void Exercicio15_matrizIdentidadeNaoQuadrada() {
        int[][] matriz = {{0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};

        boolean resultado = ExercicioQUINZE.matrizIdentidade(matriz);

        assertFalse(resultado);
    }

    @Test
    void Exercicio15_matrizNaoIdentidade() {
        int[][] matriz = {{1, 0, 0, 0}, {0, 1, 0, 0}, {1, 1, 0, 0}, {0, 0, 0, 1}};

        boolean resultado = ExercicioQUINZE.matrizIdentidade(matriz);

        assertFalse(resultado);
    }

    @Test
    void Exercicio15_matrizNaoIdentidadeComNumerosDiferentes() {
        int[][] matriz = {{1, 0, 0, 0}, {0, 1, 5, 0}, {0, 0, 1, 0}, {7, 0, 0, 1}};

        boolean resultado = ExercicioQUINZE.matrizIdentidade(matriz);

        assertFalse(resultado);
    }

    @Test
    void matrizTranspostaQuadrada() {
        double[][] matriz = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}};
        double [][] esperado = {{1, 5, 9, 13}, {2, 6, 10, 14}, {3, 7, 11, 15}, {4, 8, 12, 16}};

        double [][] resultado = ExercicioQUINZE.matrizTransposta(matriz);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void matrizTranspostaRetangular() {
        double[][] matriz = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}};
        double[][] esperado = {{1, 5, 9}, {2, 6, 10}, {3, 7, 11}, {4, 8, 12}};

        double[][] resultado = ExercicioQUINZE.matrizTransposta(matriz);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void matrizTranspostaVazia() {
        double[][] matriz = {};
        double[][] esperado = null;

        double[][] resultado = ExercicioQUINZE.matrizTransposta(matriz);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void matrizTranspostaComZerosENumerosRepetidos() {
        double[][] matriz = {{2, 4, 0}, {0, 1, 2}, {0, 0, 0}};
        double[][] esperado = {{2, 0, 0}, {4, 1, 0}, {0, 2, 0}};

        double[][] resultado = ExercicioQUINZE.matrizTransposta(matriz);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void matrizTranspostaTamanhoGrande() {
        double[][] matriz = {{2, 4, 0}, {0, 1, 2}, {0, 0, 0}, {5, -10, 2}, {4, 7, 8}, {0, 1, 3}, {-10, 20, 1}};
        double[][] esperado = {{2, 0, 0, 5, 4, 0, -10}, {4, 1, 0, -10, 7, 1, 20}, {0, 2, 0, 2, 8, 3, 1}};

        double[][] resultado = ExercicioQUINZE.matrizTransposta(matriz);

        assertArrayEquals(esperado, resultado);
    }
}