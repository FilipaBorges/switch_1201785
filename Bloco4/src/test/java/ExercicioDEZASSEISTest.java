import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDEZASSEISTest {

    @Test
    void matrizNaoQuadrada() {
        int [][] matriz= {{1,2,3},{7,8,9,10},{1,4}};
        Integer esperado=null;

        Integer resultado= ExercicioDEZASSEIS.determinanteMatrizTeoremaLaPlace(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void matrizOrdemUm() {
        int [][] matriz={{4}};
        int esperado=4;

        int resultado= ExercicioDEZASSEIS.determinanteMatrizTeoremaLaPlace(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void matrizOrdemDois() {
        int [][] matriz={{1,4},{-5,10}};
        int esperado= 30;

        int resultado= ExercicioDEZASSEIS.determinanteMatrizTeoremaLaPlace(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void matrizOrdemDoisComZeros() {
        int [][] matriz={{0,4},{5,10}};
        int esperado= -20;

        int resultado= ExercicioDEZASSEIS.determinanteMatrizTeoremaLaPlace(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void matrizIgualComZeros() {
        int [][] matriz= {{1,2,3,4},{5,6,7,8},{9,10,11,12},{1,2,3,4}};
        int [][] esperado= {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};

        int [][] resultado= ExercicioDEZASSEIS.copiaEsqueletoMatrizAZeros(matriz);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void copiarMatriz() {
        int [][] matriz= {{1,2,3,4},{5,6,7,8},{9,10,11,12},{1,2,3,4}};
        int [][] esperado= {{1,2,3,4},{5,6,7,8},{9,10,11,12},{1,2,3,4}};

        int [][] resultado= ExercicioDEZASSEIS.copiaMatriz(matriz);

        assertArrayEquals(esperado, resultado);
        assertNotSame(esperado, resultado);
    }

    @Test
    void matrizOrdemTres() {
        int [][] matriz= {{1,2,3},{2,5,6},{2,5,8}};
        int esperado=2;

        int resultado= ExercicioDEZASSEIS.determinanteMatrizTeoremaLaPlace(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void matrizOrdemTresComZeros() {
        int [][] matriz= {{1,3,0},{2,5,1},{2,1,3}};
        int esperado=2;

        int resultado= ExercicioDEZASSEIS.determinanteMatrizTeoremaLaPlace(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    void matrizOrdemQuatro() {
        int [][] matriz= {{1,0,2,0},{2,1,1,1},{2,3,0,1},{-1,1,2,2}};
        int esperado= 19;

        int resultado= ExercicioDEZASSEIS.determinanteMatrizTeoremaLaPlace(matriz);

        assertEquals(esperado, resultado);
    }

    @Test
    public void testeOrdemCinco() {
        int[][] matriz = {{1, 2, 4, 5, 1}, {6, 7, 8, 0, 0}, {4, 5, 6, 8, 3}, {5, 6, 7, 4, 8}, {6, 4, 2, 2, 2}};
        int esperado = -962;

        int resultado = ExercicioDEZASSEIS.determinanteMatrizTeoremaLaPlace(matriz);

        assertEquals(esperado, resultado);
    }
}