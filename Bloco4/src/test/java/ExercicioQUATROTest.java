import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioQUATROTest {

    @Test
    void Exercicio4_vetorPares() {
        int [] numero= {1,2,4,5,6,7,8,9};
        int [] esperado= {2,4,6,8};

        int [] resultado= ExercicioQUATRO.vetorPares(numero);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio4_vetorPares1() {
        int [] numero= {1,3,5,7,9};
        int [] esperado= {};

        int [] resultado= ExercicioQUATRO.vetorPares(numero);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio4_vetorParesComNegativos() {
        int [] numero= {1,-2,5,7,9};
        int [] esperado= {-2};

        int [] resultado= ExercicioQUATRO.vetorPares(numero);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio4_vetorPares2() {
        int [] numero= {2,2,6,8,6};
        int [] esperado= {2,2,6,8,6};

        int [] resultado= ExercicioQUATRO.vetorPares(numero);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio4_vetorImpares() {
        int [] numero= {1,2,4,5,6,7,8,9};
        int [] esperado= {1,5,7,9};

        int [] resultado= ExercicioQUATRO.vetorImpares(numero);

        assertArrayEquals(esperado, resultado);
    }


    @Test
    void Exercicio4_vetorImpares1() {
        int [] numero= {2,4,6,8};
        int [] esperado= {};

        int [] resultado= ExercicioQUATRO.vetorImpares(numero);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio4_vetorImpares2() {
        int [] numero= {1,5,9};
        int [] esperado= {1,5,9};

        int [] resultado= ExercicioQUATRO.vetorImpares(numero);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio4_vetorImparesComNegativos() {
        int [] numero= {1,5,9,-3};
        int [] esperado= {1,5,9,-3};

        int [] resultado= ExercicioQUATRO.vetorImpares(numero);

        assertArrayEquals(esperado, resultado);
    }
}