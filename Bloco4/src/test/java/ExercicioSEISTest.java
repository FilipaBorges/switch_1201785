import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioSEISTest {

    @Test
    void Exercicio6_primeirosQuatroElementos() {
        int[] vetorInicial = {1, 2, 3, 4, 5, 6, 7};
        int n = 4;
        int[] esperado = {1, 2, 3, 4};

        int[] resultado = ExercicioSEIS.vetorCurto(vetorInicial, n);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio6_nenhumElemento() {
        int[] vetorInicial = {1, 2, 3, 4, 5, 6, 7};
        int n = 0;
        int[] esperado = {};

        int[] resultado = ExercicioSEIS.vetorCurto(vetorInicial, n);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio6_doisElementos() {
        int[] vetorInicial = {14, 201, 3, 4, 5, 6, 7};
        int n = 2;
        int[] esperado = {14, 201};

        int[] resultado = ExercicioSEIS.vetorCurto(vetorInicial, n);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void Exercicio6_() {
        int[] vetorInicial = {14, 201, 3, 4, 5, 6, 7};
        int n = 9;
        int[] esperado = {0,0,0,0,0,0,0,0,0};

        int[] resultado = ExercicioSEIS.vetorCurto(vetorInicial, n);

        assertArrayEquals(esperado, resultado);
    }
}