public class ExercicioDEZOITO {

    //atenção ao ver direções! existem diagonais ascendentes/descentes e tanto para a esquerda como para a direita!
    //para verificar a existência de palavras tendo a posição de inicio podemos primeiro ver se tem comprimento para isso
    //se não tiver comprimento é impossível que a palavra lá esteja

    //conhecendo o jogo mencionado no exercicio só faz sentido que a matriz seja quadrada

    /**
     * Verifica o número de colunas para uma matriz do tipo char
     *
     * @param matriz matriz de onde se quer conhecer o número de colunas
     * @return número de colunas da matriz
     */
    public static int numeroColunas(char[][] matriz) {
        int numeroColunas = 0;
        boolean numeroIgualColunas = true;

        //se a matriz for null ou não tiver linhas(não é matriz) não pode ter colunas portanto retorna -1
        if (matriz == null || matriz.length < 1) {
            numeroColunas = -1;
            return numeroColunas;
        }
        for (int linha = 0; linha < matriz.length; linha++) {

            for (int coluna = 0; coluna < matriz[linha].length && numeroIgualColunas; coluna++) {

                if (matriz[linha].length == matriz[0].length) {
                    numeroColunas = matriz[linha].length;
                    numeroIgualColunas = true;
                } else {
                    numeroIgualColunas = false;
                    numeroColunas = -1;
                }

            }
        }
        return numeroColunas;
    }

    /*

    /**
     * verifica se uma matriz tipo char é quadrada
     * @param matriz que se quer verificar se é quadrada
     * @return retorna um booleano
     */
    public static boolean verificaMatrizQuadrada(char[][] matriz) {
        //se não houver colunas não é uma matriz mas sim um vetor. Um vetor nunca pode ser quadrado
        boolean eQuadrada;

        if (matriz == null || matriz[0].length == 0) {
            eQuadrada=false;
        }
        //primeiro temos que ver o número de colunas porque o comprimento da primeira até pode ser o mesmo que o comprimento da linha e a matriz passa com erro
        int numeroColunas = ExercicioDEZOITO.numeroColunas(matriz);

        eQuadrada= matriz.length == numeroColunas;

        return eQuadrada;
    }

    /**
     * cria uma matriz máscara dada uma matriz de letras e a letra que se pretente encontrar
     *
     * @param matrizLetras completa
     * @param letra        que queremos encontrar
     * @return matriz de 0 e 1 com o 1 no local da letra a procuraar
     */
    public static int[][] matrizMascaraParaLetra(char[][] matrizLetras, char letra) {
        if (!ExercicioDEZOITO.verificaMatrizQuadrada(matrizLetras)) {
            return null;
        }

        int[][] matrizMascara = new int[matrizLetras.length][matrizLetras[0].length];

        for (int i = 0; i < matrizLetras.length; i++) {
            for (int j = 0; j < matrizLetras[0].length; j++) {
                if (matrizLetras[i][j] != letra) {
                    matrizMascara[i][j] = 0;
                } else {
                    matrizMascara[i][j] = 1;
                }
            }
        }
        return matrizMascara;
    }


    /**
     * método que verifica se uma dada palavra está na sopa de letras
     *
     * @param matrizLetras sopa de letras
     * @param palavra      que queremos verificar se existe
     * @return só retorna se a palavra existe ou não
     */
    public static boolean verificaPalavra(char[][] matrizLetras, String palavra) {
        boolean existePalavra = false;
        //a matriz tem que ser quadrada
        if (!verificaMatrizQuadrada(matrizLetras)) {
            return existePalavra;
        }

        //se o tamanho da sopa for inferior ao tamanho da palavra é impossível que ela lá esteja
        if (matrizLetras.length < palavra.length()) {
            return existePalavra;
        }

        //primeiro vamos converter a palavra para um array
        char[] arrayPalavra = ExercicioDEZOITO.convertePalavraDeStringParaArray(palavra);

        //agora verificamos se a palavra existe em qualquer uma das direções
        if (palavraExisteDaEsquerdaParaDireita(matrizLetras, arrayPalavra)) {
            existePalavra = true;
        } else if (palavraExisteDaDireitaParaEsquerda(matrizLetras, arrayPalavra)) {
            existePalavra = true;
        } else if (palavraExisteDeCimaParaBaixo(matrizLetras, arrayPalavra)) {
            existePalavra = true;
        } else if (palavraExisteBaixoParaCima(matrizLetras, arrayPalavra)) {
            existePalavra = true;
        } else if (palavraExisteDiagonalDescendenteDireita(matrizLetras, arrayPalavra)) {
            existePalavra = true;
        } else if (palavraExisteDiagonalDescendenteEsquerda(matrizLetras, arrayPalavra)) {
            existePalavra = true;
        } else if (palavraExisteDiagonalAscendenteDireita(matrizLetras, arrayPalavra)) {
            existePalavra = true;
        } else if (palavraExisteDiagonalAscendenteEsquerda(matrizLetras, arrayPalavra)) {
            existePalavra = true;
        }
        return existePalavra;
    }

    /**
     * este método converte uma palavra em String para array de caracteres
     *
     * @param palavra palavra a converter
     * @return retorna um array de char
     */
    public static char[] convertePalavraDeStringParaArray(String palavra) {
        //primeiro vamos inicializar o array
        char[] arrayPalavra = new char[palavra.length()];

        //agora vamos colocar cada letra numa posição do array
        for (int indice = 0; indice < arrayPalavra.length; indice++) {
            arrayPalavra[indice] = palavra.charAt(indice);
        }
        return arrayPalavra;
    }

    /**
     * este método determina se o número 1 está presente na matriz máscara
     *
     * @param matrizMascara matriz que identifica o local de uma letra
     * @return se tem ou não o número 1
     */
    public static boolean existeNumeroUm(int[][] matrizMascara) {
        boolean existeNumeroUm = false;

        //vamos percorrer todas as linhas e colunas para verificar o 1
        for (int indiceLinha = 0; indiceLinha < matrizMascara.length; indiceLinha++) {
            for (int indiceColuna = 0; indiceColuna < matrizMascara[0].length; indiceColuna++) {
                if (matrizMascara[indiceLinha][indiceColuna] != 0 && !existeNumeroUm) {
                    existeNumeroUm = true;
                }
            }
        }
        return existeNumeroUm;
    }

    public static boolean palavraExisteDaEsquerdaParaDireita(char[][] matrizLetras, char[] palavra) {

        boolean verificaPalavra = false;

        //como vamos verificar da esquerda para a direita é só correr linha a linha.
        //não precisamos de correr a linha toda, só enquanto ainda tiver comprimento suficiente para a palavra
        for (int indiceLinha = 0; indiceLinha < matrizLetras.length; indiceLinha++) {
            for (int indiceColuna = 0; indiceColuna <= (matrizLetras.length - palavra.length) && !verificaPalavra; indiceColuna++) {
                if (matrizLetras[indiceLinha][indiceColuna] == palavra[0] && !verificaPalavra) {
                    verificaPalavra = true;
                    for (int indicePalavra = 1; indicePalavra < palavra.length && verificaPalavra; indicePalavra++) {
                        verificaPalavra = matrizLetras[indiceLinha][indiceColuna + indicePalavra] == palavra[indicePalavra];
                    }
                }
            }
        }
        return verificaPalavra;
    }

    public static boolean palavraExisteDaDireitaParaEsquerda(char[][] matrizLetras, char[] palavra) {

        boolean verificaPalavra = false;

        //como vamos verificar da direita para a esquerda temos que começar na linha zero mas a coluna tem que ser a última (comprimento de matriz mais um
        //não precisamos de correr a linha toda, só enquanto ainda tiver comprimento suficiente para a palavra
        for (int indiceLinha = 0; indiceLinha < matrizLetras.length; indiceLinha++) {
            for (int indiceColuna = matrizLetras.length - 1; indiceColuna >= palavra.length - 1 && !verificaPalavra; indiceColuna--) {
                if (matrizLetras[indiceLinha][indiceColuna] == palavra[0] && !verificaPalavra) {
                    verificaPalavra = true;
                    for (int indicePalavra = 1; indicePalavra < palavra.length && verificaPalavra; indicePalavra++) {
                        verificaPalavra = matrizLetras[indiceLinha][indiceColuna - indicePalavra] == palavra[indicePalavra];
                    }
                }
            }
        }
        return verificaPalavra;
    }

    public static boolean palavraExisteDeCimaParaBaixo(char[][] matrizLetras, char[] palavra) {

        boolean verificaPalavra = false;

        //como vamos verificar de cima para baixo temos que percorrer todas as linhas de uma coluna seguido
        //não precisamos de correr a coluna toda. sabendo o comprimento da palavra se não encontrarmos a primeira letra podemos passar logo para a linha seguinte
        for (int indiceColuna = 0; indiceColuna < matrizLetras.length; indiceColuna++) {
            for (int indiceLinha = 0; indiceLinha <= (matrizLetras.length - palavra.length) && !verificaPalavra; indiceLinha++) {
                if (matrizLetras[indiceLinha][indiceColuna] == palavra[0] && !verificaPalavra) {
                    verificaPalavra = true;
                    for (int indicePalavra = 1; indicePalavra < palavra.length && verificaPalavra; indicePalavra++) {
                        verificaPalavra = matrizLetras[indiceLinha + indicePalavra][indiceColuna] == palavra[indicePalavra];
                    }
                }
            }
        }
        return verificaPalavra;
    }

    public static boolean palavraExisteBaixoParaCima(char[][] matrizLetras, char[] palavra) {

        boolean verificaPalavra = false;

        //como vamos verificar de baixo para cima começamos da última linha.
        //não precisamos de correr a linha toda. sabendo o comprimento da palavra se não encontrarmos a primeira letra podemos passar logo para a linha seguinte
        for (int indiceColuna = 0; indiceColuna < matrizLetras.length; indiceColuna++) {
            for (int indiceLinha = matrizLetras.length - 1; indiceLinha >= palavra.length && !verificaPalavra; indiceLinha--) {
                if (matrizLetras[indiceLinha][indiceColuna] == palavra[0] && !verificaPalavra) {
                    verificaPalavra = true;
                    for (int indicePalavra = 1; indicePalavra < palavra.length && verificaPalavra; indicePalavra++) {
                        verificaPalavra = matrizLetras[indiceLinha - indicePalavra][indiceColuna] == palavra[indicePalavra];
                    }
                }
            }
        }
        return verificaPalavra;
    }

    public static boolean palavraExisteDiagonalDescendenteDireita(char[][] matrizLetras, char[] palavra) {

        boolean verificaPalavra = false;

        //como vamos verificar diagonal descendente para a direita queremos saber se na linha e coluna seguinte a letra é ígual a letra da palavra
        //não precisamos de correr a linha toda. sabendo o comprimento da palavra se não encontrarmos a primeira letra podemos passar logo para a linha seguinte
        for (int indiceLinha = 0; indiceLinha <= (matrizLetras.length - palavra.length); indiceLinha++) {
            for (int indiceColuna = 0; indiceColuna <= (matrizLetras.length - palavra.length) && !verificaPalavra; indiceColuna++) {
                if (matrizLetras[indiceLinha][indiceColuna] == palavra[0] && !verificaPalavra) {
                    verificaPalavra = true;
                    for (int indicePalavra = 1; indicePalavra < palavra.length && verificaPalavra; indicePalavra++) {
                        verificaPalavra = matrizLetras[indiceLinha + indicePalavra][indiceColuna + indicePalavra] == palavra[indicePalavra];
                    }
                }
            }
        }
        return verificaPalavra;
    }

    public static boolean palavraExisteDiagonalDescendenteEsquerda(char[][] matrizLetras, char[] palavra) {
        boolean verificaPalavra = false;

        //para verificar diagonal descendente para a esquerda temos que começar na linha 0 mas na última coluna e comparar a linha e colunha seguintes caso a primeira letra seja igual
        //não precisamos de percorrer a linha toda, só até ao comprimento da palavra
        for (int indiceLinha = 0; indiceLinha <= (matrizLetras.length - palavra.length); indiceLinha++) {
            for (int indiceColuna = matrizLetras.length - 1; indiceColuna >= palavra.length - 1; indiceColuna--) {
                if (matrizLetras[indiceLinha][indiceColuna] == palavra[0] && !verificaPalavra) {
                    verificaPalavra = true;
                    for (int indicePalavra = 1; indicePalavra < palavra.length && verificaPalavra; indicePalavra++) {
                        verificaPalavra = matrizLetras[indiceLinha + indicePalavra][indiceColuna - indicePalavra] == palavra[indicePalavra];
                    }
                }
            }
        }
        return verificaPalavra;
    }

    public static boolean palavraExisteDiagonalAscendenteDireita(char[][] matrizLetras, char[] palavra) {
        boolean verificaPalavra = false;

        //para verificar diagonal ascendente para a direita temos que começar na ultima linha mas na primeira coluna e comparar a linha e colunha seguintes caso a primeira letra seja igual
        //não precisamos de percorrer as linhas todas, só até ao comprimento da palavra
        for (int indiceColuna = 0; indiceColuna <= (matrizLetras.length - palavra.length); indiceColuna++) {
            for (int indiceLinha = matrizLetras.length - 1; indiceLinha >= palavra.length - 1; indiceLinha--) {
                if (matrizLetras[indiceLinha][indiceColuna] == palavra[0] && !verificaPalavra) {
                    verificaPalavra = true;
                    for (int indicePalavra = 1; indicePalavra < palavra.length && verificaPalavra; indicePalavra++) {
                        verificaPalavra = matrizLetras[indiceLinha - indicePalavra][indiceColuna + indicePalavra] == palavra[indicePalavra];
                    }
                }
            }
        }
        return verificaPalavra;
    }

    public static boolean palavraExisteDiagonalAscendenteEsquerda(char[][] matrizLetras, char[] palavra) {
        boolean verificaPalavra = false;

        //para verificar diagonal ascendente para a esquerda temos que começar na ultima linha mas na ultima coluna e comparar a linha e colunha seguintes caso a primeira letra seja igual
        //não precisamos de percorrer as linhas todas, só até ao comprimento da palavra
        for (int indiceLinha = matrizLetras.length - 1; indiceLinha >= palavra.length - 1; indiceLinha--) {
            for (int indiceColuna = matrizLetras.length - 1; indiceColuna >= palavra.length - 1; indiceColuna--) {
                if (matrizLetras[indiceLinha][indiceColuna] == palavra[0] && !verificaPalavra) {
                    verificaPalavra = true;
                    for (int indicePalavra = 1; indicePalavra < palavra.length && verificaPalavra; indicePalavra++) {
                        verificaPalavra = matrizLetras[indiceLinha - indicePalavra][indiceColuna - indicePalavra] == palavra[indicePalavra];
                    }
                }
            }
        }
        return verificaPalavra;
    }

    /**
     * Este método determina se é possível formar uma palavra na matriz dada uma coordenada inicial e uma final
     *
     * @param matrizLetras matriz onde queremos verificar se existe a palavra
     * @param palavra      a verificar
     * @param linha1       linha da cordenada inicial
     * @param coluna1      coluna da cordenada inicial
     * @param linha2       linha da cordenada final
     * @param coluna2      coluna da cordenada final
     * @return se a palavra pode exister naquelas coordenadas ou não
     */
    public static boolean palavraEPossivelNasCoordenadasDadas(char[][] matrizLetras, char[] palavra, int linha1, int coluna1, int linha2, int coluna2) {
        //iniciar um booleano para se a palavra existe ou nao
        boolean palavraEPossivel = false;
        //verificamos primeiro se as coordenadas estão dentro da matriz
        if (linha1>matrizLetras.length-1 || linha2> matrizLetras.length || coluna1>matrizLetras.length || coluna2>matrizLetras.length) {
            return false;
        }
        //iniciar booleano para saber se é da esquerda para a direita
        boolean direcao= ExercicioDEZOITO.verificaDireçaoEsquerdaDireita(coluna1,coluna2);
        boolean possivelNaDirecao=((coluna2-coluna1+1)==palavra.length || (coluna1-coluna2+1)==palavra.length);
        boolean possivelNaOrientacao= ((linha2-linha1+1)==palavra.length || (linha1-linha2+1)==palavra.length);

        //primeiro vamos verificar a direção da palavra pelas coordenadas
        //no caso da linha ser igual significa que está na horizontal só temos que verificar se é para a esquerda ou para a direita
        //Se o tamanho da palavra for igual ao intervalo entre as coordenadas então é possível
        if (linha1 == linha2) {
            if (direcao && possivelNaDirecao) {
                palavraEPossivel = true;
            } else
                palavraEPossivel = !direcao && possivelNaDirecao;
        }

        //no caso da coluna ser igual isso significa que está na vertical, só temos que verificar esquerda e direita
        if (coluna1 == coluna2 && !palavraEPossivel) {
            if (linha1 < linha2 && possivelNaOrientacao) {
                palavraEPossivel = true;
            } else palavraEPossivel = linha1 > linha2 && possivelNaOrientacao;
        }

        //se a diração for diagonal descendente direita a linha1 e coluna 1 são as inferiores a linha2 e coluna2
        //se a direção for diagonal descendente esquerda então a linha1 é inferior a linha 2 mas a coluna 1 é inferior a coluna 2
        if (linha1 < linha2 && possivelNaOrientacao && !palavraEPossivel) {
            if (direcao && possivelNaDirecao) {
                    palavraEPossivel = true;
                } else
                palavraEPossivel = !direcao && possivelNaDirecao;
            }

        //para diagonal ascendente a linha1 é superior a linha dois
        //se for para a direita coluna1 é inferior a coluna2
        //se for para a esquerda a coluna1 é superior a coluna2
        if (linha1>linha2 && possivelNaOrientacao && !palavraEPossivel) {
            if(direcao && possivelNaDirecao) {
                palavraEPossivel=true;
            } else
                palavraEPossivel= !direcao && possivelNaDirecao;
        }

        return palavraEPossivel;
    }

    /**
     * este método determina se uma palavra está da esquerda para a direita. Funciona para horizontais e diagonais
     * @param indiceColuna1 coluna da primeira coordenada
     * @param indiceColuna2 coluna da segunda coordenada
     * @return se for verdadeiro a palavra está da esquerda para a direita, se for falso está da direita para a esquerda
     * Por defeito, se for igual, a palavra não existe mas retorna falso
     */
    public static boolean verificaDireçaoEsquerdaDireita(int indiceColuna1, int indiceColuna2) {
        boolean direcaoEsquerdaDireita=false;

        if(indiceColuna1<indiceColuna2)
            direcaoEsquerdaDireita=true;
        return direcaoEsquerdaDireita;
    }




}