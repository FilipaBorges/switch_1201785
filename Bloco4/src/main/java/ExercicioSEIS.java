public class ExercicioSEIS {

    public static int[] vetorCurto (int [] vetorInicial, int n) {
        //se o n for superior ao tamanho do vetor inicial, o novo vetor vai ficar vazio.
        int[] vetorCurto = new int[n];

        for (int i = 0; i < n; i++) {
            if (n <= vetorInicial.length) {
                vetorCurto[i] = vetorInicial[i];
            }
        }
        return vetorCurto;
    }
}
