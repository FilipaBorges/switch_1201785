public class ExercicioCATORZE {

    public static boolean verificaMatrizRetangular(int [][] matriz) {
        //se uma matriz não for quadrada, é retangular. Então:
        //se não houver colunas não é uma matriz mas sim um vetor. Um vetor nunca pode ser considerado nem quadrado nem retangular
        boolean eRetangular;
        boolean numeroIgualColunas=ExercicioDOZE.matrizComNumeroIgualDeColunas(matriz);
        boolean eQuadrada=ExercicioTREZE.verificaMatrizQuadrada(matriz);

        if (matriz == null || matriz[0].length==0) {
            return eRetangular=false;
        }

        if (numeroIgualColunas && eQuadrada==false){
            return eRetangular=true;
        } else {
            return eRetangular=false;
        }
    }
}
