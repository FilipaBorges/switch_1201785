public class ExercicioDEZASSETE {

    /**
     * Exercicio 17 a: calcula o produto de uma matriz por uma constante
     *
     * @param matriz    matriz inicial
     * @param constante constante pela qual se quer multiplicar a matriz
     * @return vai retornar a matriz já com o produto feito
     */
    public static int[][] produtoMatrizPorConstante(int[][] matriz, int constante) {
        //para este exercicio a matriz tanto pode ser quadrada como retangular.
        //nao pode ser null e se estiver vazia também retorna null

        if (matriz == null || matriz.length < 1) {
            return null;
        }

        if (!ExercicioTREZE.verificaMatrizQuadrada(matriz) && !ExercicioCATORZE.verificaMatrizRetangular(matriz)) {
            return null;
        }

        //vamos iniciar uma nova matriz para armazenar o resultado do produto
        int[][] matrizProduto = new int[matriz.length][matriz[0].length];

        //vamos linha a linha e coluna a coluna fazer a multiplicação e adicionar na nova matriz
        for (int indiceLinha = 0; indiceLinha < matrizProduto.length; indiceLinha++) {
            for (int indiceColuna = 0; indiceColuna < matrizProduto[0].length; indiceColuna++) {
                matrizProduto[indiceLinha][indiceColuna] = matriz[indiceLinha][indiceColuna] * constante;
            }
        }
        return matrizProduto;
    }

    /**
     * Este método faz a soma de duas matrizes de números inteiros
     *
     * @param matriz1 primeira matriz que queremos somar
     * @param matriz2 segunda matriz que queremos somar
     * @return retorna uma matriz com a soma das duas que foram inseridas
     */
    public static int[][] somaDuasMatrizes(int[][] matriz1, int[][] matriz2) {
        //as matrizes têm que ter o mesmo tamanho se não a soma é indefinida
        //ao verificar o tamanho já conseguimos saber se ela é quadrada ou retangular portanto já não precisamos de fazer essa verificação
        //também já faz a verificação para matrizes nulas e/ou vazias
        if (ExercicioDOZE.matrizComNumeroIgualDeColunasEmTodasAsLinhas(matriz1) != ExercicioDOZE.matrizComNumeroIgualDeColunasEmTodasAsLinhas(matriz2) || ExercicioDOZE.matrizComNumeroIgualDeColunasEmTodasAsLinhas(matriz1) == -1) {
            return null;
        } else
            //o número de linhas também tem que ser verificado
            if (matriz1.length != matriz2.length) {
                return null;
            }

        //vamos iniciar uma matriz para colocar a soma das duas.
        int[][] matrizSoma = new int[matriz1.length][matriz1[0].length];


        for (int indiceLinha = 0; indiceLinha < matrizSoma.length; indiceLinha++) {
            for (int indiceColuna = 0; indiceColuna < matrizSoma[0].length; indiceColuna++) {
                matrizSoma[indiceLinha][indiceColuna] = matriz1[indiceLinha][indiceColuna] + matriz2[indiceLinha][indiceColuna];
            }
        }
        return matrizSoma;
    }

    /**
     * Método que calcula o produto de duas matrizes de igual número de linhas e colunas
     *
     * @param matriz1
     * @param matriz2
     * @return matriz final com o produto das matrizes
     */
    public static int[][] produtoDuasMatrizes(int[][] matriz1, int[][] matriz2) {
        //as matrizes têm que ter o mesmo tamanho
        //ao verificar o tamanho já conseguimos saber se ela é quadrada ou retangular portanto já não precisamos de fazer essa verificação
        //também já faz a verificação para matrizes nulas e/ou vazias
        if (ExercicioDOZE.matrizComNumeroIgualDeColunasEmTodasAsLinhas(matriz1) != ExercicioDOZE.matrizComNumeroIgualDeColunasEmTodasAsLinhas(matriz2) || ExercicioDOZE.matrizComNumeroIgualDeColunasEmTodasAsLinhas(matriz1) == -1) {
            return null;
        } else
            //o número de linhas também tem que ser verificado
            if (matriz1.length != matriz2.length) {
                return null;
            }

        //vamos iniciar uma matriz para colocar o produto das duas.
        int[][] matrizProduto = new int[matriz1.length][matriz1[0].length];


        for (int indiceLinha = 0; indiceLinha < matrizProduto.length; indiceLinha++) {
            for (int indiceColuna = 0; indiceColuna < matrizProduto[0].length; indiceColuna++) {
                //para cada produtosMatrizes[indiceLinha][indiceColuna]
                //soma-se o produto da linha(matriz1) e coluna(matriz2)
                for (int i = 0; i < matriz1[indiceLinha].length; i++) {
                    matrizProduto[indiceLinha][indiceColuna] += matriz1[indiceLinha][i] * matriz2[i][indiceColuna];
                }
            }
        }
        return matrizProduto;
    }
}
