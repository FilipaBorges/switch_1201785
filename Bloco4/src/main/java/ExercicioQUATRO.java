public class ExercicioQUATRO {

    public static int contarPares ( int [] numero) {
        int pares=0;

        for (int i=0; i< numero.length; i++) {
            if (numero[i]%2==0) {
                pares++;
            }
        }
        return pares;
    }

    public static int [] vetorPares (int [] numero) {
        int [] vetorPares = new int [contarPares(numero)];
        int indicePares=0;

        for (int i=0; i< numero.length; i++)
            if (numero[i]%2==0) {
                vetorPares[indicePares]= numero[i];
                indicePares++;
            }

        return vetorPares;
    }

    public static int contarImpar( int [] numero) {
        int impar=0;

        for (int i=0; i< numero.length; i++) {
            if (numero[i]%2==0) {
            } else
                impar++;
        }
        return impar;
    }

    public static int [] vetorImpares (int [] numero) {
        int [] vetorImpares = new int [contarImpar(numero)];
        int indiceImpares=0;

        for (int i=0; i< numero.length; i++) {
            if (numero[i]%2==0) {
            }else
            {vetorImpares[indiceImpares]= numero[i];
                indiceImpares++;
            }
        }
        return vetorImpares;
    }
}
