public class ExercicioONZE {

    public static Integer produtoEscalarDoisVetores (int[] array1, int[] array2) {
        int produtoEscalar = 0;
        //considerando que o produto escalar entre dois vetores só é possivel para vetores com o mesmo comprimento
        if (array1==null || array2==null || array1.length<1 ||  array1.length != array2.length) {
            return null;
        } else {
            //para cada elemento do array um vamos multiplicar o elemento com o mesmo índice do array 2
            for (int i = 0; i < array1.length; i++) {
                produtoEscalar += array1[i] * array2[i];
            }
        }
        return produtoEscalar;
    }
}
