import java.util.Scanner;

public class ExercicioDEZANOVE {
    public static void main(String[] args) {
        jogarSudoku();
    }

    public static void jogarSudoku() {
        Scanner ler = new Scanner(System.in);

        int[][] matrizInicial = {{1, 4, 0, 0, 0, 0, 2, 0, 6},
                {0, 0, 0, 1, 0, 2, 0, 0, 0},
                {0, 0, 2, 0, 0, 0, 1, 0, 5},
                {5, 0, 0, 2, 1, 0, 0, 9, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {4, 0, 0, 8, 0, 9, 5, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 1, 0, 7, 0, 0, 4, 0, 0},
                {3, 0, 0, 9, 0, 0, 7, 0, 0}};

        System.out.println("A sua matriz de jogo é:");
        imprimeMatriz(matrizInicial);

        do {
            System.out.println();
            System.out.println("Se deseja colocar um número marque 1.");
            System.out.println("Se deseja retirar um número marque 2.");
            int acaoAFazer = ler.nextInt();
            if ( acaoAFazer < 1 || acaoAFazer > 2 ) {
                System.out.println("Essa ação não existe, por favor remarque: ");
                acaoAFazer = ler.nextInt();
            }
            System.out.println("Indique o número a colocar/retirar:");
            int numero = ler.nextInt();
            System.out.println("Indique a linha:");
            int linha = ler.nextInt() - 1;
            System.out.println("Indique a coluna:");
            int coluna = ler.nextInt() - 1;
            if ( !verificaLimiteENumero(numero, linha, coluna) ) {
                System.out.println("Por favor verifique os dados introduzidos.");
            }

            if ( acaoAFazer == 1 && verificaLimiteENumero(numero, linha, coluna) ) {
                matrizInicial = matrizAposAdicaoDeNumero(matrizInicial, numero, linha, coluna);
                imprimeMatriz(matrizInicial);
            } else {
                matrizInicial = matrizAposRetirarNumero(matrizInicial, numero, linha, coluna);
                imprimeMatriz(matrizInicial);
            }
        } while (!verificaSeTodaAMatrizEstaPreenchida(matrizInicial));
        imprimeMatriz(matrizInicial);
        System.out.println("Terminou o jogo!");
    }

    public static void imprimeMatriz(int[][] matriz) {
        for (int indiceLinha = 0; indiceLinha < matriz.length; indiceLinha++) {
            System.out.println();
            for (int indiceColuna = 0; indiceColuna < matriz[indiceLinha].length; indiceColuna++) {
                System.out.print(" " + matriz[indiceLinha][indiceColuna]);
            }
        }
    }

    /**
     * Método que nós dá a matriz máscara de uma matriz de jogo num dado momento
     *
     * @param matriz matriz inicial já com algumas células preenchidas
     * @return matriz onde as células preenchidas têm valor 1 e as vazias valor 0.
     */
    public static int[][] matrizMascaraDeMatriz(int[][] matriz) {
        //não vamos verificar se os números da matriz inicial são entre 1 e 9 porque se foram introduzidos supomos que estejam dentro destes critérios
        //a matriz mascara vai ter as mesmas dimensões da matriz inicial
        int[][] matrizMascara = new int[matriz.length][matriz.length];

        for (int indiceLinha = 0; indiceLinha < matrizMascara.length; indiceLinha++) {
            for (int indiceColuna = 0; indiceColuna < matrizMascara.length; indiceColuna++) {
                if ( matriz[indiceLinha][indiceColuna] >= 1 ) {
                    matrizMascara[indiceLinha][indiceColuna] = 1;
                } else
                    matrizMascara[indiceLinha][indiceColuna] = 0;
            }
        }
        return matrizMascara;
    }

    /**
     * Método que nos diz se ainda existem células vazias ou não
     *
     * @param matrizMascara do jogo naquele momento
     * @return sim ou não
     */
    public static boolean verificaCelulasPorPreencher(int[][] matrizMascara) {
        boolean verificaCelulaVazia = false;

        for (int indiceLinha = 0; indiceLinha < matrizMascara.length && !verificaCelulaVazia; indiceLinha++) {
            for (int indiceColuna = 0; indiceColuna < matrizMascara.length && !verificaCelulaVazia; indiceColuna++) {
                verificaCelulaVazia = (matrizMascara[indiceLinha][indiceColuna] == 0);
            }
        }
        return verificaCelulaVazia;
    }

    /**
     * Método que determina a matriz máscara de um determinado número numa dada matriz de jogo
     *
     * @param matrizJogo no momento em que queremos saber a posição de um dado número
     * @param numero     que queremos saber a posição
     * @return matriz de 1s e 0s onde o 1 nos diz onde está o número pretendido
     */
    public static int[][] matrizMascaraDeUmNumero(int[][] matrizJogo, int numero) {
        if ( numero < 1 || numero > 9 ) {
            return null;
        }

        int[][] matrizMascaraNumero = new int[matrizJogo.length][matrizJogo.length];

        for (int indiceLinha = 0; indiceLinha < matrizJogo.length; indiceLinha++) {
            for (int indiceColuna = 0; indiceColuna < matrizJogo.length; indiceColuna++) {
                if ( matrizJogo[indiceLinha][indiceColuna] != numero ) {
                    matrizMascaraNumero[indiceLinha][indiceColuna] = 0;
                } else
                    matrizMascaraNumero[indiceLinha][indiceColuna] = 1;
            }
        }
        return matrizMascaraNumero;
    }

    public static boolean verificaLimiteENumero(int numero, int linha, int coluna) {
        boolean verificaLimite = true;
        if ( linha < 0 || linha > 9 || coluna < 0 || coluna > 8 || numero < 1 || numero > 8 ) {
            verificaLimite = false;
        }
        return verificaLimite;
    }


    /**
     * Método que retorna a matriz de jogo após jogada
     *
     * @param matriz matriz antes da jogada
     * @param numero número a adicionar
     * @param linha  onde queremos adicionar o número
     * @param coluna onde queremos adicionar o número
     * @return matriz com o número pretendido na célula pretendida
     */
    public static int[][] matrizAposAdicaoDeNumero(int[][] matriz, int numero, int linha, int coluna) {
        //se a jogada for invalida retorna a matriz inicial
        if ( !verificaLimiteENumero(numero, linha, coluna) ) {
            return matriz;
        }

        if ( verificaSeNumeroPodeSerAdicionado(matriz, numero, linha, coluna) ) {
            matriz[linha][coluna] = numero;
        }

        return matriz;

      /*  //criar a nova matriz que vai ser em tudo igual a inicial com exceção do algarismo a adicionar
        int[][] matrizAposJogada = new int[matriz.length][matriz.length];

        for (int indiceLinha = 0; indiceLinha < matrizAposJogada.length; indiceLinha++) {
            for (int indiceColuna = 0; indiceColuna < matrizAposJogada.length; indiceColuna++) {
                //subtraimos 1 à linha e coluna dada pelo utilizador porque ele não conta com o zero
                if ( indiceLinha != (linha - 1) || indiceColuna != (coluna - 1) ) {
                    matrizAposJogada[indiceLinha][indiceColuna] = matriz[indiceLinha][indiceColuna];
                } else
                    matrizAposJogada[linha - 1][coluna - 1] = numero;
            }
        }*/
    }

    /**
     * método que retira o número de uma matriz
     *
     * @param matrizInicial matriz onde queremos verificar se é possivel retirar numero
     * @param numero        numero a retirar
     * @param linha         onde está o numero
     * @param coluna        onde está o numero
     * @return matriz sem o numero
     */
    public static int[][] matrizAposRetirarNumero(int[][] matrizInicial, int numero, int linha, int coluna) {
        if ( !verificaLimiteENumero(numero, linha, coluna) ) {
            return matrizInicial;
        }

        if ( matrizInicial[linha][coluna] == numero ) {
            matrizInicial[linha][coluna] = 0;
        }

        return matrizInicial;

        //criar a nova matriz que vai ser em tudo igual a inicial com exceção do algarismo a retirar
        /*int[][] matrizAposJogada = new int[matrizInicial.length][matrizInicial.length];

        for (int indiceLinha = 0; indiceLinha < matrizAposJogada.length; indiceLinha++) {
            for (int indiceColuna = 0; indiceColuna < matrizAposJogada.length; indiceColuna++) {
                //subtraimos 1 à linha e coluna dada pelo utilizador porque ele não conta com o zero
                if ( indiceLinha != (linha - 1) || indiceColuna != (coluna - 1) ) {
                    matrizAposJogada[indiceLinha][indiceColuna] = matrizInicial[indiceLinha][indiceColuna];
                } else
                    matrizAposJogada[linha - 1][coluna - 1] = 0;
            }
        }*/
    }

    /**
     * Verifica se um número pode ou não ser adicionado
     *
     * @param matrizInicial onde queremos adicionar o número
     * @param numero        que queremos adicionar
     * @param linha         onde querermos colocar o numero
     * @param coluna        onde queremos colocar o numero
     * @return se é possivel ou não
     */
    public static boolean verificaSeNumeroPodeSerAdicionado(int[][] matrizInicial, int numero, int linha, int coluna) {
        //primeiro verificamos se a celula está vazia
        boolean jogadaValida = true;

        if ( !estaVaziaACelula(matrizInicial, linha, numero) ) {
            jogadaValida = false;
        }
        //para saber se o número pode ou não ser adicionado, para além de ter que ser de 1-9
        //também não se pode repetir na mesma linha, nem coluna, nem na matriz 3*3 em que está inserido
        //para isso é mais fácil verificar a matriz máscara do número que queremos utilizar no antes do jogador inserir
        int[][] matrizMascara = matrizMascaraDeUmNumero(matrizInicial, numero);

        for (int indiceColuna = 0; indiceColuna < 9 && jogadaValida; indiceColuna++) {
            if ( matrizMascara[linha][indiceColuna] == 1 && jogadaValida ) {
                jogadaValida = false;
            }
        }
        for (int indiceLinha = 0; indiceLinha < 9 && jogadaValida; indiceLinha++) {
            if ( matrizMascara[indiceLinha][coluna] == 1 && jogadaValida ) {
                jogadaValida = false;
            }
        }
        if ( !verificaValidadeNoQuadrante(matrizMascara, linha, coluna) && jogadaValida ) {
            jogadaValida = false;
        }

        return jogadaValida;
    }

    public static boolean verificaValidadeNoQuadrante(int[][] matrizMascara, int linha, int coluna) {
        int i, j;
        boolean validoNoQuadrante = true;

        if ( linha >= 0 && linha <= 2 ) {
            for (i = 0; i <= 2; i++) {
                if ( coluna >= 0 && coluna <= 2 ) {
                    for (j = 0; j <= 2; j++) {
                        if ( matrizMascara[i][j] == 1 && validoNoQuadrante ) {
                            validoNoQuadrante = false;
                        }
                    }
                } else if ( coluna > 2 && coluna <= 5 ) {
                    for (j = 3; j <= 5; j++) {
                        if ( matrizMascara[i][j] == 1 && validoNoQuadrante ) {
                            validoNoQuadrante = false;
                        }
                    }
                } else if ( coluna > 5 && coluna <= 9 ) {
                    for (j = 6; j <= 8; j++) {
                        if ( matrizMascara[i][j] == 1 && validoNoQuadrante ) {
                            validoNoQuadrante = false;
                        }
                    }
                }
            }
        } else if ( linha > 2 && linha <= 5 ) {
            for (i = 3; i <= 5; i++)
                if ( coluna >= 0 && coluna <= 2 ) {
                    for (j = 0; j <= 2; j++) {
                        if ( matrizMascara[i][j] == 1 && validoNoQuadrante ) {
                            validoNoQuadrante = false;
                        }
                    }
                } else if ( coluna > 2 && coluna <= 5 ) {
                    for (j = 3; j <= 5; j++) {
                        if ( matrizMascara[i][j] == 1 && validoNoQuadrante ) {
                            validoNoQuadrante = false;
                        }
                    }
                } else if ( coluna > 5 && coluna <= 9 ) {
                    for (j = 6; j <= 8; j++) {
                        if ( matrizMascara[i][j] == 1 && validoNoQuadrante ) {
                            validoNoQuadrante = false;
                        }
                    }
                }
        } else if ( linha > 5 && linha <= 8 ) {
            for (i = 6; i <= 8; i++)
                if ( coluna >= 0 && coluna <= 2 ) {
                    for (j = 0; j <= 2; j++) {
                        if ( matrizMascara[i][j] == 1 && validoNoQuadrante ) {
                            validoNoQuadrante = false;
                        }
                    }
                } else if ( coluna > 2 && coluna <= 5 ) {
                    for (j = 3; j <= 5; j++) {
                        if ( matrizMascara[i][j] == 1 && validoNoQuadrante ) {
                            validoNoQuadrante = false;
                        }
                    }
                } else if ( coluna > 5 && coluna <= 9 ) {
                    for (j = 6; j <= 8; j++) {
                        if ( matrizMascara[i][j] == 1 && validoNoQuadrante ) {
                            validoNoQuadrante = false;
                        }
                    }
                }
        }
        return validoNoQuadrante;
    }

    /**
     * método que verifica se a célula está vazia
     *
     * @param matrizInicial matriz onde queremos adicionar o numero
     * @param linha         onde queremos adicionar o numero
     * @param coluna        onde queremos adicionar o numero
     * @return se a célula está vazia ou não
     */
    public static boolean estaVaziaACelula(int[][] matrizInicial, int linha, int coluna) {
        boolean celulaVazia = true;

        if ( matrizInicial[linha][coluna] != 0 )
            celulaVazia = false;

        return celulaVazia;
    }

    /**
     * Este método verifica se toda a matriz está preenchida
     * não preciso de verificar se a matriz está valida porque faço a validação a cada adiçao de numero
     * @param matriz a verificar
     * @return sim ou não
     */
    public static boolean verificaSeTodaAMatrizEstaPreenchida(int[][] matriz) {
        boolean matrizPreenchida = true;

        for (int indiceLinha = 0; indiceLinha < 9 && matrizPreenchida; indiceLinha++) {
            for (int indiceColuna = 0; indiceColuna < 9 && matrizPreenchida; indiceColuna++) {
                if ( matriz[indiceLinha][indiceColuna] == 0 ) {
                    matrizPreenchida = false;
                }
            }
        }
        return matrizPreenchida;
    }
}
