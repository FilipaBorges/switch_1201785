public class ExercicioTREZE {

    public static boolean verificaMatrizQuadrada(int[][] matriz) {
        //se não houver colunas não é uma matriz mas sim um vetor. Um vetor nunca pode ser quadrado
        boolean eQuadrada=false;

        if (ExercicioDOZE.matrizComNumeroIgualDeColunas(matriz)) {
            if (matriz.length == matriz[0].length) {
                eQuadrada=true;
            }
        }
        return eQuadrada;
    }
}
