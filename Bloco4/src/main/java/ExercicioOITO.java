public class ExercicioOITO {

   public static int[] multiplosComuns(int limiteMin, int limiteMax, int[] numeros) {
       //primeiro verificamos se o limite minimo é o valor menor entre o mínimo e o máx
       int [] limites= ExercicioSETE.verificarLimites(limiteMin,limiteMax);
       //vamos atribuir o tamanho do array o tamanho do intervalo que queremos verificar e no fim redimensionamos
        int tamanho= limites[1]-limites[0];
        //vamos primeiro assumir o tamanho maior, que é o intervalo completo, e no fim redimensonamos o array
        int[] multiplosComuns=new int[tamanho];
        //inicio um contador para os números que são multiplos de ambos os números
       int contador=0;

       //se o array dos números for null ou se o array for vazio ou só tenha um número, então o array de multiplos comuns é null
        if (numeros==null || numeros.length<2)
            return null;
        //no primeiro loop vou correr todos os números do intervalo
        for (int i=limites[0]; i<= limites[1];i++){
            //inicio um booleano para quando for multiplo
            boolean multiplo=true;
            //no segundo loop vejo se é multiplo do primeiro número e do segundo
            for (int j=0;j< numeros.length && multiplo;j++){
                multiplo= i%numeros[j]==0;
                }
            if(multiplo) {
                multiplosComuns[contador]=i;
                contador++;
            }
        }
        return redimensionarArray(multiplosComuns, contador);
    }

    public static int [] redimensionarArray (int[] array, int tamanho){
        //se queremos redimensionar um array para mais pequeno o tamanho final nunca pode ser maior que o inicial
        //temos que garantir que o array inicial não é nulo
        if (array== null || array.length<tamanho) {
            return null;
        }

        int[] arrayRedimensionado= new int[tamanho];

        for (int i=0; i<tamanho; i++) {
            arrayRedimensionado[i]=array[i];
        }
        return arrayRedimensionado;
    }
}
