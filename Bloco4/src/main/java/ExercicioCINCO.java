public class ExercicioCINCO {

    public static int somaPares (int numero) {
        int[] numArray= ExercicioDOIS.converterArray(numero);
        int[] vetorPares= ExercicioQUATRO.vetorPares(numArray);
        int somaPar=0;

        for (int indice=0; indice< vetorPares.length; indice++) {
            somaPar+= vetorPares[indice];
        }
        return somaPar;
    }

    public static int somaImpares (int numero) {
        int[] numArray= ExercicioDOIS.converterArray(numero);
        int[] vetorImpares= ExercicioQUATRO.vetorImpares(numArray);
        int somaImpar=0;

        for (int indice=0; indice< vetorImpares.length; indice++) {
            somaImpar+= vetorImpares[indice];
        }
        return somaImpar;
    }
}
