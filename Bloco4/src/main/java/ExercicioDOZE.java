public class ExercicioDOZE {

    public static int matrizComNumeroIgualDeColunasEmTodasAsLinhas(int[][] matriz) {
        int numeroColunas = 0;
        boolean numeroIgualColunas = true;

        //se a matriz for null ou não tiver linhas(não é matriz) não pode ter colunas portanto retorna -1
        if(matriz==null || matriz.length<1) {
            numeroColunas=-1;
            return numeroColunas;
        }
        for (int linha = 0; linha < matriz.length; linha++) {

            for (int coluna = 0; coluna<matriz[linha].length && numeroIgualColunas; coluna++) {

                if (matriz[linha].length == matriz[0].length) {
                    numeroColunas=matriz[linha].length;
                    numeroIgualColunas = true;
                } else {
                    numeroIgualColunas = false;
                    numeroColunas = -1;
                }

            }
        }
        return numeroColunas;
    }

    public static boolean matrizComNumeroIgualDeColunas(int[][] matriz) {
        boolean numeroIgualColunas = true;

        //se a matriz for null ou não tiver linhas(não é matriz) não pode ter colunas portanto retorna -1
        if(matriz==null || matriz.length<1) {
            return false;
        }
        for (int linha = 0; linha < matriz.length; linha++) {

            for (int coluna = 0; coluna<matriz[linha].length && numeroIgualColunas; coluna++) {

                if (matriz[linha].length == matriz[0].length) {
                    numeroIgualColunas = true;
                } else {
                    numeroIgualColunas = false;
                }

            }
        }
        return numeroIgualColunas;
    }
}
