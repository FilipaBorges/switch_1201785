public class ExercicioDEZ {

    public static Integer menorValorVetor (int[] vetor) {
        //se o vetor for null ou estiver vazio retorna null
        //Não pode ser -1 porque o menor valor pode tomar este valor.
        if (vetor== null || vetor.length<1) {
            return null;
        }
        //assumo que o primeiro valor do vetor é o menor, se entretanto houver algum menor em comparação com este faço a troca
        int menor=vetor[0];

        //o indice é iniciado a um porque já adicionei o indice zero na condição de menor
        for(int indice=1; indice< vetor.length; indice++){
            if(vetor[indice]<menor) {
                menor=vetor[indice];
            }
        } return menor;
    }

    public static Integer maiorValorVetor (int[] vetor) {
        //se o vetor for null ou estiver vazio retorna null
        //Não pode ser -1 porque o maior valor pode tomar este valor.
        if (vetor== null || vetor.length<1) {
            return null;
        }
        //assumo que o primeiro numero é o maior. Depois corro os restantes números do array e comparo.
        //se o número seguinte for maior que o anterior então faz-se a substituição
        int maior=vetor[0];

        for(int indice=1; indice< vetor.length; indice++){
            if(vetor[indice]>maior) {
                maior=vetor[indice];
            }
        } return maior;
    }

    public static Double valorMedioVetor(int [] vetor) {
        //se o vetor for null ou estiver vazio retorna null
        //Não pode ser -1 porque a média pode tomar este valor.
        if (vetor== null || vetor.length<1) {
            return null;
        }
        //inicio a soma a zero para ir adicionando os algarismos do array aqui
        //não é preciso iniciar contador para o número de algarismos porque uso o array.length
        double soma=0;

        for (int indice=0; indice<vetor.length;indice++){
            soma+= vetor[indice];
        }
        double media=soma/(vetor.length);
        return media;
    }

    public static Double produtoElementosVetor(int [] vetor) {
        //se o vetor for null ou estiver vazio retorna -1
        if (vetor== null || vetor.length<1) {
            return null;
        }
        //quando se trata de produto não podemos iniciar a 0 pois vai absorver todos os algarismos pelo qual multiplicasmos.
        //o 1 é o elemento neutro da multiplicação
        double produto=1;

        for (int indice=0; indice<vetor.length; indice++) {
            produto*=vetor[indice];
        }
        return produto;
    }

    public static int [] vetorValoresNaoRepetidos (int [] array) {
        if (array==null) {
            return null;
        }
        //não precisamos de ordenar o array por ordem crescente uma vez que temos a condição dentro do loop
        int [] valoresUnicos= new int[array.length];
        //vou usar o comprimento inicial do array e no fim ajusto o tamanho consoante os números não repetidos que tenho
        //Inicio o novo array com o primeiro valor assumindo que é único e comparo os outros a partir daí
        valoresUnicos[0]=array[0];
        int contador=0;
        //iniciar um boleano para o numero ser ou não unico
        boolean unico= true;

        for (int i=0; i< array.length;i++) {
            //primeiro corro todos os números do array e depois verifico um a um se é igual a algum dos anteriores.
            //se não for igual a nenhum dos anteriores então introduzo o valor no novo vetor e somo um no contador.
            for (int j=0; j<i && unico; j++) {
                unico = valoresUnicos[j] != array[i];
            }
            if (unico) {
                valoresUnicos[contador]=array[i];
                contador++;
            }
            unico= true;
        }
        return ExercicioOITO.redimensionarArray(valoresUnicos, contador);
    }


    public static int [] vetorInverso(int [] vetor) {
        //se o vetor for null ou estiver vazio retorna null
        if ( vetor==null || vetor.length<1) {
            return null;
        }

        int [] vetorInverso= ExercicioNOVE.inverterArray(vetor);

        return vetorInverso;
    }

    public static boolean ePrimo(int num) {
        boolean ePrimo = true;

        if (num == 0 || num == 1) {
            ePrimo = false;
        } else
            for (int i = 2; i < num && ePrimo; i++) {
                if (num % i == 0) {
                    ePrimo = false;
                }
            }
            return ePrimo;
    }

    public static int [] elementosPrimos(int [] vetor) {
        //se o vetor for null ou estiver vazio retorna null
        if (vetor==null || vetor.length<1) {
            return null;
        }
        //um número primo não pode ter mais divisores para além de um e ele próprio
        //iniciamos com o tamanho inicial e no fim ajustamos
        int[] numerosPrimos= new int[vetor.length];
        int contador=0;

        boolean ePrimo=true;

        //Primeiro loop para correr todos os valores do array
        for(int i=0; i<vetor.length;i++) {
              //segundo loop para ver se o número do array é divisivel por algum número sem ser 1 ou ele próprio
            if (vetor[i]==0 || vetor[i]==1){
                ePrimo=false;
            } else {
                for (int j=2; j<vetor[i] && ePrimo; j++) {
                ePrimo= vetor[i]%j !=0;
                }
            }

            if (ePrimo) {
                 numerosPrimos[contador]=vetor[i];
                 contador++;
            }
            ePrimo=true;
        }
        return ExercicioOITO.redimensionarArray(numerosPrimos, contador);
    }

}
