public class ExercicioQUINZE {

    public static Integer elementoMenorValorMatriz(int[][] matriz) {
        //se a matriz for null ou estiver vazia, não há elementos então retorna null
        if (matriz == null || matriz.length < 1) {
            return null;
        }
        //começo por assumir que o menor valor da matriz é [0][0] e a partir daí comparo sempre com o anterior
        //se for menor substitui o valor, se não mantem o anterior
        int menor = matriz[0][0];

        //para cada linha verifico todas as colunas
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                if (matriz[linha][coluna] < menor) {
                    menor = matriz[linha][coluna];
                }
            }
        }
        return menor;
    }

    public static Integer elementoMaiorValorMatriz(int[][] matriz) {
        //se a matriz for null ou estiver vazia, não há elementos então retorna null
        if (matriz == null || matriz.length < 1) {
            return null;
        }
        //começo por assumir que o menor valor da matriz é [0][0] e a partir daí comparo sempre com o anterior
        //se for menor substitui o valor, se não mantem o anterior
        int maior = matriz[0][0];

        //para cada linha verifico todas as colunas
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                if (matriz[linha][coluna] > maior) {
                    maior = matriz[linha][coluna];
                }
            }
        }
        return maior;
    }

    public static Double mediaElementosMatriz(int[][] matriz) {
        //se a matriz for null ou estiver vazia, não há elementos então retorna null
        if (matriz == null || matriz.length < 1) {
            return null;
        }
        //iniciar uma variável onde fica a soma dos elementos e outra com o número total de elementos
        double soma = 0, totalElementos = 0;

        //corro todas as linhas e todas as colunas
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                soma += matriz[linha][coluna];
                totalElementos++;
            }
        }
        return soma / totalElementos;
    }

    public static Double produtosElementosMatriz(int[][] matriz) {
        //se a matriz for null ou estiver vazia, não há elementos então retorna null
        if (matriz == null || matriz.length < 1) {
            return null;
        }
        //iniciar uma variável onde fica o produto dos elementos.
        //Não posso iniciar a 0 se não o produto final vai sempre ser zero
        double produtoTotalElementos = 1;

        //corro todas as linhas e todas as colunas
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                produtoTotalElementos *= matriz[linha][coluna];
            }
        }
        return produtoTotalElementos;
    }

    public static int[] valoresUnicosMatriz(int[][] matriz) {
        //se a matriz for null ou estiver vazia, não há elementos então retorna null
        if (matriz == null || matriz.length < 1) {
            return null;
        }

        //este método só funciona para raizes que sejam quadradas ou retangulares:
        if (!ExercicioTREZE.verificaMatrizQuadrada(matriz) && !ExercicioCATORZE.verificaMatrizRetangular(matriz)) {
            return null;
        }

        // iniciar um vetor para ir colocar os valores não repetidos da matriz
        //inicio o tamanho com o total dos elementos da matriz (comprimento da linha*coluna)
        int[] valoresUnicos = new int[(matriz.length * matriz[0].length)];
        //coloco o primeiro valor na nova matriz e depois comparo a partir daqui
        valoresUnicos[0] = matriz[0][0];
        int numElementosVetor = 1;
        boolean naoUnico;

        //primeiro loop para analisar linha a linha
        for (int linha = 0; linha < matriz.length; linha++) {

            //segundo loop para cada coluna
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                naoUnico = false;
                for (int i = 0; i < numElementosVetor; i++) {
                    naoUnico = matriz[linha][coluna] == valoresUnicos[i];

                    if (naoUnico) {
                        i = numElementosVetor;
                    }
                }
                if (!naoUnico) {
                    valoresUnicos[numElementosVetor] = matriz[linha][coluna];
                    numElementosVetor++;
                }
            }
        }
        return ExercicioOITO.redimensionarArray(valoresUnicos, numElementosVetor);
    }

    public static int[] elementosPrimosMatriz(int[][] matriz) {
        //se a matriz for null ou estiver vazia, não há elementos então retorna null
        if (matriz == null || matriz.length < 1) {
            return null;
        }

        //este método só funciona para raizes que sejam quadradas ou retangulares:
        if (!ExercicioTREZE.verificaMatrizQuadrada(matriz) && !ExercicioCATORZE.verificaMatrizRetangular(matriz)) {
            return null;
        }

        //iniciar vetor para colocar os elementos primos
        int[] elementosPrimos = new int[(matriz.length * matriz[0].length)];
        int numElementosVetor = 0;

        //primeiro loop para analisar linha a linha
        for (int linha = 0; linha < matriz.length; linha++) {

            //segundo loop para cada coluna
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {

                if (ExercicioDEZ.ePrimo(matriz[linha][coluna])) {
                    elementosPrimos[numElementosVetor] = matriz[linha][coluna];
                    numElementosVetor++;
                }
            }
        }
        return ExercicioOITO.redimensionarArray(elementosPrimos, numElementosVetor);
    }

    public static int[] diagonalPrincipal(int[][] matriz) {
        //se a matriz for null ou estiver vazia, não há elementos então retorna null
        if (matriz == null || matriz.length < 1) {
            return null;
        }

        //este método só funciona para raizes que sejam quadradas ou retangulares:
        if (!ExercicioTREZE.verificaMatrizQuadrada(matriz) && !ExercicioCATORZE.verificaMatrizRetangular(matriz)) {
            return null;
        }

        int[] diagonalPrincipal = new int[(matriz.length * matriz[0].length)];
        //O primeiro valor da matriz faz sempre parte da diagonal principal
        diagonalPrincipal[0] = matriz[0][0];
        int numElemDiagonal = 1;

        for (int i = 1; i < matriz.length; i++) {
            for (int j = i; j == i; j++) {
                diagonalPrincipal[numElemDiagonal] = matriz[i][j];
                numElemDiagonal++;
            }
        }
        return ExercicioOITO.redimensionarArray(diagonalPrincipal, numElemDiagonal);
    }

    public static int[] diagonalSecundaria(int[][] matriz) {
        //se a matriz for null ou estiver vazia, não há elementos então retorna null
        if (matriz == null || matriz.length < 1) {
            return null;
        }

        //este método só funciona para raizes que sejam quadradas ou retangulares:
        if (!ExercicioTREZE.verificaMatrizQuadrada(matriz) && !ExercicioCATORZE.verificaMatrizRetangular(matriz)) {
            return null;
        }

        int[] diagonalSecundaria = new int[(matriz.length * matriz[0].length)];
        //O último valor da primeira linha faz sempre parte da diagonal secundária

        diagonalSecundaria[0] = matriz[0][matriz[0].length - 1];
        int numElemDiagonal = 1;

        for (int i = 1; i < matriz.length; i++) {
            for (int j = (matriz[0].length - i) - 1; j == (matriz[0].length - i) - 1; j--) {
                diagonalSecundaria[numElemDiagonal] = matriz[i][j];
                numElemDiagonal++;
            }
        }
        return ExercicioOITO.redimensionarArray(diagonalSecundaria, numElemDiagonal);
    }

    public static boolean matrizIdentidade(int[][] matriz) {
        boolean eIdentidade = true;
        //uma matriz identidade quem que ser quadrada
        if (!ExercicioTREZE.verificaMatrizQuadrada(matriz)) {
            return eIdentidade = false;
        }

        //uma matriz identidade é uma matriz cuja diagonal primária é só composta por números 1
        //e todos os outros valores são zero;

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                if (i == j && matriz[i][j] != 1 || i != j && matriz[i][j] != 0) {
                    eIdentidade = false;
                }
            }
        }
        return eIdentidade;
    }

    public static double[][] matrizInversa(int[][] matriz) {
        double[][] matrizInversa = new double[matriz.length][matriz.length];

        //Primeiro verificamos se é quadrada
        //O determinante tem que ser diferente de 0 para ser inversivel
        if (!ExercicioTREZE.verificaMatrizQuadrada(matriz) || ExercicioDEZASSEIS.determinanteMatrizTeoremaLaPlace(matriz) == 0 || matriz.length == 1) {
            return null;

        } else
            //se for matriz de ordem dois
            if (matriz.length == 2) {
            matrizInversa = matrizInversaOrdemDois(matriz);

        } else
            if (matriz.length>2)
            matrizInversa = matrizInversaOrdemTres(matriz);


        return matrizInversa;
    }

    public static double[][] matrizInversaOrdemDois(int[][] matriz) {
        double[][] matrizInversa = new double[matriz.length][matriz[0].length];

        int indiceLinhaAuxiliar = matriz.length - 1, indiceColunaAuxiliar = matriz.length - 1;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                if ((i + j) % 2 != 0 && (double) matriz[i][j] / ExercicioDEZASSEIS.determinanteMatrizTeoremaLaPlace(matriz) != 0.0) {
                    matrizInversa[indiceLinhaAuxiliar][indiceColunaAuxiliar] = ((double) matriz[i][j] / ExercicioDEZASSEIS.determinanteMatrizTeoremaLaPlace(matriz)) * -1; // inverter para negativo quando i + j é impar
                } else
                    matrizInversa[indiceLinhaAuxiliar][indiceColunaAuxiliar] = (double) matriz[i][j] / ExercicioDEZASSEIS.determinanteMatrizTeoremaLaPlace(matriz);

                indiceLinhaAuxiliar--;
            }
            indiceLinhaAuxiliar = matriz.length - 1;
            indiceColunaAuxiliar--;

        }

        return matrizInversa;
    }

    /**
     * Este método retorna a matriz inversa de uma matriz de ordem 3 ou superior
     * @param matriz matriz inicial
     * @return matriz inversa
     */
    public static double[][] matrizInversaOrdemTres(int[][] matriz) {
        double[][] matrizInversa = new double[matriz.length][matriz.length];
        double determinante;

        determinante = (double) ExercicioDEZASSEIS.determinanteMatrizTeoremaLaPlace(matriz);

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                if (ExercicioQUINZE.matrizAdjunta(matriz)[i][j] / determinante == -0.0) {
                    matrizInversa[i][j] = 0.0;
                } else
                    matrizInversa[i][j] = ExercicioQUINZE.matrizAdjunta(matriz)[i][j] / determinante;

            }
        }

        return matrizInversa;
    }

    /**
     * Este método calcula o menor complementar de uma matriz na posição i e j
     * @param matriz matriz inicial
     * @param i linha do número escolhido
     * @param j coluna do número escolhido
     * @return matriz de menor complementar
     */
    public static int[][] menorComplementar(int[][] matriz, int i, int j) {
        int[][] menorComplementar = new int[matriz.length - 1][matriz.length - 1];
        int linha = 0, coluna = 0;


        for (int k = 0; k < matriz.length; k++) {
            //não queremos incluir a linha i nem j, portanto se for esse o caso o programa continua sem acontecer nada
            if (k == i) continue;

            for (int l = 0; l < matriz.length; l++) {
                if (l == j) continue;
                menorComplementar[linha][coluna] = matriz[k][l];
                coluna++;

                if (coluna == matriz.length - 1) {
                    coluna = 0;
                    linha++;
                }
            }
        }

        return menorComplementar;
    }

    /**
     * Método para calcular a matriz adjunta
     * @param matriz inicial
     * @return matriz adjunta da matriz inicial
     */
    public static double[][] matrizAdjunta(int[][] matriz) {
        double[][] matrizAdjunta = new double[matriz.length][matriz.length];

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {

                double auxiliar = Math.pow(-1, i + j) * (double) ExercicioDEZASSEIS.determinanteMatrizTeoremaLaPlace(ExercicioQUINZE.menorComplementar(matriz, i, j));
                //temos que converter menor que 0.025 em 0.0
                if (Math.abs(auxiliar) <= 10e-6) {
                    auxiliar = 0;
                    matrizAdjunta[i][j] = auxiliar;
                }
            }
            matrizAdjunta = ExercicioQUINZE.matrizTransposta((matrizAdjunta));
        }

        return matrizAdjunta;
    }

    public static double [][] matrizTransposta (double[][] matriz){
        //se a matriz for null ou estiver vazia, não há elementos então retorna null
        if (matriz == null || matriz.length < 1) {
            return null;
        }

        //primeiro invertemos o tamanho das linhas e o das colunas
        double [][] matrizTransposta= new double [matriz[0].length][matriz.length];

        for (int linha=0; linha<matrizTransposta[0].length; linha++) {
            for (int coluna = 0; coluna<matrizTransposta.length; coluna++) {
                matrizTransposta[coluna][linha] = matriz[linha][coluna];
            }
        }
        return matrizTransposta;
    }
}