import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayBidimensionalTest {

    @Test
    void toMatriz() {
        ArrayBidimensional bidimensional= new ArrayBidimensional();
        int [][] esperado= {};

        assertArrayEquals(esperado, bidimensional.toMatriz());
    }

    @Test
    void comprimento() {
        int [][] array={{1,2,4},{1,3,4},{2,5,6}};
        ArrayBidimensional bidimensional= new ArrayBidimensional(array);
        int esperado=3;

        assertEquals(esperado, bidimensional.comprimento());
    }

    @Test
    void retiraPrimeiroNumeroMatriz() {
        int [][] array={{1,2,4},{1,3,4},{2,5,6}};
        ArrayBidimensional bidimensional= new ArrayBidimensional(array);
        int valor=2;
        int[][] esperado={{1,4},{1,3,4},{2,5,6}};
        bidimensional.retiraPrimeiroNumeroMatriz(valor);

        assertArrayEquals(esperado, bidimensional.toMatriz());
    }

    @Test
    void maiorElemento() {
        int [][] array={{1,2,4},{1,3,4},{2,5,6}};
        ArrayBidimensional bidimensional= new ArrayBidimensional(array);
        int esperado= 6;

        assertEquals(esperado, bidimensional.maiorElemento());
    }

    @Test
    void menorElemento() {
        int [][] array={{1,2,4},{1,3,4},{2,5,6}};
        ArrayBidimensional bidimensional= new ArrayBidimensional(array);
        int esperado= 1;

        assertEquals(esperado, bidimensional.menorElemento());
    }

    @Test
    void mediaElementos(){
        int[][] array={{1,2,4},{1,3,4},{2,5,6}};
        ArrayBidimensional bidimensional= new ArrayBidimensional(array);
        int esperado= 2;

        assertEquals(esperado, bidimensional.mediaDosElementos());
    }

    @Test
    void vetorComSomatorioDeCadaLinhaComNegativosTresLinhas() {
        int[][] matrizInicial={{1,2,9},{1,-2,4},{5,-6,29}};
        ArrayBidimensional bidimensonal=new ArrayBidimensional(matrizInicial);
        int [] esperado={12,3,28};
        Array resultado;
        resultado = bidimensonal.retornaVetorComOTotalDoSomatorioDeCadaLinha();

        assertArrayEquals(esperado, resultado.toArray());
    }

    @Test
    void vetorComSomatorioDeCadaLinhaComZerosQuatroLinhas() {
        int[][] matrizInicial={{1,2,9},{1,-2,4},{5,-6,29},{0,1,-1}};
        ArrayBidimensional bidimensonal=new ArrayBidimensional(matrizInicial);
        int [] esperado={12,3,28,0};
        Array resultado;
        resultado= bidimensonal.retornaVetorComOTotalDoSomatorioDeCadaLinha();

        assertArrayEquals(esperado, resultado.toArray());
    }

    @Test
    void indiceLinhaComMaiorSomaPenultimaLinha() {
        int[][] matrizInicial={{1,2,9},{1,-2,4},{5,-6,29},{0,1,-1}};
        ArrayBidimensional bidimensonal=new ArrayBidimensional(matrizInicial);
        int esperado= 2;

        assertEquals(esperado, bidimensonal.retornaIndiceMaiorElemento());
    }

    @Test
    void indiceLinhaComMaiorSomaPrimeira() {
        int[][] matrizInicial={{41,2,9},{31,-2,4},{5,-6,29},{0,1,-1}};
        ArrayBidimensional bidimensonal=new ArrayBidimensional(matrizInicial);
        int esperado=0;

        assertEquals(esperado, bidimensonal.retornaIndiceMaiorElemento());
    }

    @Test
    void matrizQuadrada(){
        int[][] matrizInicial={{1,2,3},{1,2,3},{1,2,3}};
        ArrayBidimensional bidimensonal=new ArrayBidimensional(matrizInicial);

        assertTrue(bidimensonal.eQuadrada());
    }

    @Test
    void matrizQuadradaNaoQuadrada(){
        int[][] matrizInicial={{1,2,3,4},{1,2,3},{1,2,3}};
        ArrayBidimensional bidimensonal=new ArrayBidimensional(matrizInicial);

        assertFalse(bidimensonal.eQuadrada());
    }

    @Test
    void matrizQuadradaNaoQuadradaDentada(){
        int[][] matrizInicial={{1,2,3},{1,2,3,4},{1,2}};
        ArrayBidimensional bidimensonal=new ArrayBidimensional(matrizInicial);

        assertFalse(bidimensonal.eQuadrada());
    }
}