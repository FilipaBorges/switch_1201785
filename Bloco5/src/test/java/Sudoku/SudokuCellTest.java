package Sudoku;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SudokuCellTest {

    @Test
    void constructorNotNull(){
        SudokuCell cell =new SudokuCell(4,true);

        assertNotNull(cell);
    }

    @Test
    void compareTheValue(){
        SudokuCell cell= new SudokuCell(4, true);
        int expected=4;
        int result= cell.getValue();

        assertEquals(expected,result);
    }

    @Test
    void addValueToNonEditableCell(){
        SudokuCell cell= new SudokuCell(2,true);

        assertThrows(IllegalArgumentException.class, () -> {cell.addValue(4);});
    }

    @Test
    void addNegativeValueToEditableCell(){
        assertThrows(IllegalArgumentException.class, () ->{SudokuCell cell= new SudokuCell(-2,false);});
    }

    @Test
    void addValueSuperiorToNineToEditableCell(){
        assertThrows(IllegalArgumentException.class, () ->{SudokuCell cell= new SudokuCell(10,false);});
    }

    @Test
    void addValueToEditableCell(){
        SudokuCell cell= new SudokuCell(0,false);
        int expected=2;
        cell.addValue(2);
        int result= cell.getValue();

        assertEquals(expected, result);
    }

    @Test
    void removeValueEditableCell(){
        SudokuCell cell= new SudokuCell(2, false);
        int expected=0;
        cell.removeValue();
        int result= cell.getValue();

        assertEquals(expected, result);
    }

    /*@Test
    void removeValueNonEditableCell(){
        SudokuCell cell= new SudokuCell(2,true);

        assertThrows(IllegalArgumentException.class, () -> {cell.removeValue();});
    }*/

    @Test
    void checkEmpty(){
        SudokuCell cell= new SudokuCell(0,false);

        assertTrue(cell.checkEmpty());
    }

    @Test
    void checkNotEmpty(){
        SudokuCell cell= new SudokuCell(1,false);

        assertFalse(cell.checkEmpty());
    }

}