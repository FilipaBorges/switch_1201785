import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MatrizTest {

    @Test
    void retornaMatrizVazia(){
        Matriz matriz= new Matriz();
        int [][] esperado={};

        assertArrayEquals(esperado, matriz.toMatriz());
    }

    @Test
    void inicializarMatrizComAlgunsValores(){
        int [][] matriz={{1,2},{3,4,5}};
        Matriz novaMatriz= new Matriz(matriz);
        int [][]esperado={{1,2},{3,4,5}};

        assertArrayEquals(esperado, novaMatriz.toMatriz());
    }

    @Test
    void adicionaValorNaPrimeiraLinha() {
        Matriz matriz= new Matriz();
        matriz.adicionarValorNumaLinha(3,0);
        int[][] esperado={{3}};

        assertArrayEquals(esperado, matriz.toMatriz());
    }

    @Test
    void adicionaValorNaLinha2() {
        Matriz matriz= new Matriz();
        matriz.adicionarValorNumaLinha(3,2);
        int[][] esperado={{},{},{3}};

        assertArrayEquals(esperado, matriz.toMatriz());
    }

    @Test
    void adicionarValorAMatrizComAlgunsValoresPreenchidosNumaLinhaExistente(){
        int [][] matriz={{1,2},{3,4,5}};
        Matriz novaMatriz= new Matriz(matriz);
        novaMatriz.adicionarValorNumaLinha(3,0);
        int [][]esperado={{1,2,3},{3,4,5}};

        assertArrayEquals(esperado, novaMatriz.toMatriz());
    }

    /*@Test
    void adicionarValorAMatrizComAlgunsValoresPreenchidosNumaNovaLinha(){
        int [][] matriz={{1,2,3},{3,4,5}};
        Matriz novaMatriz= new Matriz(matriz);
        novaMatriz.adicionarValorNumaLinha(3,2);
        int [][]esperado={{1,2,3},{3,4,5},{3}};

        assertArrayEquals(esperado, novaMatriz.toMatriz());
    }*/

    @Test
    void retirarPrimeiroElementoNaPrimeiraLinha() {
        int[][] matrizInicial= {{1,2,3},{4,5,6},{7,8,9}};
        Matriz matriz= new Matriz(matrizInicial);
        int valor=3;
        int [][] esperado= {{1,2},{4,5,6},{7,8,9}};
        matriz.retiraPrimeiroElementoComDeterminadoValor(valor);

        assertArrayEquals(esperado, matriz.toMatriz());
    }

    @Test
    void retirarPrimeiroElementoNaUltimaLinha() {
        int[][] matrizInicial= {{1,2,3},{4,5,6},{7,8,9}};
        Matriz matriz= new Matriz(matrizInicial);
        int valor=9;
        int [][] esperado= {{1,2,3},{4,5,6},{7,8}};
        matriz.retiraPrimeiroElementoComDeterminadoValor(valor);

        assertArrayEquals(esperado, matriz.toMatriz());
    }

    @Test
    void retirarPrimeiroElementoNaoExistente() {
        int[][] matrizInicial= {{1,2,3},{4,5,6},{7,8,9}};
        Matriz matriz= new Matriz(matrizInicial);
        int valor=10;
        int [][] esperado= {{1,2,3},{4,5,6},{7,8,9}};
        matriz.retiraPrimeiroElementoComDeterminadoValor(valor);

        assertArrayEquals(esperado, matriz.toMatriz());
    }

    @Test
    void matrizVazia(){
        Matriz matriz= new Matriz();

        assertTrue(matriz.estaVaziaMatriz());
    }

    @Test
    void matrizNaoVazia(){
        int[][] matrizInicial={{1}};
        Matriz matriz= new Matriz(matrizInicial);

        assertFalse(matriz.estaVaziaMatriz());
    }

    @Test
    void maiorElementoDaMatrizPrimeiraLinha() {
        int[][] matrizInicial={{1,2,9},{1,2,4},{5,6,7}};
        Matriz matriz=new Matriz(matrizInicial);
        int esperado=9;

        assertEquals(esperado, matriz.maiorElementoDaMatriz());
    }

    @Test
    void maiorElementoDaMatrizUltimoValor() {
        int[][] matrizInicial={{1,2,9},{1,2,4},{5,6,29}};
        Matriz matriz=new Matriz(matrizInicial);
        int esperado=29;

        assertEquals(esperado, matriz.maiorElementoDaMatriz());
    }

    @Test
    void maiorElementoDaMatrizComNegativos() {
        int[][] matrizInicial={{1,2,9},{1,2,4},{5,6,29},{-2,4,0}};
        Matriz matriz=new Matriz(matrizInicial);
        int esperado=29;

        assertEquals(esperado, matriz.maiorElementoDaMatriz());
    }

    @Test
    void menorElementoDaMatrizComNegativos() {
        int[][] matrizInicial={{1,2,9},{1,2,4},{5,6,29},{-2,4,0}};
        Matriz matriz=new Matriz(matrizInicial);
        int esperado=-2;

        assertEquals(esperado, matriz.menorElementoDaMatriz());
    }

    @Test
    void menorElementoDaMatrizComNegativosUltimaLinha() {
        int[][] matrizInicial={{1,2,9},{1,2,4},{5,6,29},{-2,4,-30}};
        Matriz matriz=new Matriz(matrizInicial);
        int esperado=-30;

        assertEquals(esperado, matriz.menorElementoDaMatriz());
    }

    @Test
    void mediaElementosMatriz() {
        int[][] matrizInicial={{1,2,9},{1,2,4},{5,6,29}};
        Matriz matriz=new Matriz(matrizInicial);
        double esperado=6.5555;

        assertEquals(esperado, matriz.mediaElementosDaMatriz(), 0.0001);
    }

    @Test
    void mediaElementosMatrizComNegativos() {
        int[][] matrizInicial={{1,2,9},{1,-2,4},{5,-6,29}};
        Matriz matriz=new Matriz(matrizInicial);
        double esperado=4.7777;

        assertEquals(esperado, matriz.mediaElementosDaMatriz(), 0.0001);
    }

    @Test
    void vetorComSomatorioDeCadaLinhaComNegativosTresLinhas() {
        int[][] matrizInicial={{1,2,9},{1,-2,4},{5,-6,29}};
        Matriz matriz=new Matriz(matrizInicial);
        int [] esperado={12,3,28};

        assertArrayEquals(esperado, matriz.retornaVetorComOTotalDoSomatorioDeCadaLinha());
    }

    @Test
    void vetorComSomatorioDeCadaLinhaComZerosQuatroLinhas() {
        int[][] matrizInicial={{1,2,9},{1,-2,4},{5,-6,29},{0,1,-1}};
        Matriz matriz=new Matriz(matrizInicial);
        int [] esperado={12,3,28,0};

        assertArrayEquals(esperado, matriz.retornaVetorComOTotalDoSomatorioDeCadaLinha());
    }

    @Test
    void vetorComSomatorioDeCadaColunaComZerosENegativosTresColunas() {
        int[][] matrizInicial={{1,2,9},{1,-2,4},{5,-6,29},{0,1,-1}};
        Matriz matriz=new Matriz(matrizInicial);
        int [] esperado={7,-5,41};

        assertArrayEquals(esperado, matriz.retornaVetorComOTotalDoSomatorioDeCadaColuna());
    }

    @Test
    void vetorComSomatorioDeCadaColunaComZerosENegativosQuatroColunas() {
        int[][] matrizInicial={{1,-2,4,0},{5,-6,29,1},{0,1,-1,0}};
        Matriz matriz=new Matriz(matrizInicial);
        int [] esperado={6,-7,32,1};

        assertArrayEquals(esperado, matriz.retornaVetorComOTotalDoSomatorioDeCadaColuna());
    }

    @Test
    void indiceLinhaComMaiorSomaPenultimaLinha() {
        int[][] matrizInicial={{1,2,9},{1,-2,4},{5,-6,29},{0,1,-1}};
        Matriz matriz=new Matriz(matrizInicial);
        int esperado= 2;

        assertEquals(esperado,matriz.indiceComAMaiorSoma());
    }

    @Test
    void indiceLinhaComMaiorSomaPrimeira() {
        int[][] matrizInicial={{41,2,9},{31,-2,4},{5,-6,29},{0,1,-1}};
        Matriz matriz=new Matriz(matrizInicial);
        int esperado=0;

        assertEquals(esperado,matriz.indiceComAMaiorSoma());
    }

    @Test
    void matrizQuadrada(){
        int[][] matrizInicial={{1,2,3},{1,2,3},{1,2,3}};
        Matriz matriz= new Matriz(matrizInicial);

        assertTrue(matriz.verificaMatrizQuadrada());
    }

    @Test
    void matrizQuadradaNaoQuadrada(){
        int[][] matrizInicial={{1,2,3,4},{1,2,3},{1,2,3}};
        Matriz matriz= new Matriz(matrizInicial);

        assertFalse(matriz.verificaMatrizQuadrada());
    }

    @Test
    void matrizQuadradaNaoSimetrica(){
        int[][] matrizInicial={{1,2,4},{2,2,7},{9,2,3}};
        Matriz matriz= new Matriz(matrizInicial);

        assertFalse(matriz.verificaQuadradaSimetrica());
    }

    @Test
    void matrizQuadradaSimetrica(){
        int[][] matrizInicial={{1,2,3}, {2,1,3},{3,3,1}};
        Matriz matriz= new Matriz(matrizInicial);

        assertTrue(matriz.verificaQuadradaSimetrica());
    }

    @Test
    void diagonalPrincipalSemValoresNulos(){
        int[][] matrizInicial={{1,2,3}, {2,1,3},{3,3,1}};
        Matriz matriz= new Matriz(matrizInicial);
        int esperado= 3;

        assertEquals(esperado, matriz.quantidadeDeElementosNaoNulosNaDiagonalPrincipal());
    }

    @Test
    void diagonalPrincipalComValoresNulos(){
        int[][] matrizInicial={{1,2,3}, {2,0,3},{3,3,1}};
        Matriz matriz= new Matriz(matrizInicial);
        int esperado= 2;

        assertEquals(esperado, matriz.quantidadeDeElementosNaoNulosNaDiagonalPrincipal());
    }

    @Test
    void diagonalPrincipalMatrizNaoQuadrada(){
        int[][] matrizInicial={{1,2,3}, {2,0,3},{3,3,1},{1,2,3}};
        Matriz matriz= new Matriz(matrizInicial);
        int esperado= -1;

        assertEquals(esperado, matriz.quantidadeDeElementosNaoNulosNaDiagonalPrincipal());
    }

    @Test
    void diagonalPrincipalIgualDiagonalSecundaria(){
        int[][] matrizInicial={{1,2,1}, {1,2,1},{1,2,1}};
        Matriz matriz= new Matriz(matrizInicial);

        assertTrue(matriz.verificaIgualdadeDasDiagonais());
    }

    @Test
    void diagonalPrincipalDiferentesElementosDiagonalSecundaria(){
        int[][] matrizInicial={{1,2,3}, {1,2,1},{1,2,1}};
        Matriz matriz= new Matriz(matrizInicial);

        assertFalse(matriz.verificaIgualdadeDasDiagonais());
    }

    @Test
    void retornaVetorDeElementosComMaisDeTresNumeros() {
        int [][] matrizInicial={{123,4567,2},{432,3,87654321},{3456,23,45}};
        Matriz matriz= new Matriz(matrizInicial);
        int [] esperado= {4567,87654321,3456};

        assertArrayEquals(esperado, matriz.retornaVetorComNumeroDeAlgarismosSuperiorAMediaDeAlgarismosDaMatriz());
    }

    @Test
    void retornaVetorDeElementosComMaisDeTresNumerosComNegativos() {
        int [][] matrizInicial={{123,-4567,2},{432,-3,87654321},{3456,23,45}};
        Matriz matriz= new Matriz(matrizInicial);
        int [] esperado= {-4567,87654321,3456};

        assertArrayEquals(esperado, matriz.retornaVetorComNumeroDeAlgarismosSuperiorAMediaDeAlgarismosDaMatriz());
    }

    @Test
    void percentagemParesSuperiorACinquenta(){
        int[][] matrizInicial={{123,-4567,2},{432,-3,87654321},{3456,23,45}};
        Matriz matriz= new Matriz(matrizInicial);
        int [] esperado={2,432};

        assertArrayEquals(esperado, matriz.retornaVetorComNumeroComPercentagemDeAlgarismosParesSuperioresAMedia());
    }

    @Test
    void percentagemParesSoPares(){
        int [][] matrizInicial={{222,4,68},{446,88,2},{468,822,8}};
        Matriz matriz= new Matriz(matrizInicial);
        int [] esperado= {};

        assertArrayEquals(esperado, matriz.retornaVetorComNumeroComPercentagemDeAlgarismosParesSuperioresAMedia());
    }

    @Test
    void percentagemParesSoImpares(){
        int [][] matrizInicial={{11,3,579},{7,137},{731,5,9}};
        Matriz matriz= new Matriz(matrizInicial);
        int [] esperado= {};

        assertArrayEquals(esperado, matriz.retornaVetorComNumeroComPercentagemDeAlgarismosParesSuperioresAMedia());
    }

    @Test
    void matrizComAsLinhasInvertidas(){
        int [][] matrizInicial={{11,3,579},{7,137},{731,5,9}};
        Matriz matriz= new Matriz(matrizInicial);
        int [][] esperado= {{579,3,11},{137,7},{9,5,731}};
        matriz.inverteLinhasDaMatriz();

        assertArrayEquals(esperado, matriz.toMatriz());
    }

    @Test
    void matrizComAsLinhasInvertidasComNegativos(){
        int [][] matrizInicial={{11,3,579},{-456,7,137},{731,5,9},{-20,4}};
        Matriz matriz= new Matriz(matrizInicial);
        int [][] esperado= {{579,3,11},{137,7,-456},{9,5,731},{4,-20}};
        matriz.inverteLinhasDaMatriz();

        assertArrayEquals(esperado, matriz.toMatriz());
    }

    @Test
    void matrizComAsColunasInvertidas(){
        int [][] matrizInicial={{11,3,579},{7,137,14},{731,5,9}};
        Matriz matriz= new Matriz(matrizInicial);
        int [][] esperado= {{731,5,9},{7,137,14},{11,3,579}};
        matriz.inverteColunasMatriz();

        assertArrayEquals(esperado, matriz.toMatriz());
    }

    @Test
    void matrizComAsColunasInvertidasComNegativos(){
        int [][] matrizInicial={{11,-3,579},{7,137,14},{-731,5,9}};
        Matriz matriz= new Matriz(matrizInicial);
        int [][] esperado= {{-731,5,9},{7,137,14},{11,-3,579}};
        matriz.inverteColunasMatriz();

        assertArrayEquals(esperado, matriz.toMatriz());
    }

    @Test
    void rodarMatrizNoventaGraus(){
        int [][] matrizInicial={{11,3,579},{7,137,14},{731,5,9}};
        Matriz matriz= new Matriz(matrizInicial);
        int [][] esperado= {{731,7,11},{5,137,3},{9,14,579}};
        matriz.rodarAMatrizNoventaGraus();

        assertArrayEquals(esperado, matriz.toMatriz());
    }

    @Test
    void rodarMatrizNoventaGrausMatrizNaoQuadrada(){
        int [][] matrizInicial={{11,3,579},{7,137,14},{731,5,9},{1,2,3}};
        Matriz matriz= new Matriz(matrizInicial);
        int [][] esperado= {{11,3,579},{7,137,14},{731,5,9},{1,2,3}};
        matriz.rodarAMatrizNoventaGraus();

        assertArrayEquals(esperado, matriz.toMatriz());
    }

    @Test
    void rodarMatrizCentoEOitenta(){
        int [][] matrizInicial={{11,3,579},{7,137,14},{731,5,9}};
        Matriz matriz= new Matriz(matrizInicial);
        int [][] esperado= {{9,5,731},{14,137,7},{579,3,11}};
        matriz.rodarAMatrizCentoEOitentaGraus();

        assertArrayEquals(esperado, matriz.toMatriz());
    }

    @Test
    void rodarMatrizMenosNoventaGraus(){
        int [][] matrizInicial={{11,3,579},{7,137,14},{731,5,9}};
        Matriz matriz= new Matriz(matrizInicial);
        int [][] esperado= {{579,14,9},{3,137,5},{11,7,731}};
        matriz.rodarAMatrizMenosNoventaGraus();

        assertArrayEquals(esperado, matriz.toMatriz());
    }
}