import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayTest {

    @Test
    void retornaVetorVazio(){
        Array array = new Array();
        int [] esperado={};

        int[] resultado= array.toArray();
        assertArrayEquals(esperado, resultado);
    }

    /*@Test
    void arrayNull(){
        Array array= new Array();
        array=null;

        int[] resultado= array.toArray();
        assertNull(resultado);
    }*/

    @Test
    void adicionarUmValor() {
        Array array=new Array();
        int [] esperado= {1};
        array.adicionarValor(1);

        assertArrayEquals(esperado, array.toArray());
    }

    @Test
    void adicionarCincoValores() {
        Array array=new Array();
        int [] esperado= {1,2,3,4,5};
        array.adicionarValor(1);
        array.adicionarValor(2);
        array.adicionarValor(3);
        array.adicionarValor(4);
        array.adicionarValor(5);

        assertArrayEquals(esperado, array.toArray());
    }

    @Test
    void adicionarValoresNegativosEZeros() {
        Array array=new Array();
        int [] esperado= {1,-2,0,4,5};
        array.adicionarValor(1);
        array.adicionarValor(-2);
        array.adicionarValor(0);
        array.adicionarValor(4);
        array.adicionarValor(5);

        assertArrayEquals(esperado, array.toArray());
    }

    @Test
    void retirarValorArrayVazio() {
        Array array= new Array();
        int valor=4;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                array.retirarPrimeiroElementoComDeterminadoValor(valor));

        String esperada= "O array não pode estar vazio";
        String resultado= exception.getMessage();
        assertEquals(esperada, resultado);
    }

    @Test
    void retirarValorNaoExistente() {
        Array array= new Array();
        array.adicionarValor(0);
        array.adicionarValor(1);
        array.adicionarValor(3);
        int valor=4;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                array.retirarPrimeiroElementoComDeterminadoValor(valor));

        String esperada= "O array não contém esse valor";
        String resultado= exception.getMessage();
        assertEquals(esperada, resultado);
    }

    @Test
    void retirarPrimeiroSeis() {
        Array array=new Array();
        int valor=6;
        int [] esperado= {0,4,6,6};
        array.adicionarValor(0);
        array.adicionarValor(6);
        array.adicionarValor(4);
        array.adicionarValor(6);
        array.adicionarValor(6);
        array.retirarPrimeiroElementoComDeterminadoValor(valor);

        assertArrayEquals(esperado, array.toArray());
    }

    @Test
    void retirarPrimeiroValorNegativo() {
        Array array=new Array();
        int valor=-2;
        int [] esperado= {0,-4,-5,6};
        array.adicionarValor(0);
        array.adicionarValor(-4);
        array.adicionarValor(-2);
        array.adicionarValor(-5);
        array.adicionarValor(6);
        array.retirarPrimeiroElementoComDeterminadoValor(valor);

        assertArrayEquals(esperado, array.toArray());
    }

    @Test
    void retornaValorPeloIndice() {
        Array array=new Array();
        int indice=3;
        int esperado=4;
        array.adicionarValor(0);
        array.adicionarValor(-1);
        array.adicionarValor(0);
        array.adicionarValor(4);
        array.adicionarValor(0);

        int resultado= array.retornaValorPeloIndice(indice);
        assertEquals(esperado, resultado);
    }

    @Test
    void retornaValorPeloIndiceZero() {
        Array array=new Array();
        int indice=0;
        int esperado=0;
        array.adicionarValor(0);
        array.adicionarValor(-1);
        array.adicionarValor(0);
        array.adicionarValor(4);
        array.adicionarValor(0);

        int resultado= array.retornaValorPeloIndice(indice);
        assertEquals(esperado, resultado);
    }

    @Test
    void retornaValorPeloUltimoIndice() {
        Array array=new Array();
        int indice=4;
        int esperado=-2;
        array.adicionarValor(0);
        array.adicionarValor(-1);
        array.adicionarValor(0);
        array.adicionarValor(4);
        array.adicionarValor(-2);

        int resultado= array.retornaValorPeloIndice(indice);
        assertEquals(esperado, resultado);
    }

    @Test
    void retornaValorIndiceMaiorQueArray() {
        Array array=new Array();
        int indice=7;
        array.adicionarValor(0);
        array.adicionarValor(-1);
        array.adicionarValor(0);
        array.adicionarValor(4);
        array.adicionarValor(0);

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                array.retornaValorPeloIndice(indice));

        String esperada= "O array não tem tamanho para ter esse índice";
        String resultado= exception.getMessage();
        assertEquals(esperada, resultado);
    }

    @Test
    void retornaMaiorElemento() {
        Array array=new Array();
        int esperado=4;
        array.adicionarValor(0);
        array.adicionarValor(-1);
        array.adicionarValor(0);
        array.adicionarValor(4);
        array.adicionarValor(0);

       int resultado= array.maiorElemento();

       assertEquals(esperado, resultado);
    }

    @Test
    void retornaMenorElemento() {
        Array array=new Array();
        int esperado=-1;
        array.adicionarValor(0);
        array.adicionarValor(-1);
        array.adicionarValor(0);
        array.adicionarValor(4);
        array.adicionarValor(0);

        int resultado= array.menorElemento();

        assertEquals(esperado, resultado);
    }

    @Test
    void mediaElementos() {
        Array array=new Array();
        double esperado=2.4;
        array.adicionarValor(1);
        array.adicionarValor(2);
        array.adicionarValor(0);
        array.adicionarValor(4);
        array.adicionarValor(5);

        double resultado= array.mediaElementos();

        assertEquals(esperado, resultado);
    }

    @Test
    void mediaElementosComNegativos() {
        Array array=new Array();
        double esperado=-2.4;
        array.adicionarValor(1);
        array.adicionarValor(-2);
        array.adicionarValor(-20);
        array.adicionarValor(4);
        array.adicionarValor(5);

        double resultado= array.mediaElementos();

        assertEquals(esperado, resultado);
    }

    @Test
    void mediaElementosPares() {
        Array array=new Array();
        double esperado=10;
        array.adicionarValor(1);
        array.adicionarValor(2);
        array.adicionarValor(24);
        array.adicionarValor(4);
        array.adicionarValor(5);

        double resultado= array.mediaElementosPares();

        assertEquals(esperado, resultado);
    }

    @Test
    void mediaElementosParesComNegativos() {
        Array array=new Array();
        double esperado=6;
        array.adicionarValor(1);
        array.adicionarValor(-2);
        array.adicionarValor(24);
        array.adicionarValor(-4);
        array.adicionarValor(5);

        double resultado= array.mediaElementosPares();

        assertEquals(esperado, resultado);
    }

    @Test
    void mediaElementosParesSoPares() {
        Array array=new Array();
        double esperado=9.6;
        array.adicionarValor(10);
        array.adicionarValor(2);
        array.adicionarValor(24);
        array.adicionarValor(4);
        array.adicionarValor(8);

        double resultado= array.mediaElementosPares();
        double resultado1= array.mediaElementos();

        assertEquals(esperado, resultado);
        assertEquals(resultado, resultado1);
    }

    @Test
    void mediaElementosParesSemElementosPares() {
        Array array=new Array();
        double esperado=0;
        array.adicionarValor(1);
        array.adicionarValor(5);
        array.adicionarValor(-3);
        array.adicionarValor(-1);
        array.adicionarValor(5);

        double resultado= array.mediaElementosPares();

        assertEquals(esperado, resultado);
    }

    @Test
    void mediaElementosImpares() {
        Array array=new Array();
        double esperado=7;
        array.adicionarValor(1);
        array.adicionarValor(2);
        array.adicionarValor(15);
        array.adicionarValor(4);
        array.adicionarValor(5);

        double resultado= array.mediaElementosImpares();

        assertEquals(esperado, resultado);
    }

    @Test
    void mediaElementosImparesComNegativos() {
        Array array=new Array();
        double esperado=3.5;
        array.adicionarValor(1);
        array.adicionarValor(-7);
        array.adicionarValor(15);
        array.adicionarValor(4);
        array.adicionarValor(5);

        double resultado= array.mediaElementosImpares();

        assertEquals(esperado, resultado);
    }

    @Test
    void mediaElementosImparesSoImpares() {
        Array array=new Array();
        double esperado=-1.4;
        array.adicionarValor(1);
        array.adicionarValor(-7);
        array.adicionarValor(15);
        array.adicionarValor(-21);
        array.adicionarValor(5);

        double resultado= array.mediaElementosImpares();
        double resultado1= array.mediaElementosImpares();;

        assertEquals(esperado, resultado);
        assertEquals(resultado, resultado1);
    }

    @Test
    void mediaElementosImparesSemImpares() {
        Array array=new Array();
        double esperado=0;
        array.adicionarValor(4);
        array.adicionarValor(-8);
        array.adicionarValor(16);
        array.adicionarValor(4);
        array.adicionarValor(50);

        double resultado= array.mediaElementosImpares();

        assertEquals(esperado, resultado);
    }

    @Test
    void mediaMultiplosDeTres() {
        Array array = new Array();
        array.adicionarValor(4);
        array.adicionarValor(6);
        array.adicionarValor(9);
        double esperado = 7.5;
        int multiplo = 3;
        assertEquals(esperado, array.mediaMultiplosDeUmNumero(multiplo), 0.01);
    }

    @Test
    void averageOfMultiples_noMultiples() {
        Array array = new Array();
        array.adicionarValor(3);
        array.adicionarValor(6);
        array.adicionarValor(9);
        double esperado = 0;
        int multiplo = 4;
        assertEquals(esperado, array.mediaMultiplosDeUmNumero(multiplo), 0.01);
    }


    @Test
    void ordenaVetorAscendente() {
        Array array=new Array();
        int [] esperado= {-21,-7,1,5,15};
        array.adicionarValor(1);
        array.adicionarValor(-7);
        array.adicionarValor(15);
        array.adicionarValor(-21);
        array.adicionarValor(5);

        array.ordenaAscendente();

        assertArrayEquals(esperado, array.toArray());
    }

    @Test
    void ordenaVetorAscendenteJaOrdenado() {
        Array array=new Array();
        int [] esperado= {-21,-7,1,5,15};
        array.adicionarValor(-21);
        array.adicionarValor(-7);
        array.adicionarValor(1);
        array.adicionarValor(5);
        array.adicionarValor(15);

        array.ordenaAscendente();

        assertArrayEquals(esperado, array.toArray());
    }

    @Test
    void ordenaVetorAscendenteInvertido() {
        Array array=new Array();
        int [] esperado= {-21,-7,1,5,15};
        array.adicionarValor(15);
        array.adicionarValor(5);
        array.adicionarValor(1);
        array.adicionarValor(-7);
        array.adicionarValor(-21);

        array.ordenaAscendente();

        assertArrayEquals(esperado, array.toArray());
    }

    @Test
    void ordenaVetorDescendente() {
        Array array=new Array();
        int [] esperado= {7,3,-4,-6,-21};
        array.adicionarValor(-4);
        array.adicionarValor(3);
        array.adicionarValor(7);
        array.adicionarValor(-6);
        array.adicionarValor(-21);

        array.ordenaDescendente();

        assertArrayEquals(esperado, array.toArray());
    }

    @Test
    void ordenaVetorDescendenteNormal() {
        Array array=new Array();
        int [] esperado= {10,7,5,-6,-21};
        array.adicionarValor(10);
        array.adicionarValor(7);
        array.adicionarValor(5);
        array.adicionarValor(-6);
        array.adicionarValor(-21);

        array.ordenaDescendente();

        assertArrayEquals(esperado, array.toArray());
    }
    @Test
    void ordenaVetorDescendenteTotalmenteInvertido() {
        Array array=new Array();
        int [] esperado= {10,7,5,-6,-21};
        array.adicionarValor(-21);
        array.adicionarValor(-6);
        array.adicionarValor(5);
        array.adicionarValor(7);
        array.adicionarValor(10);

        array.ordenaDescendente();

        assertArrayEquals(esperado, array.toArray());
    }

    @Test
    void verificaTodosElementosPares() {
        Array array=new Array();
        array.adicionarValor(10);
        array.adicionarValor(2);
        array.adicionarValor(24);
        array.adicionarValor(4);
        array.adicionarValor(8);

        boolean resultado= array.todosElementosPares();

        assertTrue(resultado);
    }

    @Test
    void verificaTodosElementosParesComUmImparNoFim() {
        Array array=new Array();
        array.adicionarValor(10);
        array.adicionarValor(2);
        array.adicionarValor(24);
        array.adicionarValor(4);
        array.adicionarValor(9);

        boolean resultado= array.todosElementosPares();

        assertFalse(resultado);
    }

    @Test
    void verificaTodosElementosParesComMaisQueUmImpar() {
        Array array=new Array();
        array.adicionarValor(11);
        array.adicionarValor(2);
        array.adicionarValor(24);
        array.adicionarValor(4);
        array.adicionarValor(9);

        boolean resultado= array.todosElementosPares();

        assertFalse(resultado);
    }

    @Test
    void verificaTodosElementosImpares() {
        Array array=new Array();
        array.adicionarValor(11);
        array.adicionarValor(23);
        array.adicionarValor(25);
        array.adicionarValor(41);
        array.adicionarValor(9);

        boolean resultado= array.todosElementosImpares();

        assertTrue(resultado);
    }

    @Test
    void verificaTodosElementosImparesComUmPar() {
        Array array=new Array();
        array.adicionarValor(11);
        array.adicionarValor(23);
        array.adicionarValor(25);
        array.adicionarValor(41);
        array.adicionarValor(8);

        boolean resultado= array.todosElementosImpares();

        assertFalse(resultado);
    }

    @Test
    void verificaTodosElementosImparesComNegativo() {
        Array array=new Array();
        array.adicionarValor(11);
        array.adicionarValor(23);
        array.adicionarValor(-25);
        array.adicionarValor(41);
        array.adicionarValor(9);

        boolean resultado= array.todosElementosImpares();

        assertTrue(resultado);
    }

    @Test
    void verificaDuplicadoComDuplicado() {
        Array array=new Array();
        array.adicionarValor(11);
        array.adicionarValor(23);
        array.adicionarValor(-25);
        array.adicionarValor(11);
        array.adicionarValor(9);

        boolean resultado= array.verificaDuplicados();

        assertTrue(resultado);
    }

    @Test
    void verificaDuplicadoSemDuplicado() {
        Array array=new Array();
        array.adicionarValor(11);
        array.adicionarValor(23);
        array.adicionarValor(-25);
        array.adicionarValor(12);
        array.adicionarValor(9);

        boolean resultado= array.verificaDuplicados();

        assertFalse(resultado);
    }

    @Test
    void numeroMedioAlgarismos() {
        Array array=new Array();
        int [] esperado= {11345,255};
        array.adicionarValor(11345);
        array.adicionarValor(23);
        array.adicionarValor(255);
        array.adicionarValor(12);
        array.adicionarValor(9);

        int [] resultado= array.elementosComNumeroDeAlgarismosSuperiorAoDaMedia();

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void numeroMedioAlgarismosComNegativos() {
        Array array=new Array();
        int [] esperado= {11345,-255};
        array.adicionarValor(11345);
        array.adicionarValor(23);
        array.adicionarValor(-255);
        array.adicionarValor(12);
        array.adicionarValor(9);

        int [] resultado= array.elementosComNumeroDeAlgarismosSuperiorAoDaMedia();

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void numeroMedioAlgarismosComVariosNegativos() {
        Array array=new Array();
        int [] esperado= {11345,90000};
        array.adicionarValor(11345);
        array.adicionarValor(23);
        array.adicionarValor(-255);
        array.adicionarValor(-124);
        array.adicionarValor(90000);

        int [] resultado= array.elementosComNumeroDeAlgarismosSuperiorAoDaMedia();

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void percentagemDeParesSuperiorAPercentagemDeParesTotal() {
        Array array=new Array();
        int [] esperado= {-124,90000};
        array.adicionarValor(11345);
        array.adicionarValor(23);
        array.adicionarValor(-255);
        array.adicionarValor(-124);
        array.adicionarValor(90000);

        int [] resultado= array.elementosComPercentagemDeAlgarismosParesSuperiorAMedia();

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void numerosSoComElementosPares() {
        Array array=new Array();
        int [] esperado= {22,-424};
        array.adicionarValor(11345);
        array.adicionarValor(22);
        array.adicionarValor(-255);
        array.adicionarValor(-424);
        array.adicionarValor(90000);

        int [] resultado= array.retornaElementosSoDePares();

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void numerosSoComElementosParesTodosPares() {
        Array array=new Array();
        int [] esperado= {2468,22,-288,-424,60000};
        array.adicionarValor(2468);
        array.adicionarValor(22);
        array.adicionarValor(-288);
        array.adicionarValor(-424);
        array.adicionarValor(60000);

        int [] resultado= array.retornaElementosSoDePares();

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void numerosSoComElementosParesNenhumPar() {
        Array array=new Array();
        int [] esperado= {};
        array.adicionarValor(9357);
        array.adicionarValor(11);
        array.adicionarValor(-371);
        array.adicionarValor(-977);
        array.adicionarValor(9111);

        int [] resultado= array.retornaElementosSoDePares();

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void numerosComElementosCrescentes() {
        Array array=new Array();
        int[] esperado={123,456};
        array.adicionarValor(123);
        array.adicionarValor(321);
        array.adicionarValor(456);

        int[] resultado= array.retornaNumeroComElementosCrescentes();

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void numerosComElementosCrescentesSemElementosCrescentes() {
        Array array=new Array();
        int[] esperado={};
        array.adicionarValor(-572);
        array.adicionarValor(321);
        array.adicionarValor(461);

        int[] resultado= array.retornaNumeroComElementosCrescentes();

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void vetorComCapicua() {
        Array array= new Array();
        int[] esperado={121};
        array.adicionarValor(-572);
        array.adicionarValor(121);
        array.adicionarValor(461);

        int[] resultado= array.retornaCapicua();

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void vetorComCapicuaECapicuaNegativo() {
        Array array= new Array();
        int[] esperado={121,-222};
        array.adicionarValor(-572);
        array.adicionarValor(121);
        array.adicionarValor(461);
        array.adicionarValor(-222);

        int[] resultado= array.retornaCapicua();

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void vetorNumerosComElementosIguais() {
        Array array= new Array();
        int[] esperado={222};
        array.adicionarValor(-572);
        array.adicionarValor(121);
        array.adicionarValor(461);
        array.adicionarValor(222);

        int[] resultado= array.retornaElementosCompostosPeloMesmoNumero();

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void vetorNumerosComElementosIguaisSemElementosIguais() {
        Array array= new Array();
        int[] esperado={};
        array.adicionarValor(-572);
        array.adicionarValor(121);
        array.adicionarValor(461);
        array.adicionarValor(232);

        int[] resultado= array.retornaElementosCompostosPeloMesmoNumero();

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void vetorNumerosComElementosIguaisComIguaisNegativos() {
        Array array= new Array();
        int[] esperado={-555,222};
        array.adicionarValor(-555);
        array.adicionarValor(121);
        array.adicionarValor(461);
        array.adicionarValor(222);

        int[] resultado= array.retornaElementosCompostosPeloMesmoNumero();

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void vetorNumerosNaoAmstrong() {
        Array array= new Array();
        int[] esperado={244};
        array.adicionarValor(153);
        array.adicionarValor(244);

        int[] resultado= array.retornaNumerosNaoAmstrong();

        assertArrayEquals(esperado, resultado);
    }

    @Test
    void numeroComSequenciaCrescenteMaiorQue3() {
        Array array= new Array();
        int[] esperado={123456};
        int numero=3;
        array.adicionarValor(153);
        array.adicionarValor(244);
        array.adicionarValor(123456);
        array.adicionarValor(34);
        array.adicionarValor(324567);

        assertArrayEquals(esperado, array.retornaNumerosAscendentesMaioresQueN(3));
    }

    @Test
    void numeroComSequenciaCrescenteMaiorQue2() {
        Array array= new Array();
        int[] esperado={157, 123456};
        int numero=2;
        array.adicionarValor(157);
        array.adicionarValor(272);
        array.adicionarValor(123456);
        array.adicionarValor(34);
        array.adicionarValor(324567);

        assertArrayEquals(esperado, array.retornaNumerosAscendentesMaioresQueN(2));
    }

    @Test
    void eIGualVerdade() {
        Array array = new Array(123);
        array.adicionarValor(253);
        array.adicionarValor(123221);
        array.adicionarValor(379);
        Array array1 = new Array(123);
        array1.adicionarValor(253);
        array1.adicionarValor(123221);
        array1.adicionarValor(379);
        assertTrue(array.verificaIgual(array1));
    }

    @Test
    void eIGualFalsoComprimentosDiferentes() {
        Array array = new Array(123);
        array.adicionarValor(253);
        array.adicionarValor(123221);
        array.adicionarValor(379);
        Array array1 = new Array();
        array1.adicionarValor(253);
        array1.adicionarValor(123221);
        array1.adicionarValor(379);
        assertFalse(array.verificaIgual(array1));
    }

    @Test
    void eIGualFalsoComprimentosIguais() {
        Array array = new Array(123);
        array.adicionarValor(253);
        array.adicionarValor(123221);
        array.adicionarValor(379);
        Array array1 = new Array(345);
        array1.adicionarValor(253);
        array1.adicionarValor(123221);
        array1.adicionarValor(379);
        assertFalse(array.verificaIgual(array1));
    }

}