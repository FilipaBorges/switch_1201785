import BlocosAnterior.ExercicioQUINZE;
import BlocosAnterior.ExercicioTREZE;

public class Matriz {
    //atributos
    private int[][] matriz = new int[0][0];

    //Construtor
    public Matriz() {
    }

    public Matriz(int[][] matriz) {
        this.matriz = matriz;
    }


    public int[][] toMatriz() {
        if(this.matriz==null)
            return null;
        int[][] novaMatriz = new int[this.matriz.length][];
        for (int i = 0; i < this.matriz.length; i++) {
            novaMatriz[i] = new int[this.matriz[i].length];
            for (int j = 0; j < this.matriz[i].length; j++) {
                novaMatriz[i][j] = this.matriz[i][j];
            }
        }
        return novaMatriz;
    }

    private void adicionarNovaLinha(int novaLinha) {
        if ( this.matriz.length == 0 ) {
            this.matriz = new int[novaLinha + 1][0];
        } else
            if ( this.matriz.length < novaLinha + 1 ) {
            int[][] novaMatriz = new int[novaLinha + 1][0];
            for (int i = 0; i < this.matriz.length; i++) {
                //https://www.tutorialspoint.com/java/lang/system_arraycopy.htm
                System.arraycopy(this.matriz[i], 0, novaMatriz[i], 0, this.matriz[i].length);
            }
            this.matriz = novaMatriz;
        }
    }

    /*private int[] adicionaValorNumaLinha(int valor, int linha){
        //Primeiro vou copiar a linha onde quero adicionar para um array
        Array array=new Array();
        for (int i = 0; i <= matriz.length; i++) {
            array.adicionarValor(this.matriz[linha][i]);
        }
        array.adicionarValor(valor);
        return array.toArray();
    }*/

    private void adicionarValorNaLinha(int valor, int linha) {
        int i = 0;
        int[] tempArray = new int[this.matriz[linha].length + 1];
        for (int elemento : this.matriz[linha]) {
            tempArray[i] = elemento;
            i++;
        }
        this.matriz[linha] = tempArray;
        this.matriz[linha][this.matriz[linha].length - 1] = valor;

    }

    /*public int[][] adicionarUmaLinhaComUmValorNovo(int valor, int linha) {
        adicionarNovaLinha(linha);
        int [][] novaMatriz= this.toMatriz();
        int[] array= adicionaValorNumaLinha(valor, linha);
        for (int i = 0; i < this.matriz.length; i++) {
            novaMatriz[linha][i]=array[i];
        }
        return novaMatriz;
    }*/

    public void adicionarValorNumaLinha(int valor, int linha) {
        adicionarNovaLinha(linha);
        adicionarValorNaLinha(valor, linha);
    }

    public void retiraPrimeiroElementoComDeterminadoValor(int valor) {
        boolean primeiroElemento = false;
        //Vou reaproveitar o método da classe Array e vou linha a linha verificar o elemento
        for (int indiceLinha = 0; indiceLinha < this.matriz.length && !primeiroElemento; indiceLinha++) {
            Array array = new Array();
            for (int indiceColuna = 0; indiceColuna < this.matriz[indiceLinha].length; indiceColuna++) {
                array.adicionarValor(this.matriz[indiceLinha][indiceColuna]);
            }
            if ( array.contemValor(valor) ) {
                primeiroElemento = true;
                array.retirarPrimeiroElementoComDeterminadoValor(valor);
                this.matriz[indiceLinha] = array.toArray();
            }
        }
    }

    public boolean estaVaziaMatriz() {
        return this.matriz.length == 0;
    }

    private Array converteMatrizEmArray(){
        Array array=new Array();
        for (int indiceLinha = 0; indiceLinha < this.matriz.length; indiceLinha++) {
            for (int indiceColuna = 0; indiceColuna < this.matriz[indiceLinha].length; indiceColuna++) {
                array.adicionarValor(this.matriz[indiceLinha][indiceColuna]);
            }
        }
        return array;
    }

    public int maiorElementoDaMatriz() {
        Array array= converteMatrizEmArray();
        return array.maiorElemento();
    }

    public int menorElementoDaMatriz() {
        Array array = converteMatrizEmArray();
        return array.menorElemento();
    }

    public double mediaElementosDaMatriz() {
        Array array = converteMatrizEmArray();
        return array.mediaElementos();
    }

    public int[] retornaVetorComOTotalDoSomatorioDeCadaLinha() {
        Array array = new Array();
        for (int indiceLinha = 0; indiceLinha < matriz.length; indiceLinha++) {
            int somatorioLinha = 0;
            for (int indiceColuna = 0; indiceColuna < matriz[indiceLinha].length; indiceColuna++) {
                somatorioLinha += this.matriz[indiceLinha][indiceColuna];
            }
            array.adicionarValor(somatorioLinha);
        }
        return array.toArray();
    }

    public int[] retornaVetorComOTotalDoSomatorioDeCadaColuna() {
        Array array = new Array();
        for (int indiceColuna = 0; indiceColuna < matriz[0].length; indiceColuna++) {
            int somatorioColuna = 0;
            for (int indiceLinha = 0; indiceLinha < matriz.length; indiceLinha++) {
                somatorioColuna += this.matriz[indiceLinha][indiceColuna];
            }
            array.adicionarValor(somatorioColuna);
        }
        return array.toArray();
    }

    public int indiceComAMaiorSoma() {
        //O indice da linha da matriz com maior soma corresponde ao indice do array com as somas
        int[] array = retornaVetorComOTotalDoSomatorioDeCadaLinha();
        int maiorElemento = array[0];
        int indiceMaiorElemento = 0;

        for (int indice = 0; indice < array.length; indice++) {
            if ( array[indice] > maiorElemento ) {
                maiorElemento = array[indice];
                indiceMaiorElemento = indice;
            }
        }
        return indiceMaiorElemento;
    }

    public boolean verificaMatrizQuadrada() {
        return ExercicioTREZE.verificaMatrizQuadrada(this.matriz);
    }

    public boolean verificaQuadradaSimetrica() {
        //Uma matriz quadrada é simétrica quando o valor dos indices contrários é igual
        if ( !verificaMatrizQuadrada() )
            return false;
        for (int indiceLinha = 0; indiceLinha < matriz.length; indiceLinha++) {
            for (int indiceColuna = 0; indiceColuna < matriz.length; indiceColuna++) {
                if ( matriz[indiceLinha][indiceColuna] != matriz[indiceColuna][indiceLinha] ) {
                    return false;
                }
            }
        }
        return true;
    }

    public int quantidadeDeElementosNaoNulosNaDiagonalPrincipal() {
        if ( !verificaMatrizQuadrada() )
            return -1;
        int contador = 0;
        int[] diagonalPrincipal = ExercicioQUINZE.diagonalPrincipal(this.matriz);
        for (int elemento : diagonalPrincipal) {
            if ( elemento != 0 )
                contador++;
        }
        return contador;
    }

    public boolean verificaIgualdadeDasDiagonais() {
        int[] diagonalPrincipal = ExercicioQUINZE.diagonalPrincipal(this.matriz);
        int[] diagonalSecundaria = ExercicioQUINZE.diagonalSecundaria(this.matriz);

        if ( diagonalPrincipal == null || diagonalSecundaria == null )
            return false;
        if ( diagonalPrincipal.length != diagonalSecundaria.length )
            return false;
        for (int indiceLinha = 0; indiceLinha < diagonalPrincipal.length; indiceLinha++) {
            if ( diagonalPrincipal[indiceLinha] != diagonalSecundaria[indiceLinha] )
                return false;
        }
        return true;
    }

    private int numeroDeAlgarismos(int numero) {
        numero = Math.abs(numero);
        int numAlgarismos = 0;

        do {
            numero = numero / 10;
            numAlgarismos++;
        } while (numero >= 1);

        return numAlgarismos;
    }

    private int mediaAlgarismosMatriz() {
        Array array = converteMatrizEmArray();
        return array.mediaAlgarismosDosNumerosDeVetor();
    }

    public int[] retornaVetorComNumeroDeAlgarismosSuperiorAMediaDeAlgarismosDaMatriz() {
        int mediaElementos = mediaAlgarismosMatriz();
        Array array = new Array();
        for (int indiceLinha = 0; indiceLinha < matriz.length; indiceLinha++) {
            for (int indiceColuna = 0; indiceColuna < matriz[0].length; indiceColuna++) {
                if ( numeroDeAlgarismos(matriz[indiceLinha][indiceColuna]) > mediaElementos ) {
                    array.adicionarValor(matriz[indiceLinha][indiceColuna]);
                }
            }
        }
        return array.toArray();
    }

    private double percentagemAlgarismosParesMatriz() {
        Array array = converteMatrizEmArray();
        return array.percentagemAlgarismosParesDoArray();
    }

    private int algarismosPares(int numero) {
        numero = Math.abs(numero);
        int contaPar = 0;

        do {
            if ( numero % 2 == 0 ) {
                contaPar++;
            }
            numero = numero / 10;
        } while (numero >= 1);

        return contaPar;
    }

    private double percentagemAlgarismosParesNumero(int numero) {
        double algarismosPares = algarismosPares(numero);
        double totalAlgarismos = numeroDeAlgarismos(numero);
        return (algarismosPares / totalAlgarismos) * 100;
    }

    public int[] retornaVetorComNumeroComPercentagemDeAlgarismosParesSuperioresAMedia(){
        double percentagemElementosPares= percentagemAlgarismosParesMatriz();
        Array array= new Array();
        for (int indiceLinha = 0; indiceLinha < this.matriz.length; indiceLinha++) {
            for (int indiceColuna = 0; indiceColuna < matriz[indiceLinha].length; indiceColuna++) {
                if(percentagemAlgarismosParesNumero(this.matriz[indiceLinha][indiceColuna])>percentagemElementosPares){
                    array.adicionarValor(this.matriz[indiceLinha][indiceColuna]);
                }
            }
        }
        return array.toArray();
    }

    private int [] inverteArray(int[] array){
        int[] novoArray= new int[array.length];
        for (int indice = 0; indice < array.length; indice++) {
            novoArray[array.length-1-indice]=array[indice];
        }
        return novoArray;
    }

    public void inverteLinhasDaMatriz(){
        if(estaVaziaMatriz()){
            this.matriz=matriz;
        }
        int[][]novaMatriz=new int[this.matriz.length][this.matriz[0].length];
        for (int indice = 0; indice < this.matriz.length; indice++) {
            novaMatriz[indice]=inverteArray(this.matriz[indice]);
        }
        this.matriz=novaMatriz;
    }
    
    public void inverteColunasMatriz() {
        if(estaVaziaMatriz()){
            this.matriz=matriz;
        }
        int[][] novaMatriz= new int[matriz.length][matriz[0].length];
        for (int indiceColuna = 0; indiceColuna < this.matriz.length; indiceColuna++) {
            for (int indiceLinha = 0; indiceLinha < this.matriz[indiceColuna].length; indiceLinha++) {
                novaMatriz[indiceLinha][indiceColuna]=this.matriz[this.matriz[indiceColuna].length-1-indiceLinha][indiceColuna];
            }
        }
        this.matriz= novaMatriz;
    }

    private void trocarLinhasPorColunas(){
        int [][] novaMatriz=new int[this.matriz.length][this.matriz[0].length];
        for (int indiceLinha = 0; indiceLinha < this.matriz.length; indiceLinha++) {
            for (int indiceColuna = 0; indiceColuna < this.matriz[indiceLinha].length; indiceColuna++) {
                novaMatriz[indiceLinha][indiceColuna]=this.matriz[indiceColuna][indiceLinha];
            }
        }
        this.matriz= novaMatriz;
    }

    /**
     * Este método roda a matriz 90 graus mas só funciona para matrizes quadradas
     */
    public void rodarAMatrizNoventaGraus(){
        if(verificaMatrizQuadrada()) {
            trocarLinhasPorColunas();
            inverteLinhasDaMatriz();
        }
    }

    public void rodarAMatrizCentoEOitentaGraus(){
        if(verificaMatrizQuadrada()){
            inverteLinhasDaMatriz();
            inverteColunasMatriz();
        }
    }

    public void rodarAMatrizMenosNoventaGraus(){
        if(verificaMatrizQuadrada()){
            trocarLinhasPorColunas();
            inverteColunasMatriz();
        }
    }
}
