package Sudoku;

public class SudokuCell {
    private int value;
    private boolean nonEditable;


    public SudokuCell(int value, boolean nonEditable) {
        checkValue(value);
        this.value = value;
        this.nonEditable = nonEditable;
    }

    /**
     * verifica se o valor a introduzir é válido
     *
     * @return
     */
    public int getValue() {
        if ( value < 0 || value > 9 )
            throw new IllegalArgumentException("O número não é válido");
        return this.value = value;
    }

    private void checkValue(int input) {
        if ( input < 0 || input > 9 )
            throw new IllegalArgumentException("O número não é válido");
    }

    public boolean checkEmpty() {
        return this.value == 0 && this.nonEditable == false;
    }

    /**
     * método que verifica se a célula já tem número ou não
     *
     * @return
     */
    private boolean isEmpty() {
        return value == 0;
    }

    public boolean isNonEditable() {
        if ( nonEditable ) {
            return true;
        } else return false;
    }

    /**
     * Este método adiciona um valor na célula caso não seja um valor que da matriz inicial
     *
     * @param input valor a adicionar
     */
    public void addValue(int input) {
        checkValue(input);
        if ( nonEditable ) {
            throw new IllegalArgumentException("Está célula não é editável");
        }
        this.value = input;
    }

    /**
     * Este método muda o valor da célula para zero
     */
    public void removeValue() {
        if ( nonEditable ) {
            System.out.println("Esse número é da sua matriz inicial, não pode ser removido.");
        }
        this.value = 0;
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;
        SudokuCell cell = (SudokuCell) o;
        return value == cell.value && nonEditable == cell.nonEditable;
    }

}
