package Sudoku;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);

        int[][] initialMatrixHard = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}};

        int[][] initialMatrixMedium = {
                {0, 2, 4, 0, 0, 7, 0, 0, 0},
                {6, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 3, 6, 8, 0, 4, 1, 5},
                {4, 3, 1, 0, 0, 5, 0, 0, 0},
                {5, 0, 0, 0, 0, 0, 0, 3, 2},
                {7, 9, 0, 0, 0, 0, 0, 6, 0},
                {2, 0, 9, 7, 1, 0, 8, 0, 0},
                {0, 4, 0, 0, 9, 3, 0, 0, 0},
                {3, 1, 0, 0, 0, 4, 7, 5, 0}};

        int[][] initialMatrixEasy = {
                {5, 3, 0, 6, 7, 8, 0, 0, 2},
                {6, 0, 2, 1, 9, 5, 0, 4, 8},
                {0, 9, 8, 3, 0, 2, 0, 6, 0},
                {8, 0, 9, 0, 6, 0, 4, 2, 3},
                {4, 2, 0, 8, 0, 3, 0, 9, 1},
                {7, 1, 3, 0, 2, 0, 8, 5, 6},
                {0, 6, 1, 0, 3, 7, 2, 8, 4},
                {0, 0, 0, 4, 1, 9, 6, 0, 5},
                {3, 4, 5, 0, 8, 0, 0, 7, 9}};
                /*{5,3,4,6,7,8,9,1,2},
                {6,7,2,1,9,5,3,4,8},
                {1,9,8,3,4,2,5,6,7},
                {8,5,9,7,6,1,4,2,3},
                {4,2,6,8,5,3,7,9,1},
                {7,1,3,9,2,4,8,5,6},
                {9,6,1,5,3,7,2,8,4},
                {2,8,7,4,1,9,6,3,5},
                {3,4,5,0,8,0,0,7,9}};*/

        System.out.println("Escolha a dificuldade do jogo:");
        System.out.println("1: fácil, 2: médio, 3:difícil");
        int game = ler.nextInt();
        int[][] gameMatrix = new int[9][9];
        if ( game == 1 ) {
            gameMatrix = initialMatrixEasy;
        }
        if ( game == 2 ) {
            gameMatrix = initialMatrixMedium;
        }
        if ( game == 3 ) {
            gameMatrix = initialMatrixHard;
        }
        SudokuGrid newGame = new SudokuGrid(gameMatrix);
        System.out.println("A sua matriz de jogo é a seguinte: ");
        newGame.printMatrix();
        System.out.println();
        System.out.println("Que número quer adicionar?");
        int number = ler.nextInt();
        System.out.println("Em que linha?");
        int lineIndex = ler.nextInt();
        System.out.println("E coluna?");
        int columnIndex = ler.nextInt();
        newGame.addValue(number, lineIndex, columnIndex);
        newGame.printMatrix();
        do {
            System.out.println();
            System.out.println("Quer adicionar ou remover um número?");
            System.out.println("Carregue 1 para adicionar e 2 para retirar:");
            int option = ler.nextInt();
            if ( option < 1 || option > 2 ) {
                System.out.println("Por favor insira o número 1 ou 2");
                option = ler.nextInt();
            }
            if ( option == 1 ) {
                System.out.println("Que número quer adicionar?");
                number = ler.nextInt();
                System.out.println("Em que linha?");
                lineIndex = ler.nextInt();
                System.out.println("E coluna?");
                columnIndex = ler.nextInt();
                if ( !newGame.addValue(number, lineIndex, columnIndex) ) {
                    System.out.println("A sua jogada é inválida.");
                    System.out.println("Verifique se o número já não está presente na mesma linha, coluna ou no quadrante.");
                } else
                    newGame.addValue(number, lineIndex, columnIndex);
            }
            if ( option == 2 ) {
                System.out.println("Em que linha quer remover o número?");
                lineIndex = ler.nextInt();
                System.out.println("E coluna?");
                columnIndex = ler.nextInt();
                newGame.removeValue(lineIndex, columnIndex);
            }
            newGame.printMatrix();
        } while (!newGame.isComplete());
        System.out.println();
        System.out.println("Parabéns! Terminou o seu jogo com sucesso!");
    }
}
