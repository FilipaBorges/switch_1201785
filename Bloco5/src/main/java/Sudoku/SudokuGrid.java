package Sudoku;

public class SudokuGrid {
    //private int [][] gameMatrix= new int [9][9];
    final private SudokuCell[][] gameGrid = new SudokuCell[9][9];
    private final String ANSI_GREEN = "\u001B[32m";
    private final String ANSI_RESET = "\u001B[0m";
    private final String ANSI_PURPLE = "\u001B[35m";

    public SudokuGrid(int[][] initialMatrix) {
        buildInitialMatrix(initialMatrix);
    }

    private void buildInitialMatrix(int[][] initialMatrix) {
        if ( !checkMatrix(initialMatrix) ) {
            throw new IllegalArgumentException("Tem que inserir uma matriz de 9 por 9 com números de 0 a 9");
        }
        SudokuCell cell;
        boolean nonEditable;
        for (int lineIndex = 0; lineIndex < 9; lineIndex++) {
            for (int columnIndex = 0; columnIndex < 9; columnIndex++) {
                if ( initialMatrix[lineIndex][columnIndex] != 0 ) {
                    nonEditable = true;
                } else {
                    nonEditable = false;
                }
                int value = initialMatrix[lineIndex][columnIndex];
                cell = new SudokuCell(value, nonEditable);
                this.gameGrid[lineIndex][columnIndex] = cell;
            }
        }
    }

    private boolean checkMatrix(int[][] matrix) {
        boolean isValid = true;
        if ( matrix.length != 9 || matrix[0].length != 9 ) {
            isValid = false;
        }
        for (int lineIndex = 0; lineIndex < 9 && isValid; lineIndex++) {
            for (int columnIndex = 0; columnIndex < 9 && isValid; columnIndex++) {
                if ( matrix[lineIndex][columnIndex] < 0 || matrix[lineIndex][columnIndex] > 9 ) {
                    isValid = false;
                }
            }
        }
        return isValid;
    }

    public boolean addValue(int value, int line, int column) {
        if ( isPlayValid(value, line, column) ) {
            this.gameGrid[line][column].addValue(value);
            return true;
        } else {
            return false;
        }
    }

    public void removeValue(int line, int column) {
        this.gameGrid[line][column].removeValue();
    }

    public boolean isComplete() {
        boolean isComplete = true;
        for (int lineIndex = 0; lineIndex < 9; lineIndex++) {
            for (int columnIndex = 0; columnIndex < 9; columnIndex++) {
                if ( this.gameGrid[lineIndex][columnIndex].checkEmpty() ) {
                    isComplete = false;
                }
            }
        }
        return isComplete;
    }

    public void printMatrix() {
        for (int lineIndex = 0; lineIndex < 9; lineIndex++) {
            System.out.println();
            for (int columnIndex = 0; columnIndex < 9; columnIndex++) {
                if ( this.gameGrid[lineIndex][columnIndex].isNonEditable() ) {
                    System.out.print(ANSI_PURPLE + " " + this.gameGrid[lineIndex][columnIndex].getValue() + ANSI_RESET);
                } else
                    System.out.print(ANSI_GREEN + " " + this.gameGrid[lineIndex][columnIndex].getValue() + ANSI_RESET);
            }
        }
    }

    public boolean isPlayValid(int value, int lineIndex, int columnIndex) {
        boolean isValid = true;
        if ( !checkLine(value, lineIndex) )
            isValid = false;
        if ( !checkColumn(value, columnIndex) )
            isValid = false;
        if ( !checkSection(value, lineIndex, columnIndex) )
            isValid = false;
        return isValid;
    }

    private boolean checkLine(int value, int lineIndex) {
        for (int columnIndex = 0; columnIndex < 9; columnIndex++) {
            if ( this.gameGrid[lineIndex][columnIndex].getValue() == value ) {
                return false;
            }
        }
        return true;
    }

    private boolean checkColumn(int value, int columnIndex) {
        for (int lineIndex = 0; lineIndex < 9; lineIndex++) {
            if ( this.gameGrid[lineIndex][columnIndex].getValue() == value ) {
                return false;
            }
        }
        return true;
    }

    private boolean checkSection(int value, int lineIndex, int columnIndex) {
        int i, j;
        boolean validSection = true;

        if ( lineIndex >= 0 && lineIndex <= 2 ) {
            for (i = 0; i <= 2; i++) {
                if ( columnIndex >= 0 && columnIndex <= 2 ) {
                    for (j = 0; j <= 2; j++) {
                        if ( gameGrid[i][j].getValue() == value && validSection ) {
                            validSection = false;
                        }
                    }
                } else if ( columnIndex > 2 && columnIndex <= 5 ) {
                    for (j = 3; j <= 5; j++) {
                        if ( gameGrid[i][j].getValue() == value && validSection ) {
                            validSection = false;
                        }
                    }
                } else if ( columnIndex > 5 && columnIndex <= 9 ) {
                    for (j = 6; j <= 8; j++) {
                        if ( gameGrid[i][j].getValue() == value && validSection ) {
                            validSection = false;
                        }
                    }
                }
            }
        } else if ( lineIndex > 2 && lineIndex <= 5 ) {
            for (i = 3; i <= 5; i++)
                if ( columnIndex >= 0 && columnIndex <= 2 ) {
                    for (j = 0; j <= 2; j++) {
                        if ( gameGrid[i][j].getValue() == value && validSection ) {
                            validSection = false;
                        }
                    }
                } else if ( columnIndex > 2 && columnIndex <= 5 ) {
                    for (j = 3; j <= 5; j++) {
                        if ( gameGrid[i][j].getValue() == value && validSection ) {
                            validSection = false;
                        }
                    }
                } else if ( columnIndex > 5 && columnIndex <= 9 ) {
                    for (j = 6; j <= 8; j++) {
                        if ( gameGrid[i][j].getValue() == value && validSection ) {
                            validSection = false;
                        }
                    }
                }
        } else if ( lineIndex > 5 && lineIndex <= 8 ) {
            for (i = 6; i <= 8; i++)
                if ( columnIndex >= 0 && columnIndex <= 2 ) {
                    for (j = 0; j <= 2; j++) {
                        if ( gameGrid[i][j].getValue() == value && validSection ) {
                            validSection = false;
                        }
                    }
                } else if ( columnIndex > 2 && columnIndex <= 5 ) {
                    for (j = 3; j <= 5; j++) {
                        if ( gameGrid[i][j].getValue() == value && validSection ) {
                            validSection = false;
                        }
                    }
                } else if ( columnIndex > 5 && columnIndex <= 9 ) {
                    for (j = 6; j <= 8; j++) {
                        if ( gameGrid[i][j].getValue() == value && validSection ) {
                            validSection = false;
                        }
                    }
                }
        }
        return validSection;
    }

    public int[][] toMatrix() {
        int[][] matrix = new int[9][9];
        for (int lineIndex = 0; lineIndex < 9; lineIndex++) {
            for (int columnIndex = 0; columnIndex < 9; columnIndex++) {
                matrix[lineIndex][columnIndex] = this.gameGrid[lineIndex][columnIndex].getValue();
            }
        }
        return matrix;
    }
}
