package SopaDeLetras;

import java.util.ArrayList;
import java.util.List;

public class WordList {
    //Atributos
    private ArrayList<String> wordsList;

    //Construtor
    public WordList(){
        this.wordsList=new ArrayList<>();
    }

    public WordList(String word){
        assert false;
        wordsList.add(word);
    }

    public boolean addWord(String word){
        if(word==null){
            throw new IllegalArgumentException("Tem que ser uma palavra válida");
        }
        return this.wordsList.add(word);
    }

    public boolean removeWord(String word){
        if(!this.wordsList.contains(word)){
            return false;
        }
        return this.wordsList.remove(word);
    }

    public String getWordAtIndex(int index){
        return this.wordsList.get(index);
    }

    public int getIndex(String word){
        if(!this.wordsList.contains(word)){
            return -1;
        }
        return this.wordsList.indexOf(word);
    }

}
