package SopaDeLetras;

public class GameMatrix {
    private char [][] gameMatrix;

    public GameMatrix(){
        this.gameMatrix= new char[0][0];
    };

    public GameMatrix(int size){
        getSize(size);
        this.gameMatrix= new char[size][size];
    }

    public GameMatrix(char[][]gameMatrix){
        this.gameMatrix=gameMatrix;
    }

    private int getSize(int size){
        if(size<6){
            throw new IllegalArgumentException("O tamanho escolhido tem que ser maior que 5");
        }
        return size;
    }

    public static void imprimeMatriz(char [][] gameMatrix) {
        for (int lineIndex = 0; lineIndex < gameMatrix.length; lineIndex++) {
            System.out.println();
            for (int columnIndex = 0; columnIndex < gameMatrix.length; columnIndex++) {
                System.out.print(" " + gameMatrix[lineIndex][columnIndex]);
            }
        }
    }

    public char[][] toMatrix(){
        char [][] tempMatrix= new char[gameMatrix.length][gameMatrix.length];
        for (int lineIndex = 0; lineIndex < this.gameMatrix.length; lineIndex++) {
            for (int columnIndex = 0; columnIndex < this.gameMatrix.length; columnIndex++) {
                tempMatrix[lineIndex][columnIndex]=this.gameMatrix[lineIndex][columnIndex];
            }
        }
        return tempMatrix;
    }

    public void addWord(int initialCellLine, int initialCellColumn, int finalCellLine, int finalCellColumn, String word){
        GameHelper matrix= new GameHelper(this.gameMatrix);
        this.gameMatrix=matrix.placeWord(initialCellLine, initialCellColumn,finalCellLine,finalCellColumn, word);
    }


    public void fillRandomlyMatrix(){
        Alphabet alphabet= new Alphabet();
        for (int lineIndex = 0; lineIndex < this.gameMatrix.length; lineIndex++) {
            for (int columnIndex = 0; columnIndex < this.gameMatrix.length; columnIndex++) {
                alphabet.shuffleAlphabet();
                this.gameMatrix[lineIndex][columnIndex]=alphabet.getIndex(0);
            }
        }
    }


}
