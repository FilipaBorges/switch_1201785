package SopaDeLetras;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Alphabet {
    private ArrayList<Character> alphabet= new ArrayList();

    public Alphabet(){
        this.alphabet=fillAlphabet();
    }

    public char getIndex(int index){
        return alphabet.get(index);
    }

    public void shuffleAlphabet(){
        Collections.shuffle(this.alphabet, new Random(2));
    }

    private ArrayList<Character> fillAlphabet(){
        int i=0;
        char a= 'a';
        do {this.alphabet.add(i,a);
            i++;
            a++;
        }while (i<26);
        return this.alphabet;
    }
}
