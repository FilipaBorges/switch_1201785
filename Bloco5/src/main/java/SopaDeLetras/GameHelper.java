package SopaDeLetras;

public class GameHelper {
    /*private int initialCellLine;
    private int initialCellColumn;
    private int finalCellLine;
    private int finalCellColumn;
    private String word;*/
    private char [][] gameMatrix;

    public GameHelper() {
    }

    public GameHelper(char[][] gameMatrix){
        this.gameMatrix=gameMatrix;
    }

    public char [][] placeWord(int initialCellLine, int initialCellColumn, int finalCellLine, int finalCellColumn, String word) {
        if ( !checkCoordinates(initialCellLine, initialCellColumn, finalCellLine, finalCellColumn, word) ) {
            throw new IllegalArgumentException("A palavra pretendida não cabe nessas coordenadas");
        }
        char[] arrayWord = word.toCharArray();
        if ( checkEsquerdaDireita(initialCellLine, initialCellColumn, finalCellLine, finalCellColumn) ) {
            int x = 0;
            while (x < arrayWord.length) {
                gameMatrix[initialCellLine][initialCellColumn + x] = arrayWord[x];
                x++;
            }
        } else if ( checkDireitaEsquerda(initialCellLine, initialCellColumn, finalCellLine, finalCellColumn)) {
            int x = 0;
            while (x < arrayWord.length) {
                gameMatrix[initialCellLine][initialCellColumn - x] = arrayWord[x];
                x++;
            }
        } else if ( checkCimaBaixo(initialCellLine, initialCellColumn, finalCellLine, finalCellColumn) ) {
            int x = 0;
            while (x < arrayWord.length) {
                gameMatrix[initialCellLine + x][initialCellColumn] = arrayWord[x];
                x++;
            }
        } else if ( checkBaixoCima(initialCellLine, initialCellColumn, finalCellLine, finalCellColumn) ) {
            int x = 0;
            while (x < arrayWord.length) {
                gameMatrix[initialCellLine - x][initialCellColumn] = arrayWord[x];
                x++;
            }
        } else if ( checkDiagonalDescendenteDireita(initialCellLine, initialCellColumn, finalCellLine, finalCellColumn) ) {
            int x = 0;
            while (x < arrayWord.length) {
                gameMatrix[initialCellLine + x][initialCellColumn + x] = arrayWord[x];
                x++;
            }
        } else if ( checkDiagonalDescendenteEsquerda(initialCellLine, initialCellColumn, finalCellLine, finalCellColumn) ) {
            int x = 0;
            while (x < arrayWord.length) {
                gameMatrix[initialCellLine + x][initialCellColumn - x] = arrayWord[x];
                x++;
            }
        } else if ( checkDiagonalAscendenteDireita(initialCellLine, initialCellColumn, finalCellLine, finalCellColumn) ) {
            int x = 0;
            while (x < arrayWord.length) {
                gameMatrix[initialCellLine - x][initialCellColumn + x] = arrayWord[x];
                x++;
            }
        } else if ( checkDiagonalAscendenteEsquerda(initialCellLine, initialCellColumn, finalCellLine, finalCellColumn) ) {
            int x = 0;
            while (x < arrayWord.length) {
                gameMatrix[initialCellLine - x][initialCellColumn - x] = arrayWord[x];
                x++;
            }
        }
        return this.gameMatrix;
    }


    private boolean checkCoordinates(int initialCellLine, int initialCellColumn, int finalCellLine, int finalCellColumn, String word) {
        int wordlength = word.length();
        return Math.abs(initialCellLine - finalCellLine)+1 == wordlength || Math.abs(initialCellColumn - finalCellColumn)+1 == wordlength;
    }

    private boolean checkEsquerdaDireita(int initialCellLine, int initialCellColumn, int finalCellLine, int finalCellColumn) {
        return initialCellLine == finalCellLine && initialCellColumn < finalCellColumn;
    }

    private boolean checkDireitaEsquerda(int initialCellLine, int initialCellColumn, int finalCellLine, int finalCellColumn) {
        return initialCellLine == finalCellLine && finalCellColumn < initialCellLine;
    }

    private boolean checkCimaBaixo(int initialCellLine, int initialCellColumn, int finalCellLine, int finalCellColumn) {
        return initialCellColumn == finalCellColumn && initialCellLine < finalCellLine;
    }

    private boolean checkBaixoCima(int initialCellLine, int initialCellColumn, int finalCellLine, int finalCellColumn) {
        return initialCellColumn == finalCellColumn && initialCellLine > finalCellLine;
    }

    private boolean checkDiagonalDescendenteDireita(int initialCellLine, int initialCellColumn, int finalCellLine, int finalCellColumn) {
        return initialCellLine < finalCellLine && initialCellColumn < finalCellColumn;
    }

    private boolean checkDiagonalDescendenteEsquerda(int initialCellLine, int initialCellColumn, int finalCellLine, int finalCellColumn) {
        return initialCellLine < finalCellLine && initialCellColumn > finalCellColumn;
    }

    private boolean checkDiagonalAscendenteDireita(int initialCellLine, int initialCellColumn, int finalCellLine, int finalCellColumn) {
        return initialCellLine > finalCellLine && initialCellColumn < finalCellColumn;
    }

    private boolean checkDiagonalAscendenteEsquerda(int initialCellLine, int initialCellColumn, int finalCellLine, int finalCellColumn) {
        return initialCellLine > finalCellLine && initialCellColumn > finalCellColumn;
    }


}
