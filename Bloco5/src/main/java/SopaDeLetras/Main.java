package SopaDeLetras;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o tamanho da matriz:");
        int size = ler.nextInt();

        WordList wordsList = new WordList();
        GameMatrix initialMatrix = new GameMatrix(size);
        Alphabet alphabet = new Alphabet();

        wordsList.addWord("banana");
        wordsList.addWord("ananas");
        wordsList.addWord("morango");
        wordsList.addWord("pera");

        initialMatrix.fillRandomlyMatrix();
        initialMatrix.addWord(1, 1, 6, 6, wordsList.getWordAtIndex(0));
        initialMatrix.addWord(2, 0, 2, 5, wordsList.getWordAtIndex(1));
        initialMatrix.addWord(5, 1, 5, 7, wordsList.getWordAtIndex(2));
        initialMatrix.addWord(1, 7, 4, 4, wordsList.getWordAtIndex(3));
        char[][] gameMatrix = initialMatrix.toMatrix();
        System.out.println("A sua matriz de jogo é a seguinte:");
        GameMatrix.imprimeMatriz(gameMatrix);
        System.out.println("Introduza a linha da primeira palavra a procurar: ");
        int lineIndex = ler.nextInt();
        System.out.println("E a coluna: ");
        int columnIndex = ler.nextInt();

    }
}

