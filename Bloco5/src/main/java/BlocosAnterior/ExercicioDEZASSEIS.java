package BlocosAnterior;

public class ExercicioDEZASSEIS {

    /**
     * Determinante de uma matriz desde ordem 1 a ordem n usando la Place
     * Site de apoio: https://matematicabasica.net/matrizes-e-determinantes/
     *
     * @param matriz
     * @return
     */
    public static Integer determinanteMatrizTeoremaLaPlace(int[][] matriz) {
        int determinante;

        //a matriz tem que ser quadrada. Se não for retorna null
        if (!ExercicioTREZE.verificaMatrizQuadrada(matriz)) {
            return null;
        }

        //se a matriz for de ordem 1 então o determinante é o valor da matriz
        if (matriz.length == 1) {
            determinante = matriz[0][0];
            return determinante;
        } else
            // se a matriz for de ordem dois:
            if (matriz.length == 2) {
                determinante = determinanteMatrizDeOrdemDois(matriz);
                return determinante;
            } else
                //para matriz de ordem 3 usamos a regra de Sarrus
                if (matriz.length == 3) {
                    determinante = determinanteMatrizDeOrdemTres(matriz);
                    return determinante;
                }
                else
                    determinante= determinanteMatrizDeOrdemQuatroOuSuperior(matriz);

        return determinante;
    }


    public static int determinanteMatrizDeOrdemDois(int[][] matriz) {
        int determinante = matriz[0][0] * matriz[1][1] - matriz[0][1] * matriz[1][0];

        return determinante;
    }

    public static int determinanteMatrizDeOrdemTres(int[][] matriz) {
        int determinante;

        determinante = (matriz[0][0] * matriz[1][1] * matriz[2][2]) +
                (matriz[0][1] * matriz[1][2] * matriz[2][0]) +
                (matriz[0][2] * matriz[1][0] * matriz[2][1]) -
                (matriz[0][2] * matriz[1][1] * matriz[2][0]) -
                (matriz[0][0] * matriz[1][2] * matriz[2][1]) -
                (matriz[0][1] * matriz[1][0] * matriz[2][2]);

        return determinante;
    }



    //não preciso de testar que é quadrada porque já vem do exercicio principal onde já foi testado se é quadrada
    public static int[][] copiaEsqueletoMatrizAZeros(int[][] matriz) {
        int[][] matrizZeros = new int[matriz.length][matriz.length];

        //primeiro loop para todas as linhas
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz.length; coluna++) {
                matrizZeros[linha][coluna] = 0;

            }
        }
        return matrizZeros;
    }

    public static int[][] copiaMatriz(int[][] matriz) {

        int[][] copiaMatriz = new int[matriz.length][matriz.length];

        for (int i = 0; i < matriz.length; i++) {

            for (int j = 0; j < matriz[i].length; j++) {
                copiaMatriz[i][j] = matriz[i][j];
            }
        }

        return copiaMatriz;
    }

    public static int determinanteMatrizDeOrdemQuatroOuSuperior(int[][] matriz) {
        int determinante = 0, indiceLinhaAuxiliar, indiceColunaAuxiliar;
        int[][] auxiliar = ExercicioDEZASSEIS.copiaEsqueletoMatrizAZeros(matriz);
        int[][] copiaMatriz = ExercicioDEZASSEIS.copiaMatriz(matriz);

        do {
            // Se a matriz for de ordem 4 cortamos uma linha e conseguimos usar a regra de sarrus
            //matriz de n-1
            for (int i = 0; i < copiaMatriz.length; i++) {
                if (copiaMatriz[0][i] != 0) {
                    auxiliar = new int[copiaMatriz.length - 1][copiaMatriz.length - 1];
                    indiceLinhaAuxiliar = 0;
                    indiceColunaAuxiliar = 0;
                    //começamos na linha um porque a zero já foi escolhida
                    for (int indiceLinha = 1; indiceLinha < copiaMatriz.length; indiceLinha++) {
                        for (int indiceColuna = 0; indiceColuna < copiaMatriz.length; indiceColuna++) {
                            //temos que garantir que não introduzimos a coluna a ser retirada
                            if (indiceColuna != i) {
                                auxiliar[indiceLinhaAuxiliar][indiceColunaAuxiliar] = copiaMatriz[indiceLinha][indiceColuna];
                                indiceColunaAuxiliar++;
                            }
                        }
                        indiceLinhaAuxiliar++;
                        indiceColunaAuxiliar = 0;
                    }
                    //agora vamos calcular a linha e a coluna excluidas e somar ao total do determinante
                    determinante += Math.pow(-1, i) * copiaMatriz[0][i] * ExercicioDEZASSEIS.determinanteMatrizTeoremaLaPlace(auxiliar);
                }
            }
            copiaMatriz = ExercicioDEZASSEIS.copiaMatriz(auxiliar);
        } while (auxiliar.length != 3);

        return determinante;
    }
}
