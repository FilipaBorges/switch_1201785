package BlocosAnterior;

public class ExercicioUM {

    public static int numeroDigitos(int numero) {
        numero=Math.abs(numero);
        //Se o número não for positivo retorna o valor de dígitos a zero.
        int numeroDigitos = 0;

        while (numero > 0) {
            numeroDigitos++;
            numero = numero / 10;
        }
        return numeroDigitos;
    }
}
