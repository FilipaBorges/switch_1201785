package BlocosAnterior;

public class ExercicioDOIS {

    public static int[] converterArray(int numero) {
        //se o número não for positivo retorna um array vazio
        //o array retorna o número pela mesma ordem que se encontra.
        int numeroDigitos = ExercicioUM.numeroDigitos(numero);
        int[] array = new int[numeroDigitos];


        for (int pos=(numeroDigitos-1); pos>=0; pos--) {
                array[pos] = numero % 10;;
                numero = numero / 10;
            }
        return array;
    }
}

