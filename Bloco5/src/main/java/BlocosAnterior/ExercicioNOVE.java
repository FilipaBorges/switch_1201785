package BlocosAnterior;

public class ExercicioNOVE {

    public static String verificarCapicua(int numero) {
        if (numero < 0) {
            return "Insira um número positivo";
        }
        int[] array = ExercicioDOIS.converterArray(numero);
        int[] arrayInvertido = ExercicioNOVE.inverterArray(array);

        for (int i = 0; i < array.length; i++) {
            if (arrayInvertido[i] == array[i]) {
                return "O número é uma capicua";
            }
        }return "O número não é uma capicua";
    }

    public static int[] inverterArray(int[] array) {
        int[] arrayInvertido= new int[array.length];
        int n= array.length-1;

        for (int i=0; i< array.length;i++) {
            arrayInvertido[i]=array[n-i];
        }
        return arrayInvertido;
    }
}
