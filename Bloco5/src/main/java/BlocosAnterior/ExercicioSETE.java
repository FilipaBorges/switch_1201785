package BlocosAnterior;

public class ExercicioSETE {

    public static int[] multiplosTres(int limiteMin, int limiteMax) {
        //se não houver multiplos de 3 o vetor retorna vazio
        int[] limites= ExercicioSETE.verificarLimites(limiteMin,limiteMax);
        int [] multiplosTres= new int[multiplosTresIntervalo(limiteMin,limiteMax)];
        int indice=0;

        for (int i=limites[0]; i<=limites[1];i++)
            if (i % 3 == 0) {
                multiplosTres[indice] = i;
                indice++;
            }
        return multiplosTres;
    }

    public static int multiplosTresIntervalo(int limiteMin, int limiteMax) {
        int[] limites= ExercicioSETE.verificarLimites(limiteMin,limiteMax);
        int contarMultiplosTres=0;

        for (int i=limites[0]; i<=limites[1];i++){
            if (i%3==0){
                contarMultiplosTres++;
            }
        }
        return contarMultiplosTres;
    }

    public static int[] verificarLimites(int limiteMin, int limiteMax) {
        int[] verificarLimites= new int[2];

        if(limiteMin>limiteMax) {
            verificarLimites[0]=limiteMax;
            verificarLimites[1]=limiteMin;
        } else {
            verificarLimites[0]=limiteMin;
            verificarLimites[1]=limiteMax;
        }
        return verificarLimites;
    }

    public static int[] multiplosX(int limiteMin, int limiteMax, int x) {
        //se não houver multiplos de x o vetor retorna vazio
        int[] limites= ExercicioSETE.verificarLimites(limiteMin,limiteMax);
        int [] multiplosX= new int[contarMultiplosXnumIntervalo(limiteMin,limiteMax,x)];
        int indice=0;

        for (int i=limites[0]; i<=limites[1];i++)
            if (i % x == 0) {
                multiplosX[indice] = i;
                indice++;
            }
        return multiplosX;
    }

    public static int contarMultiplosXnumIntervalo (int limiteMin, int limiteMax, int x){
        int [] limites= ExercicioSETE.verificarLimites(limiteMin,limiteMax);
        int contarMultiplosX=0;

        for (int i=limites[0]; i<=limites[1];i++){
            if (i%x==0){
                contarMultiplosX++;
            }
        }
        return contarMultiplosX;
    }
}
