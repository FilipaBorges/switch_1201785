import BlocosAnterior.ExercicioQUINZE;

public class ArrayBidimensional {
    //atributos
    private Array[] bidimensional;

    //Construtores
    public ArrayBidimensional(){
        Array[] biArray= new Array[0];
        this.bidimensional=biArray;
    }

    public ArrayBidimensional(Array[] biArray){
        Array[] novoBiArray= new Array[biArray.length];
        for (int i = 0; i < biArray.length; i++) {
            novoBiArray[i]=biArray[i];
        }
        this.bidimensional= novoBiArray;
    }

    public ArrayBidimensional(int[][] array){
        Array[] novoBiArray= new Array[array.length];
        for (int i = 0; i < array.length; i++) {
            Array arr = new Array(array[i]);
            novoBiArray[i]=arr;
            }
        this.bidimensional=novoBiArray;
        }

    public int [][] toMatriz(){
        int[][]copiaBiArray= new int[this.bidimensional.length][];
        for (int i = 0; i < this.bidimensional.length; i++) {
            Array arr= new Array(this.bidimensional[i].toArray());
            copiaBiArray[i]=arr.toArray();
        }
        return copiaBiArray;
    }

    public int comprimento(){
        int contador=0;
        for (Array elemento:this.bidimensional) {
            contador++;
        }
        return contador;
    }

    public void retiraPrimeiroNumeroMatriz(int valor){
        boolean primeiroElemento=false;
        for (int indiceLinha = 0; indiceLinha < this.bidimensional.length && !primeiroElemento; indiceLinha++) {
            int comprimentoColuna= this.bidimensional[indiceLinha].comprimento();
            this.bidimensional[indiceLinha].retirarPrimeiroElementoComDeterminadoValor(valor);
            if(this.bidimensional[indiceLinha].comprimento()!=comprimentoColuna){
                primeiroElemento=true;
            }
        }
    }

    public boolean verificaMatrizVazia(){
        return bidimensional.length!=0;
    }

    public int maiorElemento(){
        int maiorElemento;
        Array maior = new Array();
        for (Array array:this.bidimensional) {
            maior.adicionarValor(array.maiorElemento());
        }
        maiorElemento=maior.maiorElemento();
        return maiorElemento;
    }

    public int menorElemento(){
        int menorElemento;
        Array menor = new Array();
        for (Array array:this.bidimensional) {
            menor.adicionarValor(array.menorElemento());
        }
        menorElemento=menor.menorElemento();
        return menorElemento;
    }

    public double mediaDosElementos(){
        int media;
        Array array = new Array();
        for (Array arr:this.bidimensional) {
            array.adicionarValor((int) arr.mediaElementos());
        }
        media= (int) array.mediaElementos();
        return media;
    }

    private int somaValoresNaMesmaLinha(Array array){
        int somatorio=0;
        int [] tempArray= array.toArray();
        for (int elemento:tempArray) {
           somatorio += elemento;
        }
        return somatorio;
    }

    public Array retornaVetorComOTotalDoSomatorioDeCadaLinha() {
        Array array = new Array();
        /*int somatorioLinha;
        for (int indiceLinha = 0; indiceLinha < bidimensional.length; indiceLinha++) {
            array=this.bidimensional[indiceLinha];
            int somatorioLinha = somaValoresNaMesmaLinha(array);
            array.adicionarValor(somatorioLinha);
        }*/
        for (Array arr:this.bidimensional) {
            array.adicionarValor(somaValoresNaMesmaLinha(arr));
        }
        return array;
    }

    public int retornaIndiceMaiorElemento(){
        Array somatorio= retornaVetorComOTotalDoSomatorioDeCadaLinha();
        int [] arraySomatorio= somatorio.toArray();
        int maiorElemento=arraySomatorio[0];
        int indiceMaior=0;
        for (int indice = 0; indice < somatorio.comprimento(); indice++) {
            if(arraySomatorio[indice]>maiorElemento){
                maiorElemento=arraySomatorio[indice];
                indiceMaior=indice;
            }
        }
        return indiceMaior;
    }

    public boolean eQuadrada() {
        verificaMatrizVazia();
        boolean quadrada=true;
        if(this.bidimensional.length!=this.bidimensional[0].comprimento()){
            quadrada=false;
        }
        for (Array array:this.bidimensional) {
            if(array.comprimento()!=this.bidimensional.length){
                quadrada=false;
            }
        }
        return quadrada;
    }
}
