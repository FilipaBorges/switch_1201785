import BlocosAnterior.ExercicioDOIS;
import BlocosAnterior.ExercicioNOVE;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Array {
    //atributos
    private int[] array;

    //Construtores
    public Array() {
        int[] copia= new int[0];
        this.array=copia;
    }

    public Array(int numero) {
        adicionarValor(numero);
    }

    public Array(int [] array){
        int[] copia= new int[array.length];
        for (int i = 0; i < array.length; i++) {
            copia[i]=array[i];
        }
        this.array=copia;
    }

    //Operações
    public int[] toArray() {
        if ( array == null ) {
            return null;
        }
        return Arrays.stream(this.array).toArray();
    }

    public int comprimento(){
        return this.array.length;
    }

    private boolean estaVazio() {
        if ( array.length <= 0 )
            throw new IllegalArgumentException("O array não pode estar vazio");
        return true;
    }

    //só para retornar true ou false
    public boolean vazio() {
        return this.array.length == 0;
    }

    public void adicionarValor(int valor) {
        int i = 0;
        int[] tempArray = new int[array.length + 1];
        for (int elemento : this.array) {
            tempArray[i] = elemento;
            i++;
        }
        this.array = Arrays.stream(tempArray).toArray();
        this.array[array.length - 1] = valor;
    }


    public boolean contemValor(int valor) {
        for (int elemento : this.array) {
            if ( elemento == valor )
                return true;
        }
        return false;
    }

    public void retirarPrimeiroElementoComDeterminadoValor(int valor) {
        if (!estaVazio())
            throw new IllegalArgumentException("O array não pode ser null");
        if(!contemValor(valor)){
            this.array=array;
        }else {
        int i = 0;
        int[] tempArray = new int[array.length - 1];
        boolean primeiroValor = false;
        for (int elemento : this.array) {
            if ( elemento == valor && !primeiroValor ) {
                primeiroValor = true;
            } else {
                tempArray[i] = elemento;
                i++;
            }
            this.array = Arrays.stream(tempArray).toArray();
        }
        }
    }

    public int retornaValorPeloIndice(int indice) {
        estaVazio();

        if ( indice >= array.length )
            throw new IllegalArgumentException("O array não tem tamanho para ter esse índice");

        return array[indice];
    }

    public int numeroElementos() {
        return array.length;
    }

    public int maiorElemento() {
        estaVazio();
        int maiorElemento = array[0];

        for (int elemento : this.array) {
            if ( elemento > maiorElemento )
                maiorElemento = elemento;
        }
        return maiorElemento;
    }

    public int menorElemento() {
        estaVazio();
        int menorElemento = array[0];

        for (int elemento : this.array) {
            if ( elemento < menorElemento )
                menorElemento = elemento;
        }
        return menorElemento;
    }

    public double mediaElementos() {
        estaVazio();
        if ( array.length == 1 ) {
            return array[0];
        }

        double somaElem = 0;
        double numElem = array.length;
        for (int indice : this.array) {
            somaElem += indice;
        }
        return somaElem / numElem;
    }

    private boolean verificaPar(int elemento) {
        return (elemento % 2 == 0);
    }

    public double mediaElementosPares() {
        estaVazio();

        double somaElemPar = 0;
        double numElemPares = 0;
        for (int indice : this.array) {
            if ( verificaPar(indice) ) {
                somaElemPar += indice;
                numElemPares++;
            }
        }
        if ( numElemPares == 0 )
            return 0;
        return somaElemPar / numElemPares;
    }

    public double mediaElementosImpares() {
        estaVazio();
        double somaElemImpar = 0;
        double numElemImpares = 0;
        for (int indice : this.array) {
            if ( !verificaPar(indice) ) {
                somaElemImpar += indice;
                numElemImpares++;
            }
        }
        if ( numElemImpares == 0 )
            return 0;
        return somaElemImpar / numElemImpares;
    }

    public double mediaMultiplosDeUmNumero(int multiplo) {
        estaVazio();
        double somaElementos = 0;
        double numeroElementos = 0;
        for (int element : this.array) {
            if (element % multiplo == 0) {
                somaElementos += element;
                numeroElementos++;
            }
        }
        if (numeroElementos == 0) return 0;
        return somaElementos / numeroElementos;
    }

    public void ordenaAscendente() {
        estaVazio();
        IntStream tempArray = Arrays.stream(this.array).sorted();
        this.array = tempArray.toArray();
    }

    public void ordenaDescendente() {
        estaVazio();
        int indice = 0;
        IntStream tempArray = Arrays.stream(this.array).sorted();
        int[] arrayOrdenado = tempArray.toArray();

        for (int i = arrayOrdenado.length - 1; i >= 0; i--) {
            this.array[indice] = arrayOrdenado[i];
            indice++;
        }
    }

    private boolean soUmElemento() {
        return this.array.length == 1;
    }

    public boolean todosElementosPares() {
        boolean todosPares = true;
        for (int elemento : this.array) {
            if ( !verificaPar(elemento) && todosPares ) {
                todosPares = false;
            }
        }
        return todosPares;
    }

    public boolean todosElementosImpares() {
        estaVazio();
        boolean todosImpares = true;
        for (int elemento : this.array) {
            if ( verificaPar(elemento) && todosImpares ) {
                todosImpares = false;
            }
        }
        return todosImpares;
    }

    public boolean verificaDuplicados() {
        estaVazio();
        boolean verificaDuplicado = false;
        for (int indice = 0; indice < this.array.length - 1 && !verificaDuplicado; indice++) {
            for (int indiceA = indice + 1; indiceA < this.array.length - 1 && !verificaDuplicado; indiceA++) {
                if ( array[indice] == array[indiceA] )
                    verificaDuplicado = true;
            }
        }
        return verificaDuplicado;
    }

    private int numeroDeAlgarismos(int numero) {
        numero = Math.abs(numero);
        int numAlgarismos = 0;

        do {
            numero = numero / 10;
            numAlgarismos++;
        } while (numero >= 1);

        return numAlgarismos;
    }

    public int mediaAlgarismosDosNumerosDeVetor() {
        int somaAlgarismos = 0;
        for (int elemento : this.array) {
            somaAlgarismos += numeroDeAlgarismos(elemento);
        }
        return somaAlgarismos / this.array.length;
    }

    /*private int [] novoArrayTamanhoDiferente(int[] tempArray, int indice){
        int [] novoArray = new int[indice];
        int novoIndice=0;
        for (int numero:tempArray) {
            while(novoIndice<indice) {
                novoArray[novoIndice]=tempArray[novoIndice];
                novoIndice++;
            }
        }
        return novoArray;
    }*/

    public int[] elementosComNumeroDeAlgarismosSuperiorAoDaMedia() {
        int mediaElementos = mediaAlgarismosDosNumerosDeVetor();
        /*int[] tempArray = new int[array.length];
        int indice = 0;*/
        Array novoArray = new Array();
        for (int elemento : this.array) {
            if ( numeroDeAlgarismos(elemento) > mediaElementos ) {
                novoArray.adicionarValor(elemento);
                /*tempArray[indice] = elemento;
                indice++;*/
            }
        }
        /*int[] novoArray = novoArrayTamanhoDiferente(tempArray, indice);*/
        return novoArray.toArray();
    }

    private int algarismosPares(int numero) {
        numero = Math.abs(numero);
        int contaPar = 0;

        do {
            if ( numero % 2 == 0 ) {
                contaPar++;
            }
            numero = numero / 10;
        } while (numero >= 1);

        return contaPar;
    }

    private double percentagemAlgarismosParesNumero(int numero) {
        double algarismosPares = algarismosPares(numero);
        double totalAlgarismos = numeroDeAlgarismos(numero);
        return (algarismosPares / totalAlgarismos) * 100;
    }

    public double percentagemAlgarismosParesDoArray() {
        double somaPar = 0;
        double totalAlgarismos = numeroAlgarismosArray();
        for (int elemento : this.array) {
            somaPar += algarismosPares(elemento);
        }
        return (somaPar / totalAlgarismos) * 100;
    }

    private int numeroAlgarismosArray() {
        int totalAlgarismos = 0;
        for (int elemento : this.array) {
            totalAlgarismos += numeroDeAlgarismos(elemento);
        }
        return totalAlgarismos;
    }

    public int[] elementosComPercentagemDeAlgarismosParesSuperiorAMedia() {
        double percentagemAlgarismosPares = percentagemAlgarismosParesDoArray();
        Array novoArray = new Array();
        for (int elemento : this.array) {
            if ( percentagemAlgarismosParesNumero(elemento) > percentagemAlgarismosPares ) {
                novoArray.adicionarValor(elemento);
            }
        }
        //int[] novoArray= novoArrayTamanhoDiferente(tempArray, indice);
        return novoArray.toArray();
    }

    public int[] retornaElementosSoDePares() {
        Array novoArray = new Array();
        for (int elemento : this.array) {
            if ( percentagemAlgarismosParesNumero(elemento) == 100 ) {
                novoArray.adicionarValor(elemento);
            }
        }
        return novoArray.toArray();
    }

    private boolean verificaNumeroComElementosCrescentes(int numero) {
        boolean crescente=true;
        int [] numeroEmVetor= ExercicioDOIS.converterArray(numero);

        for (int i = 0; i < numeroEmVetor.length-1; i++) {
            if(numeroEmVetor[i]>numeroEmVetor[i+1])
                crescente=false;
        }
        return crescente;
    }

    public int[] retornaNumeroComElementosCrescentes() {
        /*int[] tempArray= new int[array.length];
        int indice=0;*/
        Array novoArray = new Array();

        for (int elemento : this.array) {
            if(verificaNumeroComElementosCrescentes(elemento))
                novoArray.adicionarValor(elemento);
                /*tempArray[indice]=elemento;
                indice++;*/
            }
        return novoArray.toArray();
    }

    private boolean verificarCapicua(int numero) {
        boolean eCapicua = true;

        int[] array = ExercicioDOIS.converterArray(numero);
        int[] arrayInvertido = ExercicioNOVE.inverterArray(array);

        for (int i = 0; i < array.length && eCapicua; i++) {
            if ( arrayInvertido[i] != array[i] ) {
                eCapicua = false;
            }
        }
        return eCapicua;
    }

    public int[] retornaCapicua() {
        Array novoArray = new Array();

        for (int elemento : this.array) {
            if ( verificarCapicua(elemento) ) {
                novoArray.adicionarValor(elemento);
            }
        }
        return novoArray.toArray();
    }

    private boolean verificaElementosIguais(int valor) {
        boolean elementosIguais = true;
        int[] numero = ExercicioDOIS.converterArray(valor);
        for (int i = 0; i < numero.length-1 && elementosIguais; i++) {
                if ( numero[i] != numero[i + 1] )
                    elementosIguais = false;
            }
        return elementosIguais;
    }

    public int[] retornaElementosCompostosPeloMesmoNumero() {
        Array novoArray = new Array();

        for (int elemento : this.array) {
            if(verificaElementosIguais(elemento)){
                novoArray.adicionarValor(elemento);
            }
        }
        return novoArray.toArray();
    }

    private boolean verificaAmnstrong(int numero) {
        int algarismo, numInicial = numero;
        double somaAlgarismos = 0;

        while (numero >=1) {
            algarismo = numero % 10;
            somaAlgarismos = somaAlgarismos + Math.pow(algarismo, 3);
            numero = numero / 10;
        }
        return numInicial == somaAlgarismos;
    }

    public int[] retornaNumerosNaoAmstrong() {
        Array novoArray= new Array();

        for (int elemento:this.array) {
            if(!verificaAmnstrong(elemento)){
                novoArray.adicionarValor(elemento);
            }
        }
        return novoArray.toArray();
    }

    private boolean verificaNumeroMaiorQueN(int []array, int n){
        return array.length>n;
    }

    public int [] retornaNumerosAscendentesMaioresQueN(int n){
        Array novoArray=new Array();

        for (int elemento:this.array) {
            int[] numero= ExercicioDOIS.converterArray(elemento);
            if(verificaNumeroMaiorQueN(numero,n) && verificaNumeroComElementosCrescentes(elemento))
                novoArray.adicionarValor(elemento);
        }
        return novoArray.toArray();
    }

    public boolean verificaIgual(Array array) {
        int[] novoArray = array.toArray();
        if (this.array.length != novoArray.length) {
            return false;
        }
        for (int i = 0; i < this.array.length; i++) {
            if (this.array[i] != novoArray[i]) {
                return false;
            }
        }
        return true;
    }

}
