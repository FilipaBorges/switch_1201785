import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentTest {

    @Test
    public void createValidStudent() {
        Student student = new Student(1190001, "Paulo");
        assertNotNull(student);
    }


    @Test
    public void createStudentNegativeNumber() {
        assertThrows(IllegalArgumentException.class, () -> {
            Student student = new Student(-1111111, "Paulo");
        });
    }

    @Test
    public void createStudentSixNumber() {
        assertThrows(IllegalArgumentException.class, () -> {
            Student student = new Student(111111, "Paulo");
        });
    }

    @Test
    public void createStudentEigthNumber() {
        assertThrows(IllegalArgumentException.class, () -> {
            Student student = new Student(11111111, "Paulo");
        });
    }

    @Test
    public void createStudentInvalidName() {
        assertThrows(IllegalArgumentException.class, () -> {
            Student student = new Student(1111111, "Paul");
        });
    }

    @Test
    public void createStudentNoEvaluation() {
        Student student = new Student(1190001, "Paulo");

        boolean result= student.isEvaluated();

        assertFalse(result);
    }

    @Test
    public void doEvaluation() {
        Student student = new Student(1190001, "Paulo");
        int expected=15;
        int result= student.doEvaluation(15);

        assertEquals(expected,result);
    }

    @Test
    public void doEvaluationWithPreviousGrade() {
        Student student = new Student(1190001, "Paulo");
        student.setGrade(15);
        int expected=15;
        int result=student.doEvaluation(10);

        assertEquals(expected,result);
    }

    @Test
    public void setGradeWithOutOfBoundsGrade() {
        Student student = new Student(1190001, "Paulo");
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            student.setGrade(22);
        });
        String expectedMessage= "A nota tem que ser um valor entre 0 e 20";
        String actualMessage= exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void sortStudentsByNumberAscCasePositive() {
        Student student1 = new Student(1190001, "Paulo");
        Student student2 =new Student(1100001, "Filipa");
        int expected=1;
        int result= student1.compareToByNumber(student2);
        assertEquals(expected,result);
    }

    @Test
    public void sortStudentsByNumberAscCaseNegative() {
        Student student1 = new Student(1190001, "Paulo");
        Student student2 =new Student(1190002, "Filipa");
        int expected=-1;
        int result= student1.compareToByNumber(student2);
        assertEquals(expected,result);
    }

    @Test
    public void sortStudentsByNumberAscCaseSame() {
        Student student1 = new Student(1190001, "Paulo");
        Student student2 =new Student(1190001, "Filipa");
        int expected=0;
        int result= student1.compareToByNumber(student2);
        assertEquals(expected,result);
    }

    @Test
    public void sortStudentsByGradeDesCasePositive() {
        Student student1 = new Student(1190001, "Paulo");
        student1.doEvaluation(19);
        Student student2 =new Student(1190001, "Filipa");
        student2.doEvaluation(15);
        int expected=1;
        int result= student1.compareToByGrade(student2);
        assertEquals(expected,result);
    }

    @Test
    public void sortStudentsByGradeDesCaseNegative() {
        Student student1 = new Student(1190001, "Paulo");
        student1.doEvaluation(15);
        Student student2 =new Student(1190001, "Filipa");
        student2.doEvaluation(19);
        int expected=-1;
        int result= student1.compareToByGrade(student2);
        assertEquals(expected,result);
    }

    @Test
    public void sortStudentsByGradeDesCaseEquals() {
        Student student1 = new Student(1190001, "Paulo");
        student1.doEvaluation(15);
        Student student2 =new Student(1190001, "Filipa");
        student2.doEvaluation(15);
        int expected=0;
        int result= student1.compareToByGrade(student2);
        assertEquals(expected,result);
    }
}