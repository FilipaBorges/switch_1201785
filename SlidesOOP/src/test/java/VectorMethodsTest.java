import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VectorMethodsTest {

    @Test
    public void sortVectorAscWithNull() {
        int[] result= VectorMethods.sortVectorAsc(null);

        Assert.assertNull(result);
    }

    @Test
    public void sortVectorAscBeingEmpty() {
        int [] vector= {};
        int [] expected={};

        int[] result= VectorMethods.sortVectorAsc(vector);

        Assert.assertSame(vector, result);
        Assert.assertEquals(expected.length, result.length);
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void sortVectorAscWithOneElement() {
        int [] vector= {4};
        int [] expected={4};

        int[] result= VectorMethods.sortVectorAsc(vector);

        Assert.assertSame(vector, result);
        Assert.assertEquals(expected.length, result.length);
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void sortVectorAscWithTwoElementsCorrectlyOrdered() {
        int [] vector= {-1,4};
        int [] expected={-1,4};

        int[] result= VectorMethods.sortVectorAsc(vector);

        Assert.assertSame(vector, result);
        Assert.assertEquals(expected.length, result.length);
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void sortVectorAscWithTwoElementsIncorrectlyOrdered() {
        int [] vector= {30,25};
        int [] expected={25,30};

        int[] result= VectorMethods.sortVectorAsc(vector);

        Assert.assertSame(vector, result);
        Assert.assertEquals(expected.length, result.length);
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void sortVectorAscWithSeveralElementsIncorrectlyOrdered() {
        int [] vector= {30,25,25,-1,20};
        int [] expected={-1,20,25,25,30};

        int[] result= VectorMethods.sortVectorAsc(vector);

        Assert.assertSame(vector, result);
        Assert.assertEquals(expected.length, result.length);
        Assert.assertArrayEquals(expected, result);
    }

}