public class Student {
    //atributos
    private int number;
    private String name;
    private int grade;

    //métodos contrutores
    Student(int number, String name) {
        setNumber(number);
        setName(name);
        this.grade=-1;
    }

    //métodos de negócio
    private boolean isNameValid(String name) {
        if ( name != null && name.length() >= 5 ) {
            return true;
        } else
            throw new IllegalArgumentException("Nome de aluno inválido");
    }

    private boolean isNumberValid(int number) {
        String stringNumber = Integer.toString(number);
        if ( stringNumber.length() != 7 || number < 0 ) {
            throw new IllegalArgumentException("Número de aluno não válido.");
        }
        return true;
    }

    private boolean isGradeValid(int grade) {
        return grade>=0 && grade<=20;
    }

    public void setName(String name) {
        if ( isNameValid(name) )
            this.name = name;
    }

    public void setNumber(int number) {
        if ( isNumberValid(number) )
            this.number = number;
    }

    public int getNumber(){
        return this.number;
    }

    public void setGrade(int grade) {
        if(isGradeValid(grade))
            this.grade=grade;
        else
            throw new IllegalArgumentException("A nota tem que ser um valor entre 0 e 20");
    }

    public boolean isEvaluated() {
        return grade>=0;
    }

    public int doEvaluation(int grade) {
        if(isGradeValid(grade) && !this.isEvaluated()){
            return this.grade=grade;
        }
        return this.grade;
    }

    public int compareToByNumber(Student other){
        if(this.number<other.number){
            return -1;
        }
        if (this.number>other.number){
            return 1;
        }
        return 0;
    }

    public int compareToByGrade(Student other){
        if(this.grade<other.grade){
            return -1;
        }
        if(this.grade>other.grade){
            return 1;
        }
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return this.number == student.number;
    }
}
