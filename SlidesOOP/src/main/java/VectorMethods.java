public class VectorMethods {

    public static int [] sortVectorAsc(int [] vector){
        if(vector!=null) {
            int temp=0;

            for (int indiceLinha = 0; indiceLinha < vector.length; indiceLinha++) {
                for (int indice2 = indiceLinha+1; indice2 < vector.length; indice2++) {
                    if(vector[indiceLinha]>vector[indice2]) {
                        temp=vector[indiceLinha];
                        vector[indiceLinha]=vector[indice2];
                        vector[indice2]=temp;
                    }
                }
            }
        }
        return vector;
    }
}
