package com.company;

import java.util.Scanner;

import static com.company.Bloco3.verificaPesoRacaoAdequada;
import static com.company.Bloco3.verificaRacaCanino;

public class Main {
    public static void main(String[] args) {
        //Exercicio3();
        //Exercicio8();
        //Exercicio9();
        //Exercicio10();
        //Exercicio13();
        //Exercicio14();
        //Exercicio15();
        //Exercicio17a();
        //Exercicio17b();

    }


    public static void Exercicio3() {
        int tamanhoSeqNumeros, seqNumeros;
        double contaPar = 0, somaImpares = 0, contaImpar = 0, percentagemPar, mediaImpar;

        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o tamanho da sequência:");
        tamanhoSeqNumeros = ler.nextInt();
        System.out.println("Introduza o primeiro número");
        seqNumeros = ler.nextInt();

        if (tamanhoSeqNumeros <= 0) {
            System.out.println("Colocando um número negativo a sequência termina.");
        } else

            for (int j = 0; j < seqNumeros; j++) {

                if (seqNumeros % 2 == 0) {
                    contaPar += 1;
                } else {
                    contaImpar += 1;
                    somaImpares += seqNumeros;
                }
                System.out.println("Introduza o próximo número");
                seqNumeros = ler.nextInt();
            }
        percentagemPar = (contaPar / (contaImpar + contaPar)) * 100;
        mediaImpar = somaImpares / contaImpar;

        System.out.println("A percentagem de número pares é de: " + String.format("%.2f", percentagemPar) + "% e a média dos números ímpares é de:" + String.format("%.2f", mediaImpar));
    }

    public static void Exercicio8() {
        int num1, num2, somaNumeros = 0, menorNumero, limiteSoma;

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o valor máximo da soma:");
        limiteSoma = ler.nextInt();
        System.out.println("Introduza o número:");
        num1 = ler.nextInt();
        menorNumero = num1;

        while (somaNumeros < limiteSoma) {
            System.out.println("Introduza o número:");
            num2 = ler.nextInt();
            somaNumeros += num2;

            if (num2 < menorNumero) {
                num2 = menorNumero;
            }
        }
        System.out.println("O menor número introduzido é: " + menorNumero);
    }

    public static void Exercicio9() {
        int numeroFuncionarios;
        double salarioBase, horasExtra, salarioMensal, somaSalarios = 0, mediaSalariosMensais = 0;

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o número de funcionários:");
        numeroFuncionarios = ler.nextInt();

        for (int i = 1; i <= numeroFuncionarios; i++) {

            do {
                System.out.println("Introduza o salário base do funcionário:");
                salarioBase = ler.nextDouble();
                System.out.println("Introduza as horas extras que o funcionário fez:");
                horasExtra = ler.nextDouble();
                if (horasExtra < 0 || salarioBase < 650) {
                    System.out.println("Dados inválidos.");
                } else {
                    salarioMensal = salarioBase + (salarioBase * 0.02) * horasExtra;
                    System.out.println("O salário mensal deste funcionário é de: " + salarioMensal);

                    somaSalarios += salarioMensal;
                    mediaSalariosMensais = somaSalarios / i;
                }
            } while (horasExtra != -1);

            System.out.println("A média dos salários mensais pagos pela empresa é de: " + mediaSalariosMensais);
        }
    }

    public static void Exercicio10() {
        int num1, num2, produtoNumeros = 1, maiorNumero, limiteProduto;

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o valor máximo do produto:");
        limiteProduto = ler.nextInt();
        System.out.println("Introduza o número:");
        num1 = ler.nextInt();
        maiorNumero = num1;

        while (produtoNumeros < limiteProduto) {
            System.out.println("Introduza o número:");
            num2 = ler.nextInt();
            if (num2 < 0) {
                System.out.println("O número introduzido é inválido.");
            } else {
                produtoNumeros *= num2;
            }

            if (num2 > maiorNumero) {
                maiorNumero = num2;
            }
        }
        System.out.println("O maior número introduzido é: " + maiorNumero);
    }

    public static void Exercicio13() {
        int codigoProduto;

        do {
            Scanner ler = new Scanner(System.in);
            System.out.println("Introduza o código do produto: ");
            codigoProduto = ler.nextInt();

            if (codigoProduto > 0) {
                if (codigoProduto == 1) {
                    System.out.println("Alimento não perecível");
                } else if (codigoProduto > 1 && codigoProduto < 5) {
                    System.out.println("Alimento perecível");
                } else if (codigoProduto < 7) {
                    System.out.println("Vestuário");
                } else if (codigoProduto == 7) {
                    System.out.println("Higiene Pessoal");
                } else if (codigoProduto < 16) {
                    System.out.println("Limpeza e utensílios domésticos");
                } else
                    System.out.println("Código inválido");
            }
        } while (codigoProduto != 0);
    }

    public static void Exercicio14() {
        int x;
        double valor = 0, conversao;
        //x=1 dolar, x=2 libra, x=3 Iene, x=4 coroa suiça, x=5 franco suiço

        Scanner ler = new Scanner(System.in);

        do {
            System.out.println("Introduza a moeda da qual quer fazer a conversão em euros:");
            System.out.println("Selecione 1 para Dólar, 2 para Libra, 3 para Iene, 4 para Coroa Suiça e 4 para Franco Suiço.");
            x = ler.nextInt();

            if (x > 0 && x < 6) {
                System.out.println("Introduza o valor que quer converter:");
                valor = ler.nextDouble();

                if (valor > 0) {
                    switch (x) {
                        case 1: {
                            conversao = valor * 1.534;
                            System.out.println("O valor em euros é de: " + String.format("%.3f", conversao));
                            break;
                        }
                        case 2: {
                            conversao = valor * 0.774;
                            System.out.println("O valor em euros é de: " + String.format("%.3f", conversao));
                            break;
                        }
                        case 3: {
                            conversao = valor * 161.480;
                            System.out.println("O valor em euros é de: " + String.format("%.3f", conversao));
                            break;
                        }
                        case 4: {
                            conversao = valor * 9.593;
                            System.out.println("O valor em euros é de: " + String.format("%.3f", conversao));
                            break;
                        }
                        case 5: {
                            conversao = valor * 1.601;
                            System.out.println("O valor em euros é de: " + String.format("%.3f", conversao));
                            break;
                        }
                    }
                } else {
                    System.out.println("Não podem ser introduzidos valores negativos");
                }
            }
        } while (valor > 0);
    }

    public static void Exercicio15() {
        int nota = 0;

        Scanner ler = new Scanner(System.in);

        do {
            System.out.println("Introduza a nota do aluno: ");
            System.out.println("Para parar a leitura introduza uma nota negativa");
            nota = ler.nextInt();

            if (nota >= 0 && nota <= 20) {
                if (nota >= 0 && nota <= 4) {
                    System.out.println("Mau");
                } else if (nota <= 9) {
                    System.out.println("Mediocre");
                } else if (nota <= 13) {
                    System.out.println("Suficiente");
                } else if (nota <= 17) {
                    System.out.println("Bom");
                } else if (nota <= 20) {
                    System.out.println("Muito Bom");
                } else
                    System.out.println("A nota introduzida não é válida");
            }
        } while (nota >= 0);
    }

    public static void Exercicio17a() {
        double pesoAnimal, quantidadeRacao;

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o peso do animal:");
        pesoAnimal = ler.nextDouble();
        System.out.println("Introduza a quantidade de ração que o animal come por dia:");
        quantidadeRacao = ler.nextDouble();

        if (pesoAnimal <= 0) {
            System.out.println("Peso impossível.");
        } else if (pesoAnimal <= 10) {
            if (quantidadeRacao == 100) {
                System.out.println("A quantidade de ração é adequada.");
            } else
                System.out.println("A quantidade de ração é inadequada.");
        } else if (pesoAnimal <= 25) {
            if (quantidadeRacao == 250) {
                System.out.println("A quantidade de ração é adequada.");
            } else
                System.out.println("A quantidade de ração é inadequada.");
        } else if (pesoAnimal <= 45) {
            if (quantidadeRacao == 300) {
                System.out.println("A quantidade de ração é adequada.");
            } else
                System.out.println("A quantidade de ração é inadequada.");
        } else if (pesoAnimal > 45) {
            if (quantidadeRacao == 500) {
                System.out.println("A quantidade de ração é adequada.");
            } else
                System.out.println("A quantidade de ração é inadequada.");
        }
    }

    public static void Exercicio17b() {
        double pesoCanino = 0;
        String racaCanino;
        int pesoDadoRacaCanino;
        boolean racaoAdequada = false;

        Scanner ler = new Scanner(System.in);

        do {
            System.out.println("Insira o peso do canino em kg:");
            pesoCanino = ler.nextDouble();
            racaCanino = verificaRacaCanino(pesoCanino);

            if (racaCanino != "") {
                System.out.println("Insira o peso da ração em gramas: ");
                pesoDadoRacaCanino = ler.nextInt();
                if (pesoDadoRacaCanino > 0) {
                    racaoAdequada = verificaPesoRacaoAdequada(racaCanino, pesoDadoRacaCanino);
                    if (racaoAdequada) {
                        System.out.println("A quantidade de ração que dá ao seu canino é adequada");
                    } else {
                        System.out.println("A quantidade de ração que dá ao seu canino não é adequada");
                    }
                } else {
                    System.out.println("O peso da ração do seu canino não pode ser negativo nem nulo.");
                }
            } else {
                System.out.println("Sair");
            }
        } while (pesoCanino > 0);
    }
}