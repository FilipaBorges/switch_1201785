package com.company;

import com.sun.org.apache.xpath.internal.functions.FuncFalse;

import java.lang.invoke.SwitchPoint;

public class Bloco3 {

    public static int Exercicio1(int numero) {
        int resultado = 1;
        int x;

        for (x = numero; x > 1; x--) {
            resultado = resultado * x;
        }
        return resultado;
    }

    public static String Exercicio2(int numeroAlunos, double[] notas) {
        double numeroNotasPositivas = 0, numeroNotasNegativas = 0, somaNotasNegativas = 0, mediaNotasNegativas, percentagemPositivas;
        String mediaNegativasNumeroPositivas;

        if (numeroAlunos <= 0) {
            return "Introduza um número de alunos válido";
        } else {
            for (int i = 0; i < numeroAlunos; i++) {
                double nota = notas[i];

                if (nota >= 10) {
                    numeroNotasPositivas = numeroNotasPositivas + 1;
                } else if (nota < 10 && nota >= 0) {
                    numeroNotasNegativas = numeroNotasNegativas + 1;
                    somaNotasNegativas = somaNotasNegativas + notas[i];
                } else {
                    return "Dados inválidos";
                }
            }
        }

        percentagemPositivas = (numeroNotasPositivas / numeroAlunos) * 100;
        mediaNotasNegativas = somaNotasNegativas / numeroNotasNegativas;

        mediaNegativasNumeroPositivas = "Percentagem de positivas: " + String.format("%.2f", percentagemPositivas) + ". Média de negativas: " + String.format("%.2f", mediaNotasNegativas);

        return mediaNegativasNumeroPositivas;
    }

    /*//versão do exercicio 2 feita pelo professor
    public static int [] NotasPositivas (int [] arr) {
        boolean Pn= false;

        int P=new.int[1];

        for (int i=0; i< arr.length; i++) {
            if (arr[i]>=10) {
                if (Pn== false) {
                    P[0]=arr[i];
                    Pn=true; }
            } else P= (P, arr[i]);
        } return P;

    }*/

    public static String Exercicio3(int[] seqNumero) {
        double percentagemNPares, mediaImpares;
        double numeroPar = 0, numeroImpar = 0, somaImpar = 0;
        double comprimentoArray;

        for (int i = 0; i < (seqNumero.length); i++) {
            if (seqNumero[i] <= 0) {
                break;
            } else {

                int numero = seqNumero[i];
                if (numero % 2 == 0) {
                    numeroPar++;
                } else {
                    somaImpar += numero;
                    numeroImpar++;
                }
            }
        }

        comprimentoArray = numeroPar + numeroImpar;
        percentagemNPares = (numeroPar / comprimentoArray) * 100;
        mediaImpares = somaImpar / numeroImpar;

        return "A percentagem de números pares é de: " + percentagemNPares + " e a média de números ímpares é de: " + mediaImpares;
    }

    public static String Exercicio4a(int minimo, int maximo) {
        int numeroMultiplo3 = 0;
        int numero = minimo;

        if (minimo < 0 || minimo > maximo) {
            return "Dados inválidos";
        } else

            for (int i = 0; i <= (maximo - minimo); i++) {
                if (numero % 3 == 0) {
                    numeroMultiplo3 += 1;
                    numero += 1;
                } else
                    numero += 1;
            }
        return "No intervalo entre " + minimo + " e " + maximo + " existem " + numeroMultiplo3 + " números multiplos de 3.";
    }

    public static String Exercicio4b(int minimo, int maximo, int x) {
        int numeroMultiplox = 0;
        int numero = minimo;

        if (minimo < 0 || minimo > maximo || x <= 0) {
            return "Dados inválidos";
        } else

            for (int i = 0; i <= (maximo - minimo); i++) {
                if (numero % x == 0) {
                    numeroMultiplox += 1;
                    numero += 1;
                } else
                    numero += 1;
            }
        return "No intervalo entre " + minimo + " e " + maximo + " existem " + numeroMultiplox + " números multiplos de " + x;
    }

    public static String Exercicio4c(int minimo, int maximo) {
        int numeroMultiplo3 = 0, numeroMultiplo5 = 0;
        int numero = minimo;

        if (minimo < 0 || minimo > maximo) {
            return "Dados inválidos";
        } else

            for (int i = 0; i <= (maximo - minimo); i++) {
                if (numero % 3 == 0 && numero % 5 == 0) {
                    numeroMultiplo3 += 1;
                    numeroMultiplo5 += 1;
                    numero += 1;
                } else if (numero % 3 == 0) {
                    numeroMultiplo3 += 1;
                    numero += 1;
                } else if (numero % 5 == 0) {
                    numeroMultiplo5 += 1;
                    numero += 1;
                } else
                    numero += 1;
            }
        return "No intervalo entre " + minimo + " e " + maximo + " existem " + numeroMultiplo3 + " números multiplos de 3 e " + numeroMultiplo5 + " números múltiplos de 5.";
    }

    public static String Exercicio4d(int minimo, int maximo, int x, int y) {
        int numeroMultiplox = 0, numeroMultiploy = 0;
        int numero = minimo;

        if (minimo < 0 || minimo > maximo || x < 0 || y < 0) {
            return "Dados inválidos";
        } else

            for (int i = 0; i <= (maximo - minimo); i++) {
                if (numero % x == 0 && numero % y == 0) {
                    numeroMultiplox += 1;
                    numeroMultiploy += 1;
                    numero += 1;
                } else if (numero % x == 0) {
                    numeroMultiplox += 1;
                    numero += 1;
                } else if (numero % y == 0) {
                    numeroMultiploy += 1;
                    numero += 1;
                } else
                    numero += 1;
            }
        return "No intervalo entre " + minimo + " e " + maximo + " existem " + numeroMultiplox + " números multiplos de " + x + " e " + numeroMultiploy + " números múltiplos de " + y;
    }

    public static String Exercicio4e(int minimo, int maximo, int x, int y) {
        int numero = minimo;
        int somaMultiplos = 0;

        if (minimo < 0 || minimo > maximo || x < 0 || y < 0) {
            return "Dados inválidos";
        } else

            for (int i = 0; i <= (maximo - minimo); i++) {
                if (numero % x == 0 && numero % y == 0) {
                    somaMultiplos = 2 * numero + somaMultiplos;
                    numero += 1;
                } else if (numero % x == 0) {
                    somaMultiplos = numero + somaMultiplos;
                    numero += 1;
                } else if (numero % y == 0) {
                    somaMultiplos = numero + somaMultiplos;
                    numero += 1;
                } else
                    numero += 1;
            }
        return "A soma dos múltiplos de " + x + " e de " + y + " é de: " + somaMultiplos;
    }

    public static String Exercicio5a(int min, int max) {
        int somaPar = 0;
        int numero = min;

        if (min < 0 || min > max) {
            return "Dados inválidos";
        } else

            for (int i = 0; i <= (max - min); i++) {
                if (numero % 2 == 0) {
                    somaPar = numero + somaPar;
                    numero += 1;
                } else
                    numero += 1;
            }
        return "A soma de todos os números pares neste intervalo é de: " + somaPar;
    }

    public static String Exercicio5b(int min, int max) {
        int numeroPar = 0;
        int numero = min;

        if (min < 0 || min > max) {
            return "Dados inválidos";
        } else

            for (int i = 0; i <= (max - min); i++) {
                if (numero % 2 == 0) {
                    numeroPar += 1;
                    numero += 1;
                } else
                    numero += 1;
            }
        return "A quantidade de número pares neste intervalo é de: " + numeroPar;
    }

    public static String Exercicio5c(int min, int max) {
        int somaImpar = 0;
        int numero = min;

        if (min < 0 || min > max) {
            return "Dados inválidos";
        } else

            for (int i = 0; i <= (max - min); i++) {
                if (numero % 2 == 0) {
                    numero += 1;
                } else {
                    somaImpar = numero + somaImpar;
                    numero += 1;
                }
            }
        return "A soma de todos os números ímpares neste intervalo é de: " + somaImpar;
    }

    public static String Exercicio5d(int min, int max) {
        int numeroImpar = 0;
        int numero = min;

        if (min < 0 || min > max) {
            return "Dados inválidos";
        } else

            for (int i = 0; i <= (max - min); i++) {
                if (numero % 2 == 0) {
                    numero += 1;
                } else {
                    numeroImpar += 1;
                    numero += 1;
                }
            }
        return "A quantidade de número ímpares neste intervalo é de: " + numeroImpar;
    }

    public static String Exercicio5e(int min, int max, int x) {
        int somaMultiplos = 0;
        int num = min;
        int aux;

        if (max < min) {
            aux = min;
            min = max;
            max = aux;
        } else

            for (int i = min; i <= max; i++) {
                if (i % x == 0) {
                    somaMultiplos = somaMultiplos + num;
                    num += 1;
                } else {
                    num += 1;
                }
            }
        return "A soma dos múltiplos de " + x + " neste intervalo é de: " + somaMultiplos;
    }

    public static String Exercicio5f(int min, int max, int x) {
        int produtoMultiplos = 1;
        int num = min;

        for (int i = min; i <= max; i++) {
            if (i % x == 0 && i != 0) {
                produtoMultiplos = produtoMultiplos * num;
                num += 1;
            } else {
                num += 1;
            }
        }
        return "O produto de todos os números múltiplos de " + x + " neste intervalo é de: " + produtoMultiplos;
    }

    public static String Exercicio5g(int min, int max, int x) {
        int num = min;
        double numeroMultiplosX = 0, totalMultiplos = 0;

        if (min < 0 || max < 0) {
            return "O intervalo indicado não é válido";
        }
        for (int i = min; i <= max; i++) {
            if (i % x == 0) {
                totalMultiplos = totalMultiplos + num;
                numeroMultiplosX++;
                num++;
            } else {
                num++;
            }
        }
        double mediaMultiplosX = totalMultiplos / numeroMultiplosX;
        return "A média dos múltiplos de " + x + " neste intervalo é de: " + String.format("%.1f", mediaMultiplosX);
    }

    public static String Exercicio5h(int min, int max, String escolha, int X, int Y) {
        int num = min;
        double numeroMultiplosX = 0, totalMultiplosx = 0, numeroMultiplosy = 0, totalMultiplosy = 0;

        if (min < 0 || max < 0) {
            return "O intervalo indicado não é válido";
        }
        if (escolha == "x") {
            for (int i = min; i <= max; i++) {
                if (i % X == 0) {
                    totalMultiplosx = totalMultiplosx + num;
                    numeroMultiplosX++;
                    num++;
                } else {
                    num++;
                }
            }
            return "A média dos múltiplos de x é de: " + (totalMultiplosx / numeroMultiplosX);
        } else if (escolha == "y") {
            for (int i = min; i <= max; i++) {
                if (i % Y == 0) {
                    totalMultiplosy = totalMultiplosy + num;
                    numeroMultiplosy++;
                    num++;
                } else {
                    num++;
                }
            }
        }
        return "A média dos múltiplos de y é de: " + (totalMultiplosy / numeroMultiplosy);
    }

    public static int Exercicio6a(int numLongo) {
        int numAlgarismos = 1;

        do {
            numAlgarismos++;
            numLongo = numLongo / 10;
        } while (numLongo > 1);

        return numAlgarismos;
    }

    public static int Exercicio6b(int numLongo) {
        int algarismoPar = 0;

        while (numLongo > 1) {
            if (numLongo % 2 == 0) {
                algarismoPar++;
                numLongo = numLongo / 10;
            } else {
                numLongo = numLongo / 10;
            }
        }

        return algarismoPar;
    }

    public static int Exercicio6c(int numLongo) {
        int algarismoImpar = 0;

        while (numLongo > 1) {
            if (numLongo % 2 == 0) {
                numLongo = numLongo / 10;
            } else {
                algarismoImpar++;
                numLongo = numLongo / 10;
            }
        }

        return algarismoImpar;
    }

    public static int Exercicio6d(int numLongo) {
        int algarismo, somaAlgarismos = 0;

        while (numLongo > 0.09) {
            algarismo = numLongo % 10;
            somaAlgarismos = somaAlgarismos + algarismo;
            numLongo = numLongo / 10;
        }
        return somaAlgarismos;
    }

    public static int Exercicio6e(int numLongo) {
        int algarismo, somaAlgarismoPar = 0;

        while (numLongo > 0.09) {
            if (numLongo % 2 == 0) {
                algarismo = numLongo % 10;
                somaAlgarismoPar = somaAlgarismoPar + algarismo;
                numLongo = numLongo / 10;
            } else {
                numLongo = numLongo / 10;
            }
        }
        return somaAlgarismoPar;
    }

    public static int Exercicio6f(int numLongo) {
        int algarismo, somaAlgarismoImpar = 0;

        while (numLongo > 0.09) {
            if (numLongo % 2 == 0) {
                numLongo = numLongo / 10;
            } else {
                algarismo = numLongo % 10;
                somaAlgarismoImpar = somaAlgarismoImpar + algarismo;
                numLongo = numLongo / 10;
            }
        }
        return somaAlgarismoImpar;
    }

    public static String Exercicio6g(int numLongo) {
        int algarismo;
        double numeroAlgarismos = 0, somaAlgarismos = 0, mediaAlgarismos = 0.0;

        while (numLongo > 0.09) {
            algarismo = numLongo % 10;
            somaAlgarismos = somaAlgarismos + algarismo;
            numeroAlgarismos++;
            numLongo = numLongo / 10;
        }
        mediaAlgarismos = somaAlgarismos / numeroAlgarismos;
        return String.format("%.1f", mediaAlgarismos);
    }

    public static double Exercicio6h(int numLongo) {
        int algarismo, algarismoPar = 0, somaAlgarismoPar = 0;
        double mediaAlgarismosPar;

        while (numLongo > 0.09) {
            if (numLongo % 2 == 0) {
                algarismo = numLongo % 10;
                algarismoPar++;
                somaAlgarismoPar = somaAlgarismoPar + algarismo;
                numLongo = numLongo / 10;
            } else {
                numLongo = numLongo / 10;
            }
        }
        mediaAlgarismosPar = somaAlgarismoPar / algarismoPar;
        return mediaAlgarismosPar;
    }

    public static double Exercicio6i(int numLongo) {
        int algarismo, algarismoImpar = 0, somaAlgarismoImpar = 0;
        double mediaAlgarismosImpar;

        while (numLongo > 0.09) {
            if (numLongo % 2 == 0) {
                numLongo = numLongo / 10;
            } else {
                algarismo = numLongo % 10;
                algarismoImpar++;
                somaAlgarismoImpar = somaAlgarismoImpar + algarismo;
                numLongo = numLongo / 10;
            }
        }
        mediaAlgarismosImpar = somaAlgarismoImpar / algarismoImpar;
        return mediaAlgarismosImpar;
    }

    public static int Exercicio6j(int numLongo) {
        int numInvertido = 0;

        while (numLongo > 0.09) {
            numInvertido = numInvertido * 10;
            numInvertido = numInvertido + (numLongo % 10);
            numLongo = numLongo / 10;
        }
        return numInvertido;
    }

    public static int inverterNumero(int numLongo) {
        int numInvertido = 0;
        while (numLongo > 0.09) {
            numInvertido = numInvertido * 10;
            numInvertido = numInvertido + (numLongo % 10);
            numLongo = numLongo / 10;
        }
        return numInvertido;
    }

    public static boolean Exercicio7a(int numLongo) {
        int numInvertido = 0;
        int numInicial = numLongo;

        while (numLongo > 0.09) {
            numInvertido = numInvertido * 10;
            numInvertido = numInvertido + (numLongo % 10);
            numLongo = numLongo / 10;
        }
        return numInicial == numInvertido;
    }

    public static boolean Exercicio7b(int numLongo) {
        int algarismo, numInicial = numLongo;
        double somaAlgarismos = 0;

        while (numLongo > 0.09) {
            algarismo = numLongo % 10;
            somaAlgarismos = somaAlgarismos + Math.pow(algarismo, 3);
            numLongo = numLongo / 10;
        }
        return numInicial == somaAlgarismos;
    }

    public static int Exercicio7c(int min, int max) {
        //Se não houver capicuas no intervalo é retornado o valor -1
        for (int i = min; i <= max; i++) {
            if (Exercicio7a(i))
                return i;
        }
        return -1;
    }

    public static int Exercicio7d(int min, int max) {
        for (int i = max; i >= min; i--) {
            if (Exercicio7a(i)) {
                return i;
            }
        }
        return -1;
    }

    public static int Exercicio7e(int min, int max) {
        int somaCapicuas = 0;

        for (int i = min; i <= max; i++) {
            if (Exercicio7a(i)) {
                somaCapicuas++;
            } else
                return -1;
        }
        return somaCapicuas;
    }

    public static int Exercicio7f(int min, int max) {
        for (int i = min; i <= max; i++) {
            if (Exercicio7b(i)) {
                return i;
            }
        }
        return -1;
    }

    public static int Exercicio7g(int min, int max) {
        int somaAmstrong = 0;

        for (int i = min; i <= max; i++) {
            if (Exercicio7b(i)) {
                somaAmstrong++;
            }
        }
        return somaAmstrong;
    }

    public static String Exercicio11(int numero) {
        int maneirasObterN = 0;
        String resultado = "";
        if (numero > 0 && numero < 21) {
            for (int i = 0; i < ((numero + 1) / 2); i++) {
                if (i + (numero - i) == numero) {
                    maneirasObterN++;
                    resultado = resultado + i + "+" + (numero - i) + ",";
                }
            }
        }
        return "Existem " + maneirasObterN + " maneiras de obter " + numero + " que são: " + resultado;
    }

    public static String Exercicio12(int a, int b, int c) {
        double delta, x1, x2;
        delta = ((b * b) - (4 * a * c));
        //se a=0 a equação não é de segundo grau.

        if (a == 0) {
            return "Não é equação de segundo grau";
        } else {
            if (delta > 0) {
                x1 = ((-b + (Math.sqrt(delta))) / (2 * a));
                x2 = ((-b - (Math.sqrt(delta))) / (2 * a));
                return "A equação tem duas raízes reais: " + String.format("%.2f", x1) + " e " + String.format("%.2f", x2);
            } else if (delta == 0) {
                x1 = ((-b + (Math.sqrt(delta))) / (2 * a));
                return "A equação tem uma raiz dupla com o valor de " + String.format("%.2f", x1);
            } else
                return "A equação tem raizes imaginárias";
        }
    }

    public static String Exercicio13(int codigoProduto) {

        while (codigoProduto != 0) {
            if (codigoProduto > 0) {
                if (codigoProduto == 1) {
                    return "Alimento não perecível";
                } else if (codigoProduto < 5) {
                    return "Alimento perecível";
                } else if (codigoProduto < 7) {
                    return "Vestuário";
                } else if (codigoProduto == 7) {
                    return "Higiene Pessoal";
                } else if (codigoProduto < 16) {
                    return "Limpeza e utensílios domésticos";
                } else
                    return "Código inválido";
            }
        }
        return "Código inválido";
    }

    public static String Exercicio14(int x, double valor) {
        double conversao;
        //x=1 dolar, x=2 libra, x=3 Iene, x=4 coroa suiça, x=5 franco suiço

        while (valor > 0) {
            switch (x) {
                case 1: {
                    conversao = valor * 1.534;
                    return "O valor em euros é de: " + String.format("%.3f", conversao);
                }
                case 2: {
                    conversao = valor * 0.774;
                    return "O valor em euros é de: " + String.format("%.3f", conversao);
                }
                case 3: {
                    conversao = valor * 161.480;
                    return "O valor em euros é de: " + String.format("%.3f", conversao);
                }
                case 4: {
                    conversao = valor * 9.593;
                    return "O valor em euros é de: " + String.format("%.3f", conversao);
                }
                case 5: {
                    conversao = valor * 1.601;
                    return "O valor em euros é de: " + String.format("%.3f", conversao);
                }
            }
            return "Não realiza essa conversão";
        }
        return "Não podem ser introduzidos valores negativos";
    }

    public static String Exercicio15(int nota) {
         do {
            if (nota <= 4) {
                return ("Mau");
            } else if (nota <= 9) {
                return ("Mediocre");
            } else if (nota <= 13) {
                return ("Suficiente");
            } else if (nota <= 17) {
                return ("Bom");
            } else {
                return ("Muito Bom");
            }
        } while (nota >= 0 && nota <= 20);
    }

    public static String Exercicio16(double salario) {
        double salarioLiquido;
        //se o salário for um valor impossível retorna -1.

        do {
            if (salario < 500) {
                salarioLiquido = salario * 0.9;
                return String.format("%.2f", salarioLiquido);
            } else if (salario < 1000) {
                salarioLiquido = salario * 0.85;
                return String.format("%.2f", salarioLiquido);
            } else if (salario >= 1000) {
                salarioLiquido = salario * 0.80;
                return String.format("%.2f", salarioLiquido);
            }
        } while (salario > 0);
        return "Não pode pagar negativo ao empregado.";
    }

    public static String Exercicio17a(double pesoAnimal, double quantidadeRacao) {

        if (pesoAnimal <= 10) {
            if (quantidadeRacao == 100) {
                return ("A quantidade de ração é adequada.");
            } else
                return ("A quantidade de ração é inadequada.");
        } else if (pesoAnimal <= 25) {
            if (quantidadeRacao == 250) {
                return ("A quantidade de ração é adequada.");
            } else
                return ("A quantidade de ração é inadequada.");
        } else if (pesoAnimal <= 45) {
            if (quantidadeRacao == 300) {
                return ("A quantidade de ração é adequada.");
            } else
                return ("A quantidade de ração é inadequada.");
        } else if (pesoAnimal > 45) {
            if (quantidadeRacao == 500) {
                return ("A quantidade de ração é adequada.");
            } else
                return ("A quantidade de ração é inadequada.");
        }
        return "Peso do animal impossível";
    }

    public static String verificaRacaCanino(double pesoCanino) {
        if (pesoCanino > 0.0 && pesoCanino <= 10) {
            return "Pequena";
        } else if (pesoCanino <= 25.0) {
            return "Media";
        } else if (pesoCanino <= 45) {
            return "Grande";
        } else if (pesoCanino > 45) {
            return "Gigante";
        }
        return "";
    }

    public static boolean verificaPesoRacaoAdequada(String racaCanino, int pesoRacao) {
        if (racaCanino == "Pequena" && pesoRacao == 100) {
            return true;
        } else if (racaCanino == "Media" && pesoRacao == 250) {
            return true;
        } else if (racaCanino == "Grande" && pesoRacao == 300) {
            return true;
        } else
            return racaCanino == "Gigante" && pesoRacao == 500;
    }

    public static int numCCcomExtra(int numCC, int numExtra) {
        int numFinalCC;

        numFinalCC = numCC * 10 + numExtra;

        return numFinalCC;
    }

    public static double somaPonderada(int numFinalCC) {
        int algarismo;
        double somaPonderada = 0;

        for (int i = 1; i <= 9; i++) {
            algarismo = numFinalCC % 10;
            somaPonderada = somaPonderada + algarismo * i;
            numFinalCC = numFinalCC / 10;
        }
        return somaPonderada;
    }

    public static String Exercicio18(int numCC, int numExtra) {
        int numFinalCC = numCCcomExtra(numCC, numExtra);

        if (somaPonderada(numFinalCC) % 11 == 0) {
            return "O número do CC está correto";
        } else {
            return "O número do CC está errado";
        }
    }

    public static String Exercicio19(int numLongo) {
        int algarismo, numPares = 0, numImpares = 0;

        while (numLongo > 0.09) {
            algarismo = numLongo % 10;
            if (algarismo % 2 == 0) {
                numPares = numPares * 10 + algarismo;
                numLongo = numLongo / 10;
            } else {
                numImpares = numImpares * 10 + algarismo;
                numLongo = numLongo / 10;
            }
        }
        numPares = inverterNumero(numPares);
        numImpares = inverterNumero(numImpares);
        return numPares + " e " + numImpares;
    }

    public static int obterSomaDivisoresdeUmNumero(int num) {
        int somaDivisores = 0;
        int x = 1;

        for (int i = 1; i < num; i++) {
            if (num % x == 0) {
                somaDivisores = somaDivisores + x;
                x++;
            } else {
                x++;
            }
        }
        return somaDivisores;
    }

    public static String Exercicio20(int num) {

        do {
            if (obterSomaDivisoresdeUmNumero(num) == num) {
                return "O número é perfeito";
            } else if (obterSomaDivisoresdeUmNumero(num) > num) {
                return "É um número abundante";
            } else if (obterSomaDivisoresdeUmNumero(num) < num) {
                return "É um número reduzido";
            }
        } while (num>0);
        return "O número tem que ser positivo";
    }
}
