package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco3Test {

    @Test
    void Exercicio1_Teste1() {
        int numero = 4;
        int esperado = 24;

        int resultado = Bloco3.Exercicio1(numero);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio1_Test2() {
        int numero = -1;
        int esperado = 1;

        int resultado = Bloco3.Exercicio1(numero);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio2_test1() {
        int numeroAlunos = 4;
        double[] notas = {12, 14, 8, 9};
        String esperado = "Percentagem de positivas: " + String.format("%.2f", 50.00) + ". Média de negativas: " + String.format("%.2f", 8.50);

        String resultado = Bloco3.Exercicio2(numeroAlunos, notas);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio2_test2() {
        int numeroAlunos = 0;
        double[] notas = {0};
        String esperado = "Introduza um número de alunos válido";

        String resultado = Bloco3.Exercicio2(numeroAlunos, notas);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio2_test3() {
        int numeroAlunos = 3;
        double[] notas = {-2, 10, 11};
        String esperado = "Dados inválidos";

        String resultado = Bloco3.Exercicio2(numeroAlunos, notas);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio3_test1() {
        int[] seqNum = {2, 4, 6, 7, 9, -2, 5};
        String esperada = "A percentagem de números pares é de: " + String.format("%.1f", 60.0) + " e a média de números ímpares é de: " + String.format("%.1f", 8.0);

        String resultado = Bloco3.Exercicio3(seqNum);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio4a_DadosInvalidos() {
        int minimo = 10;
        int maximo = 5;
        String esperada = "Dados inválidos";

        String resultado = Bloco3.Exercicio4a(minimo, maximo);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio4a_DadosInvalidos1() {
        int minimo = -2;
        int maximo = 25;
        String esperada = "Dados inválidos";

        String resultado = Bloco3.Exercicio4a(minimo, maximo);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio4a_DadosInvalidos2() {
        int minimo = 10;
        int maximo = -5;
        String esperada = "Dados inválidos";

        String resultado = Bloco3.Exercicio4a(minimo, maximo);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio4a_5Multiplos() {
        int minimo = 0;
        int maximo = 12;
        String esperada = "No intervalo entre " + 0 + " e " + 12 + " existem " + 5 + " números multiplos de 3.";

        String resultado = Bloco3.Exercicio4a(minimo, maximo);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio4a_3Multiplos() {
        int minimo = 6;
        int maximo = 12;
        String esperada = "No intervalo entre " + 6 + " e " + 12 + " existem " + 3 + " números multiplos de 3.";

        String resultado = Bloco3.Exercicio4a(minimo, maximo);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio4b_DadosInvalidos() {
        int minimo = 5;
        int maximo = 10;
        int x = 0;
        String esperada = "Dados inválidos";

        String resultado = Bloco3.Exercicio4b(minimo, maximo, x);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio4b_3Multiplos() {
        int minimo = 0;
        int maximo = 10;
        int x = 5;
        String esperada = "No intervalo entre " + 0 + " e " + 10 + " existem " + 3 + " números multiplos de " + 5;

        String resultado = Bloco3.Exercicio4b(minimo, maximo, x);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio4b_4Multiplos() {
        int minimo = 0;
        int maximo = 18;
        int x = 6;
        String esperada = "No intervalo entre " + 0 + " e " + 18 + " existem " + 4 + " números multiplos de " + 6;

        String resultado = Bloco3.Exercicio4b(minimo, maximo, x);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio4c_3Multiplos3e2Multiplos5() {
        int minimo = 0;
        int maximo = 6;
        String esperada = "No intervalo entre " + 0 + " e " + 6 + " existem " + 3 + " números multiplos de 3 e " + 2 + " números múltiplos de 5.";

        String resultado = Bloco3.Exercicio4c(minimo, maximo);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio4c_DadosInvalidos() {
        int minimo = 12;
        int maximo = 6;
        String esperada = "Dados inválidos";

        String resultado = Bloco3.Exercicio4c(minimo, maximo);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio4d_Multiplos2eMultiplos7() {
        int minimo = 0;
        int maximo = 20;
        int x = 2;
        int y = 7;
        String esperada = "No intervalo entre " + 0 + " e " + 20 + " existem " + 11 + " números multiplos de " + 2 + " e " + 3 + " números múltiplos de " + 7;

        String resultado = Bloco3.Exercicio4d(minimo, maximo, x, y);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio4e_SomaMultiplos2E5() {
        int minimo = 0;
        int maximo = 10;
        int x = 2;
        int y = 5;
        String esperada = "A soma dos múltiplos de " + 2 + " e de " + 5 + " é de: " + 45;

        String resultado = Bloco3.Exercicio4e(minimo, maximo, x, y);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio4e_SomaMultiplos3E17() {
        int minimo = 0;
        int maximo = 20;
        int x = 3;
        int y = 17;
        String esperada = "A soma dos múltiplos de " + 3 + " e de " + 17 + " é de: " + 80;

        String resultado = Bloco3.Exercicio4e(minimo, maximo, x, y);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio5a_SomaPares() {
        int min = 0;
        int max = 10;
        String esperada = "A soma de todos os números pares neste intervalo é de: " + 30;

        String resultado = Bloco3.Exercicio5a(min, max);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio5a_SomaParInvalido() {
        int min = -2;
        int max = 4;
        String esperada = "Dados inválidos";

        String resultado = Bloco3.Exercicio5a(min, max);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio5b_DadosInvalido() {
        int min = 10;
        int max = 2;
        String esperada = "Dados inválidos";

        String resultado = Bloco3.Exercicio5b(min, max);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio5b_10NumerosPares() {
        int min = 0;
        int max = 20;
        String esperada = "A quantidade de número pares neste intervalo é de: " + 11;

        String resultado = Bloco3.Exercicio5b(min, max);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio5c_DadosInvalidos() {
        int min = -1;
        int max = 2;
        String esperada = "Dados inválidos";

        String resultado = Bloco3.Exercicio5c(min, max);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio5c_8Impares() {
        int min = 4;
        int max = 20;
        String esperada = "A soma de todos os números ímpares neste intervalo é de: " + 96;

        String resultado = Bloco3.Exercicio5c(min, max);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio5d_DadosInvalido() {
        int min = -2;
        int max = 2;
        String esperada = "Dados inválidos";

        String resultado = Bloco3.Exercicio5d(min, max);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio5d_10NumerosPares() {
        int min = 5;
        int max = 20;
        String esperada = "A quantidade de número ímpares neste intervalo é de: " + 8;

        String resultado = Bloco3.Exercicio5d(min, max);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio5e_4Multiplos4() {
        int min = 3;
        int max = 17;
        int x = 4;
        String esperada = "A soma dos múltiplos de " + 4 + " neste intervalo é de: " + 40;

        String resultado = Bloco3.Exercicio5e(min, max, x);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio5f_test1() {
        int min = 2;
        int max = 9;
        int x = 2;
        String esperada = "O produto de todos os números múltiplos de " + 2 + " neste intervalo é de: " + 384;

        String resultado = Bloco3.Exercicio5f(min, max, x);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio5f_test2() {
        int min = 0;
        int max = 10;
        int x = 5;
        String esperada = "O produto de todos os números múltiplos de " + 5 + " neste intervalo é de: " + 50;

        String resultado = Bloco3.Exercicio5f(min, max, x);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio5g_test1Media() {
        int min = 2;
        int max = 10;
        int x = 2;
        String esperada = "A média dos múltiplos de " + 2 + " neste intervalo é de: " + 6.0;

        String resultado = Bloco3.Exercicio5g(min, max, x);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio5g_testeInvalido() {
        int min = 3;
        int max = -2;
        int x = 2;
        String esperada = "O intervalo indicado não é válido";

        String resultado = Bloco3.Exercicio5g(min, max, x);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio5g_test12Media() {
        int min = 10;
        int max = 27;
        int x = 5;
        String esperada = "A média dos múltiplos de " + 5 + " neste intervalo é de: " + 17.5;

        String resultado = Bloco3.Exercicio5g(min, max, x);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio5h_testeX() {
        int min = 2;
        int max = 8;
        String escolha = "x";
        int X = 2;
        int Y = 4;
        String esperada = "A média dos múltiplos de x é de: " + 5.0;

        String resultado = Bloco3.Exercicio5h(min, max, escolha, X, Y);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio5h_testeY() {
        int min = 2;
        int max = 8;
        String escolha = "y";
        int X = 2;
        int Y = 4;
        String esperada = "A média dos múltiplos de y é de: " + 6.0;

        String resultado = Bloco3.Exercicio5h(min, max, escolha, X, Y);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio6a_numeroLongo12345() {
        int numLongo = 12345;
        int esperado = 5;

        int resultado = Bloco3.Exercicio6a(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6a_numeroLongo12345678910() {
        int numLongo = 1234567891;
        int esperado = 10;

        int resultado = Bloco3.Exercicio6a(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6b_3Par() {
        int numLongo = 112243579;
        int esperado = 3;

        int resultado = Bloco3.Exercicio6b(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6b_4Par() {
        int numLongo = 24685555;
        int esperado = 4;

        int resultado = Bloco3.Exercicio6b(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6b_2Par() {
        int numLongo = 357964;
        int esperado = 2;

        int resultado = Bloco3.Exercicio6b(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6b_0Par() {
        int numLongo = 57937;
        int esperado = 0;

        int resultado = Bloco3.Exercicio6b(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6c_3Impar() {
        int numLongo = 247965;
        int esperado = 3;

        int resultado = Bloco3.Exercicio6c(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6c_0Impar() {
        int numLongo = 4682;
        int esperado = 0;

        int resultado = Bloco3.Exercicio6c(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6c_4Impar() {
        int numLongo = 5579462;
        int esperado = 4;

        int resultado = Bloco3.Exercicio6c(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6d_teste1() {
        int numLongo = 1234;
        int esperado = 10;

        int resultado = Bloco3.Exercicio6d(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6d_teste2() {
        int numLongo = 4444444;
        int esperado = 28;

        int resultado = Bloco3.Exercicio6d(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6e_SomaPar10() {
        int numLongo = 345679;
        int esperado = 10;

        int resultado = Bloco3.Exercicio6e(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6e_SomaPar6() {
        int numLongo = 222579;
        int esperado = 6;

        int resultado = Bloco3.Exercicio6e(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6e_SomaPar0() {
        int numLongo = 9753;
        int esperado = 0;

        int resultado = Bloco3.Exercicio6e(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6f_SomaImpar0() {
        int numLongo = 4682;
        int esperado = 0;

        int resultado = Bloco3.Exercicio6f(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6f_SomaImpar10() {
        int numLongo = 76348;
        int esperado = 10;

        int resultado = Bloco3.Exercicio6f(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6g_MediaAlgarismos() {
        int numLongo = 222222;
        String esperado = "2.0";

        String resultado = Bloco3.Exercicio6g(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6g_MediaAlgarismos1() {
        int numLongo = 123456;
        String esperado = "3.5";

        String resultado = Bloco3.Exercicio6g(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6h_MediaAlgarismosPar() {
        int numLongo = 123456;
        Double esperado = 4.0;

        Double resultado = Bloco3.Exercicio6h(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6h_MediaAlgarismosPar1() {
        int numLongo = 46579;
        Double esperado = 5.0;

        Double resultado = Bloco3.Exercicio6h(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6i_MediaAlgarismosImpar() {
        int numLongo = 46579;
        Double esperado = 7.0;

        Double resultado = Bloco3.Exercicio6i(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6i_MediaAlgarismosImpar1() {
        int numLongo = 79846;
        Double esperado = 8.0;

        Double resultado = Bloco3.Exercicio6i(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6j_numInvertido() {
        int numLongo = 12345;
        int esperado = 54321;

        int resultado = Bloco3.Exercicio6j(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio6j_numInvertido1() {
        int numLongo = 2347;
        int esperado = 7432;

        int resultado = Bloco3.Exercicio6j(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio7a_capicua() {
        int numLongo = 1331;
        boolean esperado = true;

        boolean resultado = Bloco3.Exercicio7a(numLongo);

        assertTrue(esperado);
    }

    @Test
    void Exercicio7a_naoCapicua() {
        int numLongo = 1789;
        boolean esperado = false;

        boolean resultado = Bloco3.Exercicio7a(numLongo);

        assertFalse(esperado);
    }

    @Test
    void Exercicio7b_Amstrong() {
        int numLongo = 153;
        boolean esperado = true;

        boolean resultado = Bloco3.Exercicio7b(numLongo);

        assertTrue(esperado);
    }

    @Test
    void Exercicio7b_naoAmstrong() {
        int numLongo = 244;
        boolean esperado = false;

        boolean resultado = Bloco3.Exercicio7b(numLongo);

        assertFalse(esperado);
    }

    @Test
    void Exercicio7c_primeiraCapicua() {
        int min = 10;
        int max = 20;
        int esperado = 11;

        int resultado = Bloco3.Exercicio7c(min, max);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio7c_semCapicua() {
        int min = 12;
        int max = 21;
        int esperado = -1;

        int resultado = Bloco3.Exercicio7c(min, max);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio7c_primeiraCapicua1() {
        int min = 100;
        int max = 200;
        int esperado = 101;

        int resultado = Bloco3.Exercicio7c(min, max);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio7d_ultimaCapicua1() {
        int min = 100;
        int max = 203;
        int esperado = 202;

        int resultado = Bloco3.Exercicio7d(min, max);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio7d_naoHaCapicua() {
        int min = 12;
        int max = 21;
        int esperado = -1;

        int resultado = Bloco3.Exercicio7d(min, max);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio7e_somaCapicua4() {
        int min = 100;
        int max = 132;
        int esperado = 4;

        int resultado = Bloco3.Exercicio7e(min, max);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio7e_somaCapicua0() {
        int min = 100;
        int max = 132;
        int esperado = -1;

        int resultado = Bloco3.Exercicio7e(min, max);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio7f_primeiroAmnstrong() {
        int min = 152;
        int max = 160;
        int esperado = 153;

        int resultado = Bloco3.Exercicio7f(min, max);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio7g_somaAmnstrong() {
        int min = 152;
        int max = 160;
        int esperado = 1;

        int resultado = Bloco3.Exercicio7g(min, max);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio11_3maneiras() {
        int numero = 5;
        String esperada = "Existem " + 3 + " maneiras de obter " + 5 + " que são: " + "0+5,1+4,2+3,";

        String resultado = Bloco3.Exercicio11(numero);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio11_8maneiras() {
        int numero = 15;
        String esperada = "Existem " + 8 + " maneiras de obter " + 15 + " que são: " + "0+15,1+14,2+13,3+12,4+11,5+10,6+9,7+8,";

        String resultado = Bloco3.Exercicio11(numero);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio12_equacaoNaoE2Grau() {
        int a = 0;
        int b = 2;
        int c = 4;
        String esperada = "Não é equação de segundo grau";

        String resultado = Bloco3.Exercicio12(a, b, c);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio12_duasRaizes() {
        int a = 2;
        int b = 9;
        int c = 2;
        String esperado = "A equação tem duas raízes reais: " + -0.23 + " e " + -4.27;

        String resultado = Bloco3.Exercicio12(a, b, c);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio12_umaRaiz() {
        int a = 4;
        int b = -4;
        int c = 1;
        String esperado = "A equação tem uma raiz dupla com o valor de " + String.format("%.2f", 0.50);

        String resultado = Bloco3.Exercicio12(a, b, c);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio12_raizImaginaria() {
        int a = 1;
        int b = -4;
        int c = 5;
        String esperado = "A equação tem raizes imaginárias";

        String resultado = Bloco3.Exercicio12(a, b, c);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio13_alimentoNaoPerecivel() {
        int codigoProduto = 1;
        String esperada = "Alimento não perecível";

        String resultado = Bloco3.Exercicio13(codigoProduto);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio13_alimentoPerecivel() {
        int codigoProduto = 2;
        String esperada = "Alimento perecível";

        String resultado = Bloco3.Exercicio13(codigoProduto);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio13_alimentoPerecivel1() {
        int codigoProduto = 4;
        String esperada = "Alimento perecível";

        String resultado = Bloco3.Exercicio13(codigoProduto);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio13_vestuario() {
        int codigoProduto = 5;
        String esperada = "Vestuário";

        String resultado = Bloco3.Exercicio13(codigoProduto);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio13_vestuario1() {
        int codigoProduto = 6;
        String esperada = "Vestuário";

        String resultado = Bloco3.Exercicio13(codigoProduto);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio13_higienePessoal() {
        int codigoProduto = 7;
        String esperada = "Higiene Pessoal";

        String resultado = Bloco3.Exercicio13(codigoProduto);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio13_limpeza() {
        int codigoProduto = 8;
        String esperada = "Limpeza e utensílios domésticos";

        String resultado = Bloco3.Exercicio13(codigoProduto);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio13_limpeza1() {
        int codigoProduto = 15;
        String esperada = "Limpeza e utensílios domésticos";

        String resultado = Bloco3.Exercicio13(codigoProduto);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio13_invalido() {
        int codigoProduto = 16;
        String esperada = "Código inválido";

        String resultado = Bloco3.Exercicio13(codigoProduto);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio13_invalido1() {
        int codigoProduto = 0;
        String esperada = "Código inválido";

        String resultado = Bloco3.Exercicio13(codigoProduto);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio14_dolar() {
        int x = 1;
        int valor = 3;
        String esperada = "O valor em euros é de: " + 4.602;

        String resultado = Bloco3.Exercicio14(x, valor);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio14_libra() {
        int x = 2;
        int valor = 3;
        String esperada = "O valor em euros é de: " + 2.322;

        String resultado = Bloco3.Exercicio14(x, valor);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio14_iene() {
        int x = 3;
        int valor = 3;
        String esperada = "O valor em euros é de: " + String.format("%.3f", 484.440);

        String resultado = Bloco3.Exercicio14(x, valor);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio14_coroaSueca() {
        int x = 4;
        int valor = 3;
        String esperada = "O valor em euros é de: " + 28.779;

        String resultado = Bloco3.Exercicio14(x, valor);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio14_francoSuico() {
        int x = 5;
        int valor = 3;
        String esperada = "O valor em euros é de: " + 4.803;

        String resultado = Bloco3.Exercicio14(x, valor);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio14_naoConverte() {
        int x = 6;
        int valor = 3;
        String esperada = "Não realiza essa conversão";

        String resultado = Bloco3.Exercicio14(x, valor);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio14_paraLeitura() {
        int x = 3;
        int valor = -4;
        String esperada = "Não podem ser introduzidos valores negativos";

        String resultado = Bloco3.Exercicio14(x, valor);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio15_mau() {
        int nota = 3;
        String esperada = "Mau";

        String resultado = Bloco3.Exercicio15(nota);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio15_mediocre() {
        int nota = 7;
        String esperada = "Mediocre";

        String resultado = Bloco3.Exercicio15(nota);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio15_suficiente() {
        int nota = 12;
        String esperada = "Suficiente";

        String resultado = Bloco3.Exercicio15(nota);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio15_bom() {
        int nota = 16;
        String esperada = "Bom";

        String resultado = Bloco3.Exercicio15(nota);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio15_muitoBom() {
        int nota = 20;
        String esperada = "Muito Bom";

        String resultado = Bloco3.Exercicio15(nota);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio15_naoValido() {
        int nota = -1;
        String esperada = "A nota introduzida não é válida";

        String resultado = Bloco3.Exercicio15(nota);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio16_primeiroPatamar() {
        double salario = 475;
        String esperado = "427.50";

        String resultado = Bloco3.Exercicio16(salario);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio16_segundoPatamar() {
        double salario = 957;
        String esperado = "813.45";

        String resultado = Bloco3.Exercicio16(salario);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio16_terceiroPatamar() {
        double salario = 1200;
        String esperado = "960.00";

        String resultado = Bloco3.Exercicio16(salario);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio16_valorInvalido() {
        double salario = -30;
        String esperado = "Não pode pagar negativo ao empregado.";

        String resultado = Bloco3.Exercicio16(salario);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio18_numCCcorreto() {
        int numCC = 14578846;
        int numExtra = 6;
        String esperada = "O número do CC está correto";

        String resultado = Bloco3.Exercicio18(numCC, numExtra);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio19_paresEimpares() {
        int numLongo = 12345678;
        String resultado = "2468" + " e " + "1357";

        String esperado = Bloco3.Exercicio19(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio19_paresEimpares1() {
        int numLongo = 68792446;
        String resultado = "682446" + " e " + "79";

        String esperado = Bloco3.Exercicio19(numLongo);

        assertEquals(esperado, resultado);
    }

    @Test
    void somaDivisores() {
        int num=6;
        int esperado=6;

        int resultado= Bloco3.obterSomaDivisoresdeUmNumero(num);

        assertEquals(esperado, resultado);
    }

    @Test
    void Exercicio20_numeroPerfeito() {
        int num=6;
        String esperada="O número é perfeito";

        String resultado= Bloco3.Exercicio20(num);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio20_numeroAbundante() {
        int num=12;
        String esperada="É um número abundante";

        String resultado= Bloco3.Exercicio20(num);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio20_numeroReduzido() {
        int num=9;
        String esperada="É um número reduzido";;

        String resultado= Bloco3.Exercicio20(num);

        assertEquals(esperada, resultado);
    }

    @Test
    void Exercicio20_numeroNegativo() {
        int num=-2;
        String esperada="O número tem que ser positivo.";

        String resultado= Bloco3.Exercicio20(num);

        assertEquals(esperada, resultado);
    }

}

