package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        //Exercicio3();
        //Exercicio4();
        //Exercicio5();
        //Exercicio6();
        //Exercicio7();
        //Exercicio8();
        //Exercicio9();
        //Exercicio10();
        //Exercicio11();
        //Exercicio12();
        Exercicio13();
    }

    public static void Exercicio3() {
        //estrutura de dados
        double raio, altura, volume;

        //Inicialização para leitura de dados
        Scanner ler = new Scanner(System.in);

        //Leitura de dados
        System.out.println("Introduza o raio");
        raio = ler.nextInt();
        System.out.println("Introduza a altura");
        altura = ler.nextInt();

        //Processamento
        volume = (Math.PI*raio*altura)*1000;

        //saida de dados
        System.out.println("O volume total em litros é:" + volume);
    }
    //-------------------------------------------------------------

    public static double Exercicio3_v2(double raio, double altura) {
        //estrutura de dados
        double volume;

        //Processamento
        volume = (Math.PI*raio*altura)*1000;

        return volume;
    }

//-------------------------------------------------------------

    public static void Exercicio4()
    {
        //estrutura de dados
        int tempo, distancia;

        //inicialização para leitura de dados
        Scanner ler = new Scanner(System.in);

        //leitura de dados
        System.out.println("Introduza o intervalo de tempo em segundos:");
        tempo = ler.nextInt();

        //processamento: a velocidade do som é 1224km/h. Para converter em metro por segundo /3600 e *100
        distancia= tempo * 340;

        //Saída de dados
        System.out.println("A distância a que se encontra a trovoada é de:" + distancia);
    }

    public static void Exercicio5() {

        //dados
        int tempo;
        final double GRAVIDADE= 9.8;
        double altura;

        //Inicialização para leitura de dados
        Scanner ler = new Scanner(System.in);

        //leitura de dados
        System.out.println("Introduza os segundos que o objeto demorou do topo do edíficio até ao chão:");
        tempo = ler.nextInt();

        //Processamento
        altura = (GRAVIDADE*(Math.pow(tempo,2)))/2;

        //Saida de dados
        System.out.println("A altura do edifício em metros é de:" + altura);
    }

    public static void Exercicio6() {
        //Estrutura de dados
        int alturaEdificio, alturaPessoa, sombraEdificio, sombraPessoa;

        //Inicialização para leitura de dados
        Scanner ler = new Scanner(System.in);

        //leitura de dados
        System.out.println("Introduza a altura da pessoa em metros:");
        alturaPessoa = ler.nextInt();
        System.out.println("Introduza a sombra do Edifício em metros:");
        sombraEdificio = ler.nextInt();
        System.out.println("Introduza a sombra da pessoa em metros:");
        sombraPessoa = ler.nextInt();

        //Processamento
        alturaEdificio = (sombraEdificio/sombraPessoa)*alturaPessoa;

        //saida de dados
        System.out.println("A altura do Edifício é:" + alturaEdificio);
    }

    public static void Exercicio7() {
        //estrutura de dados
        int distanciaManuel, tempoManuel, velocidadeManuel, tempoZe, distanciaZe;

        //Inicialização para leitura de dados
        Scanner ler = new Scanner(System.in);

        //leitura de dados
        System.out.println("Introduza a distância percorrida pelo Manuel em metros:");
        distanciaManuel = ler.nextInt();
        System.out.println("Introduza o tempo que o Manuel demorou a acabar em segundos:");
        tempoManuel=ler.nextInt();
        System.out.println("Introduza o tempo que o Ze percorreu em segundos:");
        tempoZe=ler.nextInt();

        //Processamento
        velocidadeManuel=distanciaManuel/tempoManuel;
        distanciaZe=velocidadeManuel*tempoZe;

        //saida de dados
        System.out.println("A distância percorrida pelo Zé foi:" + distanciaZe);
    }

    public static void Exercicio8() {
        //Estrutura de dados
        double comprimento1, comprimento2, angulo, anguloRadianos, distanciaEntreOperarios;

        //Inicialização para leitura de dados
        Scanner ler = new Scanner(System.in);

        //leitura de dados
        System.out.println("Introduza o comprimento do 1º cabo:");
        comprimento1 = ler.nextDouble();
        System.out.println("Introduza o comprimento do 2º cabo:");
        comprimento2 = ler.nextDouble();
        System.out.println("Introduza o ângulo entre os dois cabos:");
        angulo = ler.nextDouble();

        //processamento
        anguloRadianos=Math.toRadians(angulo);
        distanciaEntreOperarios= Math.pow(comprimento1,2)+Math.pow(comprimento2,2)-(2*comprimento1*comprimento2*Math.cos(anguloRadianos));

        //Saida de dados
        System.out.println("A distância entre operários é:" + distanciaEntreOperarios);
    }

    public static void Exercicio9() {
        //Estrutura de dados
        double comprimento, altura, perimetro;

        //Inicialização de leitura de dados
        Scanner ler=new Scanner(System.in);

        //leitura de dados
        System.out.println("Introduza o comprimento do retângulo:");
        comprimento=ler.nextDouble();
        System.out.println("Introduza a altura do retângulo:");
        altura=ler.nextDouble();

        //processamento
        perimetro = getPerimetro(comprimento, altura);

        //saida de dados
        System.out.println("O perimetro do retângulo é de:" + perimetro);
    }

    public static double getPerimetro(double comprimento, double altura) {
        double perimetro;
        perimetro= 2*altura + 2*comprimento;
        return perimetro;
    }

    public static void Exercicio10() {
       //Estrutura de dados
        double c1, c2, h;

        Scanner ler = new Scanner(System.in);

        //leitura de dados
        System.out.println("Introduza o valor do cateto 1:");
        c1=ler.nextDouble();
        System.out.println("Introduza o valor do cateto 2:");
        c2=ler.nextDouble();

        //processamento
        h= geth(c1, c2);

        //saida de dados
        System.out.println("A altura do triângulo é de:"+h);
    }

    public static double geth(double c1, double c2) {
        return Math.sqrt((Math.pow(c1, 2) + Math.pow(c2, 2)));
    }

    public static void Exercicio11() {
       //dados
       double x,y;

       Scanner ler=new Scanner(System.in);

       //leitura de dados
        System.out.println("Introduza o valor de x:");
        x=ler.nextDouble();

        //processamento
        y=Math.pow(x,2)-3*x+1;

        //saida de dados
        System.out.println("O valor da equação é"+y);
    }
     public static void Exercicio12() {
        //dados
         double celsius, fahrenheit;

         Scanner ler=new Scanner(System.in);

         //leitura de dados
         System.out.println("Introduza a temperatura em Celsius:");
         celsius=ler.nextDouble();

         //processamento
         fahrenheit=((9.0/5.0)*celsius)+32;

         //saida de dados
         System.out.println("A temperatura em Fahrenheit é de:"+ fahrenheit);
     }

    public static void Exercicio13() {
        //dados
        int H,M, MT;

        Scanner ler=new Scanner(System.in);

        //leitura de dados
        System.out.println("Introduza as horas:");
        H=ler.nextInt();
        System.out.println("Introduza os minutos;");
        M=ler.nextInt();

        //processamento
        MT=(H*60)+M;

        //saida de dados
        System.out.println("Às " + H + " horas e " + M + " minutos já decorreram " + MT + " minutos desde as 0H");
    }

}
