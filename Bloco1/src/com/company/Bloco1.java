package com.company;

import java.util.Scanner;

public class Bloco1 {

    public static double Exercicio3_v2(double raio, double altura) {
        //estrutura de dados
        double volume;

        //Processamento
        volume = (Math.PI*raio*altura)*1000;

        return volume;
    }

    public static double Exercicio4_v2(double tempo) {
        double distancia;

        //processamento: a velocidade do som é 1224km/h. Para converter em metro por segundo /3600 e *100
        distancia = tempo * 340;

        return distancia;
    }

    public static double Exercicio5_v2(double tempo) {
         final double GRAVIDADE=9.8;
         double altura;

         //Processamento
         altura = (GRAVIDADE*(Math.pow(tempo,2)))/2;

         return altura;
    }

    public static double Exercicio6_v2(double alturaPessoa, double sombraEdificio, double sombraPessoa) {
         double alturaEdificio;
         
         //Processamento
        alturaEdificio = (sombraEdificio/sombraPessoa)*alturaPessoa;

        return alturaEdificio;
    }
    
    public static double Exercicio7_v2(double distanciaManuel, double tempoManuel, double velocidadeManuel, double tempoZe) {
        double distanciaZe;

        //processamento
        velocidadeManuel=distanciaManuel/tempoManuel;
        distanciaZe=velocidadeManuel*tempoZe;

        return distanciaZe;
    }

    public static double Exercicio8_v2(double comprimento1, double comprimento2, double angulo, double anguloRadianos) {
        double distanciaEntreOperarios;

        //processamento
        anguloRadianos=Math.toRadians(angulo);
        distanciaEntreOperarios= Math.pow(comprimento1,2)+Math.pow(comprimento2,2)-(2*comprimento1*comprimento2*Math.cos(anguloRadianos));

        return distanciaEntreOperarios;
    }

    public static double Exercicio9_v2(double comprimento, double altura) {
        double perimetro;

        //processamento
        perimetro= Main.getPerimetro(comprimento, altura);

        return perimetro;
    }

    public static double Exercicio10_v2(double c1, double c2) {
        double h;

        //processamento
        h= Main.geth(c1,c2);

        return h;
    }

    public static double Exercicio11_v2(double x) {
        double y;

        y=Math.pow(x,2)-3*x+1;

        return y;
    }

    public static double Exercicio12_v2(double C)  {
        double F;

        F=32+((9.0/5.0)*C);

        return F;
    }

    public static double Exercicio13_v2(int H, int M) {
        int MT;

        MT=(H*60)+M;

        return MT;
    }
}