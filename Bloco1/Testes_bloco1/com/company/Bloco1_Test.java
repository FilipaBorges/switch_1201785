package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco1_Test {

    @Test
    void Exercicio3_v2_test1() {
        //arrange
        double raio = 2;
        double altura = 2;
        double esperado = 12566.37;
        //act
        double resultado = Bloco1.Exercicio3_v2(raio,altura);
        //assert
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void Exercicio3_v2_test2() {
        //arrange
        double raio = 3;
        double altura = 5;
        double esperado = 47123.88;
        //act
        double resultado = Bloco1.Exercicio3_v2(raio,altura);
        //assert
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void Exercicio4_v2_test1() {
        //arrange
        double tempo=10;
        double esperado=3400;
        //act
        double volume = Bloco1.Exercicio4_v2(tempo);
        //assert
        assertEquals(esperado,volume);
    }

    @Test
    void Exercicio4_v2_test2() {
        //arrange
        double tempo=14.7;
        double esperado=4998;
        //act
        double volume = Bloco1.Exercicio4_v2(tempo);
        //assert
        assertEquals(esperado,volume,0.001);
    }

    @Test
    void Exercicio5_v2_test1() {
        //arange
        double tempo=2;
        double esperado=19.6;
        //act
        double resultado= Bloco1.Exercicio5_v2(tempo);
        //assert
        assertEquals(esperado,resultado,0.001);
    }

    @Test
    void Exercicio5_v2_test2() {
        //arange
        double tempo=5;
        double esperado=122.5;
        //act
        double resultado= Bloco1.Exercicio5_v2(tempo);
        //assert
        assertEquals(esperado,resultado,0.001);
    }

    @Test
    void Exercicio6_v2_test1() {
        double alturaPessoa=2;
        double sombraEdificio=40;
        double sombraPessoa=4;
        double esperado=20;

        //act
        double resultado= Bloco1.Exercicio6_v2(alturaPessoa,sombraEdificio,sombraPessoa);
        //assert
        assertEquals(esperado,resultado,0.001);
    }

    @Test
    void Exercicio6_v2_test2() {
        double alturaPessoa = 1.5;
        double sombraEdificio = 25;
        double sombraPessoa = 3;
        double esperado = 12.5;

        //act
        double resultado = Bloco1.Exercicio6_v2(alturaPessoa, sombraEdificio, sombraPessoa);
        //assert
        assertEquals(esperado, resultado, 0.001);
    }

    @Test
    void Exercicio7_v2_test1() {
        double distanciaManuel=42195;
        double tempoManuel=14530;
        double velocidadeManuel=distanciaManuel/tempoManuel;
        double tempoZe=3900;
        double esperado=11325.56;

        //act
        double resultado= Bloco1.Exercicio7_v2(distanciaManuel,tempoManuel,velocidadeManuel,tempoZe);
        //assert
        assertEquals(esperado,resultado,0.10);
    }

    @Test
    void Exercicio7_v2_test2() {
        double distanciaManuel = 50000;
        double tempoManuel = 15000;
        double velocidadeManuel = distanciaManuel / tempoManuel;
        double tempoZe = 3900;
        double esperado = 13000;

        //act
        double resultado = Bloco1.Exercicio7_v2(distanciaManuel, tempoManuel, velocidadeManuel, tempoZe);
        //assert
        assertEquals(esperado, resultado, 0.001);
    }

    @Test
    void Exercicio8_v2_test1() {
        double comprimento1=40;
        double comprimento2=60;
        double angulo=60;
        double anguloRadianos=Math.toRadians(angulo);
        double esperado= 2800;
        //act
        double resultado= Bloco1.Exercicio8_v2(comprimento1,comprimento2,angulo,anguloRadianos);
        //assert
        assertEquals(esperado,resultado,0.1);
    }

    @Test
    void Exercicio8_v2_test2() {
        double comprimento1=50;
        double comprimento2=30;
        double angulo=40;
        double anguloRadianos=Math.toRadians(angulo);
        double esperado= 1101.86;
        //act
        double resultado= Bloco1.Exercicio8_v2(comprimento1,comprimento2,angulo,anguloRadianos);
        //assert
        assertEquals(esperado,resultado,0.1);
    }

    @Test
    void Exercicio9_v2_test1() {
        double altura= 4;
        double comprimento=2;
        double expected= 12;

        //act
        double result= Bloco1.Exercicio9_v2(comprimento,altura);
        //assert
        assertEquals(expected, result,0.000001);
    }

    @Test
    void Exercicio9_v2_test2() {
        double altura = 5;
        double comprimento =10;
        double esperado= 30;

        //act
        double resultado= Bloco1.Exercicio9_v2(comprimento, altura);
        //assert
        assertEquals(esperado, resultado, 0.00001);
    }

    @Test
    void Exercicio10_v2_test1() {
        double c1=2;
        double c2=2;
        double esperado=2.828;

        //act
        double resultado= Bloco1.Exercicio10_v2(c1,c2);

        assertEquals(esperado,resultado, 0.01);
    }

    @Test
    void Exercicio10_v2_test2() {
        double c1=3;
        double c2=4;
        double esperado=5;

        double resultado= Bloco1.Exercicio10_v2(c1,c2);

        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void Exercicio11_v2_test1() {
        double x=4;
        double esperado=5;

        double resultado= Bloco1.Exercicio11_v2(x);

        assertEquals(esperado,resultado,0.001);
    }

    @Test
    void Exercicio11_v2_test2() {
        double x=3;
        double esperado=1;

        double resultado=Bloco1.Exercicio11_v2(x);

        assertEquals(esperado,resultado,0.001);
    }

    @Test
    void Exercicio12_v2_test1() {
        double C=20;
        double esperado=68;

        double resultado=Bloco1.Exercicio12_v2(C);

        assertEquals(esperado,resultado,0.001);
    }

    @Test
    void Exercicio12_v2_test2() {
        double C=34;
        double esperado=93.2;

        double resultado=Bloco1.Exercicio12_v2(C);

        assertEquals(esperado,resultado,0.001);
    }

    @Test
    void Exercicio13_v2_test1() {
        int H=4;
        int M=6;
        int esperado=246;

        double resultado= Bloco1.Exercicio13_v2(H,M);

        assertEquals(esperado, resultado, 0.001);
    }
    @Test
    void Exercicio13_v2_test2() {
        int H = 8;
        int M = 20;
        int esperado = 500;

        double resultado = Bloco1.Exercicio13_v2(H, M);

        assertEquals(esperado, resultado, 0.001);
    }
}